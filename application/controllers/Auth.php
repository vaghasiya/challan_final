<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Authmodel');
	}
	public function index()
	{
		if($this->session->userdata("AdminId")){
			redirect(base_url("Dashboard"));
		}
		$this->load->view('login');	
	}
	public function doLogin()
	{
		$this->load->library('encrypt');
		$Email = $this->input->post('Email');
		$where = array('Email' => $Email );
		$pass =  $this->input->post('Password');

		$this->form_validation->set_message('required', '<i style="color:red;  margin:-2px;">Please Enter Valid Username or Password !</i>');
		$this->form_validation->set_rules('Email', 'Email', 'required');
		$this->form_validation->set_rules('Password', 'Password', 'required');
				
		if ($this->form_validation->run() == TRUE)
		{
			$data = $this->Authmodel->checkAuth('tbl_admin',$where);
			$reffer = $this->session->userdata('last_page') ? $this->session->userdata('last_page') : base_url('Dashboard');
			if($data['Password'] != ''){
				$dbpass = $this->encrypt->decode($data['Password']);
				if ($pass == $dbpass) {
					$session_set = array(
						'AdminId' => $data['AdminId'],
						'Email' => $data['Email'],
						'Name' => $data['Name']
					);
					$this->session->set_userdata($session_set);
					$this->session->set_flashdata('success','Login Successfully !');
					redirect($reffer);
				} else {
					$this->session->set_flashdata('error','Please Enter Valid Username or Password !');
					$this->load->view('login');
				}
			} else {
				$this->session->set_flashdata('error','No Record Found For This User!');
				$this->load->view('login');
			}
		} else {
			$this->session->set_flashdata('error','Please Enter Valid Username or Password !');
			$this->load->view('login');
		}
	}
	public function Logout()
	{
		$array_items = array('AdminId' => '', 'Email' => '');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url().'Auth');
	}
	public function forgotPassword()
	{
		$this->load->library('encrypt');
		$to_email = $this->input->post('UserEmail');
		$this->form_validation->set_rules('UserEmail', 'UserEmail', 'required');		
		if ($this->form_validation->run() == TRUE)
		{
			$res = $this->Authmodel->selectPassFromEmail('tbl_admin','Password',array('Email'=>$to_email));
			$pass = $this->encrypt->decode($res);
			if($res)
			{
				$data['subject'] = "Your Password";
				$data['body'] = "Your Current Password Is => <h3>".$pass."</h3>";
				$data['from_email'] = "admin@servicebucket.com";
				$data['from_name'] = "Service Bucket";
				$data['to_email'] = $to_email;
				$res = $this->general->SendEmail($data);
				if($res > 0){
					$this->session->set_flashdata('success','Passwod is Successfully Send To Email Id Check Your Email.');
					redirect(base_url('Auth'));
				} else {
					$this->session->set_flashdata('error','Error occure while sending an Email!');
					redirect(base_url('Auth'));
				}
			} else {
				$this->session->set_flashdata('error','Your Email '.$to_email.' is not in our record !!!!!!!!');
				redirect(base_url('Auth'));
			}
		} else {
			$this->session->set_flashdata('error','Please Enter Email First!!!!!!!!');
			redirect(base_url('Auth'));	
		}
	}
	public function changePassword()
	{
		$this->load->library('encrypt');
		$oldpassword = $this->input->post('OldPassword');
		$newpassword = $this->input->post('NewPassword');
		$checkpassword = $this->encrypt->encode($oldpassword);

		$this->form_validation->set_rules('OldPassword','Old Password','required');
		$this->form_validation->set_rules('NewPassword','Password','required');
		
		if ($this->form_validation->run() == TRUE)
		{
			$res = $this->encrypt->decode($this->Authmodel->checkOldPass('tbl_admin',array()));
			if($res == $checkpassword)
			{
				$this->Authmodel->saveNewPass('tbl_admin',array('Password'=>$newpassword),array('Id'=>$this->session->userdata('Id')));
				$this->session->set_flashdata('success','Password Changed Successfully');
				redirect(base_url('Login'));
			} else {
				$this->session->set_flashdata('error','Your Old Password Should Not Matched!');	
				redirect(base_url('Login'));
			}
		} else {
			$this->session->set_flashdata('error','Please Enter Password And New Password!');
			redirect(base_url('Login'));
		}
	}
}
/* End of file Ajaxlogin.php */
/* Location: ./application/controllers/Ajaxlogin.php */