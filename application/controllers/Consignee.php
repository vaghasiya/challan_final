<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Consignee extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('consignee_list',$data);	
		}

		function add_consignee()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$consignee_name = $this->input->post('consignee_name');
			$consignee_address = $this->input->post('consignee_address');

			$this->form_validation->set_rules('consignee_name', 'Consignee Name', 'required');
			$this->form_validation->set_rules('consignee_address', 'Consignee Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
		        	'consignee_name' => $consignee_name, 
		        	'consignee_address' => $consignee_address, 
		        	'user_id' => $this->session->userdata('login_id'),
		        	'create_date' => date('Y-m-d H:i:s')
	        	);
	          	// echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->insert_record('tbl_consignee',$data);
				if ($record !='') {
					$this->session->set_flashdata('success', 'Consignee Add Successfully....!');
            		redirect($_SERVER['HTTP_REFERER']);	
				}
				else
				{
					$this->session->set_flashdata('error', 'Consignee Not Added....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}	
		}

		function update_consignee()
		{
			$consignee_id = $this->input->post('consignee_id');

			$consignee_name = $this->input->post('consignee_name');
			$consignee_address = $this->input->post('consignee_address');

			$this->form_validation->set_rules('consignee_name', 'consignee Name', 'required');
			$this->form_validation->set_rules('consignee_address', 'consignee Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	        		'consignee_name' => $consignee_name, 
		        	'consignee_address' => $consignee_address,
	        	);  
	            // echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->update_record('tbl_consignee',$data,array('consignee_id'=>$consignee_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'consignee Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'consignee Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_consignee($id)
		{
			$record = $this->Production_model->delete_record('tbl_consignee',array('consignee_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'consignee Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'consignee Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>