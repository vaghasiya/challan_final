<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Consignor extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['consignor_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('consignor_list',$data);	
		}

		function add_consignor()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$consignor_name = ucfirst($this->input->post('consignor_name'));
			$consignor_address = $this->input->post('consignor_address');

			$this->form_validation->set_rules('consignor_name', 'consignor Name', 'required');
			$this->form_validation->set_rules('consignor_address', 'Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	           		'consignor_name' => $consignor_name,
		        	'consignor_address' =>$consignor_address, 
		        	'user_id' => $this->session->userdata('login_id'),
		        	'create_date' => date('Y-m-d H:i:s')
	        	);

	          	// echo "<pre>"; print_r($data); exit;

	          	$get_consignor_name = $this->Production_model->get_all_with_where('tbl_consignor','','',array('consignor_name'=>$consignor_name));

				if ($consignor_name == $get_consignor_name[0]['consignor_name']) {
					$this->session->set_flashdata('error', 'Consignor Name Allredy Exicute....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$record = $this->Production_model->insert_record('tbl_consignor',$data);
					if ($record !='') {
						$this->session->set_flashdata('success', 'Consignor Created Successfully....!');
	            		redirect($_SERVER['HTTP_REFERER']);	
					}
					else
					{
						$this->session->set_flashdata('error', 'Consignor Not Created....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}	
			}	
		}

		function update_consignor()
		{
			$consignor_id = $this->input->post('consignor_id');

			$consignor_name = ucfirst($this->input->post('edit_consignor_name'));
			$consignor_address = $this->input->post('edit_consignor_address');

			$this->form_validation->set_rules('edit_consignor_name', 'consignor Name', 'required');
			$this->form_validation->set_rules('edit_consignor_address', 'consignor Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	           		'consignor_name' => $consignor_name,
		        	'consignor_address' => $consignor_address,
	        	);  
	            // echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->update_record('tbl_consignor',$data,array('consignor_id'=>$consignor_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'consignor Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'consignor Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_consignor($id)
		{
			$record = $this->Production_model->delete_record('tbl_consignor',array('consignor_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Consignor Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Consignor Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>