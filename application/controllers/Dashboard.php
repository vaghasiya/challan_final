<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function index()
	{
		$data['total_warehouse'] = $this->Production_model->get_all_with_where('tbl_warehouse','','',array('user_id'=>$this->session->userdata('login_id')));

		$data['total_consignor'] = $this->Production_model->get_all_with_where('tbl_consignor','','',array('user_id'=>$this->session->userdata('login_id')));

		$data['total_consignee'] = $this->Production_model->get_all_with_where('tbl_consignee','','',array('user_id'=>$this->session->userdata('login_id')));

		$data['total_item'] = $this->Production_model->get_all_with_where('tbl_item','','',array('user_id'=>$this->session->userdata('login_id')));

		$data['total_vehical'] = $this->Production_model->get_all_with_where('tbl_vehical','','',array('user_id'=>$this->session->userdata('login_id')));

		$data['total_user'] = $this->Production_model->get_all_with_where('tbl_user','','',array());

		// echo"<pre>"; print_r($data); exit;
		$this->load->view('dashboard',$data);
	}
}
