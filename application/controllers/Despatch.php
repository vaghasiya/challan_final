<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Despatch extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();

			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['despatch_details'] = $this->Production_model->get_all_with_where('tbl_despatch','despatch_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('despatch',$data);	
		}

		function add_despatch()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$despatch_name = ucfirst($this->input->post('despatch_name'));

			$this->form_validation->set_rules('despatch_name', 'Despatch Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$get_despatch_name = $this->Production_model->get_all_with_where('tbl_despatch','','',array('despatch_name'=>$despatch_name));

				if ($despatch_name == $get_despatch_name[0]['despatch_name']) {
					$this->session->set_flashdata('error', 'Despatch Name Allredy Exicuted....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
		        	$data = array(
			        	'despatch_name' => $despatch_name, 
			        	'user_id' => $this->session->userdata('login_id'),
			        	'create_date' => date('Y-m-d H:i:s')
		        	);
		          	// echo "<pre>"; print_r($data); exit;

					$record = $this->Production_model->insert_record('tbl_despatch',$data);
					if ($record !='') {
						$this->session->set_flashdata('success', 'Despatch Add Successfully....!');
	            		redirect($_SERVER['HTTP_REFERER']);	
					}
					else
					{
						$this->session->set_flashdata('error', 'Despatch Not Added....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}	
			}	
		}

		function update_despatch()
		{
			$despatch_id = $this->input->post('despatch_id');
			$despatch_name = ucfirst($this->input->post('edit_despatch_name'));

			$this->form_validation->set_rules('edit_despatch_name', 'Despatch Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
		        	'despatch_name' => $despatch_name,
	        	);  
	            // echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->update_record('tbl_despatch',$data,array('despatch_id'=>$despatch_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'Despatch Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'Despatch Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_despatch($id)
		{
			$record = $this->Production_model->delete_record('tbl_despatch',array('despatch_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Despatch Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Despatch Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>