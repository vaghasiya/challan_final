<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Destination extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();

			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['destination_details'] = $this->Production_model->get_all_with_where('tbl_destination','destination_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('destination',$data);	
		}

		function add_destination()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$destination_name = ucfirst($this->input->post('destination_name'));

			$this->form_validation->set_rules('destination_name', 'Destination Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$get_destination_name = $this->Production_model->get_all_with_where('tbl_destination','','',array('destination_name'=>$destination_name));

				if ($destination_name == $get_destination_name[0]['destination_name']) {
					$this->session->set_flashdata('error', 'Destination Name Allredy Exicuted....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
		        	$data = array(
			        	'destination_name' => $destination_name, 
			        	'user_id' => $this->session->userdata('login_id'),
			        	'create_date' => date('Y-m-d H:i:s')
		        	);
		          	// echo "<pre>"; print_r($data); exit;

					$record = $this->Production_model->insert_record('tbl_destination',$data);
					if ($record !='') {
						$this->session->set_flashdata('success', 'Destination Add Successfully....!');
	            		redirect($_SERVER['HTTP_REFERER']);	
					}
					else
					{
						$this->session->set_flashdata('error', 'Destination Not Added....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}	
			}	
		}

		function update_destination()
		{
			$destination_id = $this->input->post('destination_id');
			$destination_name = ucfirst($this->input->post('edit_destination_name'));

			$this->form_validation->set_rules('edit_destination_name', 'Destination Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
		        	'destination_name' => $destination_name,
	        	);  
	            // echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->update_record('tbl_destination',$data,array('destination_id'=>$destination_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'Destination Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'Destination Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_destination($id)
		{
			$record = $this->Production_model->delete_record('tbl_destination',array('destination_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Destination Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Destination Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>