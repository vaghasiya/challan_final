<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dompdf_test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->library('dompdf_gen');
	}

	public function index() {	
		// Load all views as normal
		$this->load->view('table');	
	}

	//============== pdf genrate ===========//

	public function create_pdf()
	{
		require_once 'dompdf/autoload.inc.php';

		$dompdf = new Dompdf\Dompdf();
  
 		// $data['get'] = $this->Certificate_model->get_cer_id($id); 
		$html = ($this->load->view('pdf_page',$data,true)); 

        $dompdf->loadHtml($html);
 
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
 
        // Render the HTML as PDF
        $dompdf->render();
 
        // Get the generated PDF file contents
        $pdf = $dompdf->output();

        file_put_contents('PDF'.'/'.$id.'.pdf', $pdf );
        //exit;
 
        // Output the generated PDF to Browser
        $dompdf->stream();
	}
}
