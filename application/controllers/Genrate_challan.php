<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Genrate_challan extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			// $data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['item_details'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			// $data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			$where['tbl_genrate_challan.user_id'] = $this->session->userdata('login_id'); 
			
			$join[0]['table_name'] = 'tbl_consignee';
			$join[0]['column_name'] = 'tbl_consignee.consignee_id = tbl_genrate_challan.consignee_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'tbl_consignor';
			$join[1]['column_name'] = 'tbl_consignor.consignor_id = tbl_genrate_challan.consignor_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'tbl_vehical';
			$join[2]['column_name'] = 'tbl_vehical.vehical_id = tbl_genrate_challan.vehical_id';
			$join[2]['type'] = 'left';

			$join[3]['table_name'] = 'tbl_warehouse';
			$join[3]['column_name'] = 'tbl_warehouse.warehouse_id = tbl_genrate_challan.warehouse_id';
			$join[3]['type'] = 'left';

			$data['challan_details'] = $this->Production_model->jointable_descending(array('tbl_genrate_challan.*','tbl_consignee.consignee_id','tbl_consignee.consignee_name','tbl_consignee.consignee_address','tbl_consignor.consignor_name','tbl_consignor.consignor_address','tbl_vehical.vehical_number','tbl_vehical.vehical_type','tbl_warehouse.warehouse_name','tbl_warehouse.warehouse_address'),'tbl_genrate_challan','',$join,'tbl_genrate_challan.challan_id','desc',$where);	

			$data['invoice_number_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			// echo "<pre>"; echo $this->db->last_query(); print_r($data['invoice_number_details']); exit;

			$this->load->view('challan_genrate',$data);	
		}

		function challan_add(){
			$data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['item_details'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			$this->load->view('add_challan',$data);	
		}

		function add_challan()
		{
			$item_id = $this->input->post('item_id');
			$item_quantity = $this->input->post('item_quantity');
			$item_quantities = count($this->input->post('item_quantity')); 
			
			$invoice_number = $this->input->post('invoice_number');
			$invoice_numberes = count($this->input->post('invoice_number')); 

			$data = array(
		    	'refrence_number' => rand(111,999),
		    	'user_id' =>  $this->session->userdata('login_id'),
		    	'consignor_id' => $this->input->post('consignor_id'),
		    	'consignee_id' => $this->input->post('consignee_id'),
		    	'vehical_id' => $this->input->post('vehical_id'),
		    	'warehouse_id' => $this->input->post('warehouse_id'),
		    	'perticulars_goods' => $this->input->post('perticulars_goods'),
		    	'e_way_bill_no' => $this->input->post('e_way_bill_no'),
		    	'create_date' => date('Y-m-d H:i:s')
		    );
			// echo"<pre>"; print_r($data); exit;

			// $number_allredy = $this->Production_model->get_all_with_where('tbl_genrate_challan','','',array('vehical_id'=> $this->input->post('vehical_id'),'user_id'=>$this->session->userdata('login_id')));

	  //   	if (count($number_allredy) > 0) {
	  //   		$this->session->set_flashdata('error', 'Vehical Number Allredy Exicuted....!');
			// 	redirect($_SERVER['HTTP_REFERER']);
	  //   	}
	  //   	else{
				$record = $this->Production_model->insert_record('tbl_genrate_challan',$data);	
				
				if ($record !='') {	
					if (isset($invoice_numberes)) {		
						for ($i = 0; $i < $invoice_numberes; $i++) {	

						    $invoice_data = array(
						    	'challan_id' => $record,
						    	'user_id'=>$this->session->userdata('login_id'),
						    	'invoice_number' => $invoice_number[$i]
						    );

						    if ($invoice_number[$i] !=null) {
						    	// echo"<pre> insert"; print_r($invoice_data); 
						   		$challan_item = $this->Production_model->insert_record('tbl_invoice_number',$invoice_data);
						    }
						}
					}

					if (isset($item_quantities)) {		
						for ($i = 0; $i < $item_quantities; $i++) {	

						    $item_data = array(
						    	'challan_id' => $record,
						    	'user_id'=>$this->session->userdata('login_id'),
						    	'item_id' => $item_id[$i],
						    	'item_quantity' => $item_quantity[$i]
						    );

						    // $item_allredy = $this->Production_model->get_all_with_where('tbl_challan_item','','',array('challan_item_id'=> $item_id[$i],'user_id'=>$this->session->userdata('login_id')));

						    // echo"<pre>"; echo $this->db->last_query(); print_r($item_allredy);

					   //  	if (count($item_allredy) > 0) {
					   //  		$this->session->set_flashdata('error', 'Item Allredy Exicuted....!');
								// redirect($_SERVER['HTTP_REFERER']);
					   //  	}
					   //  	else{
							    if ($item_quantity[$i] !=null) {
							   		$challan_item = $this->Production_model->insert_record('tbl_challan_item',$item_data);
							    }
					    	// }
						}
						// exit;
					}
					$this->session->set_flashdata('success', 'Challan Create Successfully....!');
					redirect(base_url('Genrate_challan'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Challan Not Created....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			// }	
			// exit;
		}

		function edit_challan($id)
		{
			$data['challan_item_details'] = $this->Production_model->get_all_with_where('tbl_challan_item','challan_item_id','desc',array('challan_id'=>$id ,'user_id'=>$this->session->userdata('login_id'))); 

			$data['invoice_number_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('challan_id'=>$id,'user_id'=>$this->session->userdata('login_id'))); 
			// echo"<pre>"; print_r($data['invoice_number_details']); exit;

			$data['total_item'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			$data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','','',array('user_id'=> $this->session->userdata('login_id'))); 

			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','','',array('user_id'=> $this->session->userdata('login_id')));

			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','','',array('user_id'=> $this->session->userdata('login_id'))); 

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$where['tbl_genrate_challan.user_id'] = $this->session->userdata('login_id'); 
			$where['tbl_genrate_challan.challan_id'] = $id; 
			
			$join[0]['table_name'] = 'tbl_consignee';
			$join[0]['column_name'] = 'tbl_consignee.consignee_id = tbl_genrate_challan.consignee_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'tbl_consignor';
			$join[1]['column_name'] = 'tbl_consignor.consignor_id = tbl_genrate_challan.consignor_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'tbl_vehical';
			$join[2]['column_name'] = 'tbl_vehical.vehical_id = tbl_genrate_challan.vehical_id';
			$join[2]['type'] = 'left';

			$join[3]['table_name'] = 'tbl_warehouse';
			$join[3]['column_name'] = 'tbl_warehouse.warehouse_id = tbl_genrate_challan.warehouse_id';
			$join[3]['type'] = 'left';

			$data['challan_details'] = $this->Production_model->jointable_descending(array('tbl_genrate_challan.*','tbl_consignee.consignee_id','tbl_consignee.consignee_address','tbl_consignor.consignor_name','tbl_consignor.consignor_address','tbl_vehical.vehical_number','tbl_vehical.vehical_type','tbl_warehouse.warehouse_id','tbl_warehouse.warehouse_name','tbl_warehouse.warehouse_address'),'tbl_genrate_challan','',$join,'tbl_genrate_challan.challan_id','desc',$where);	


			// echo"<pre>"; print_r($data['challan_item_details']); exit;

			$this->load->view('challan_edit_details',$data);
		}

		function update_challan()
		{
			$challan_id = $this->input->post('challan_id');

			$consignor_id = $this->input->post('consignor_id');
			$consignee_id = $this->input->post('consignee_id');
			$vehical_id = $this->input->post('vehical_id');
			$warehouse_id = $this->input->post('warehouse_id');
			$e_way_bill_no = $this->input->post('e_way_bill_no');

			$perticulars_goods = $this->input->post('edit_perticulars_goods');

			$this->form_validation->set_rules('consignor_id', 'consignor Name', 'required');
			$this->form_validation->set_rules('consignee_id', 'consignee Address', 'required');
			$this->form_validation->set_rules('vehical_id', 'Vehical Number', 'required');
			$this->form_validation->set_rules('warehouse_id', 'warehouse Name', 'required');
			$this->form_validation->set_rules('edit_perticulars_goods', 'Perticulars goods', 'required');
			$this->form_validation->set_rules('e_way_bill_no', 'E-way bill no', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {	
	        	$temp = $this->input->post('edit_item_id');
				$temp = $this->input->post('edit_item_quantity');

				$invoice_temp = $this->input->post('edit_invoice_number');

				if (isset($temp)) {					
					$item_id = $this->input->post('edit_item_id');
				}
				//echo"<pre>"; print_r($item_id); exit;

				if (isset($temp) && !empty($temp)) {					
					$item_quantity = array_filter($this->input->post('edit_item_quantity'));
				}

				if (isset($invoice_temp)) {					
					$invoice_number = $this->input->post('edit_invoice_number');
				}

				if (isset($invoice_temp) && !empty($invoice_temp)) {					
					$invoice_number = array_filter($this->input->post('edit_invoice_number'));
				}
				// echo"<pre>"; print_r($item_quantity); exit;
	        	
	        	// ================================================================= //
				// ============= Challan Invoice Number update start =============== //
				// ================================================================= //

	        	$count_invc_no = count($invoice_number);  
			    
			    if (isset($count_invc_no)) {		
					for ($i = 0; $i < $count_invc_no; $i++) {

						if ($invoice_number[$i] !='' && !empty($invoice_number[$i]) && isset($invoice_number[$i]))
						{						   
						    $incv_num_data = array(
						        'challan_id' => $challan_id,
						        'user_id' => $this->session->userdata('login_id'),
						        'invoice_number' => $invoice_number[$i]
						    );

						    $invoice_number_id = $this->input->post('invoice_number_id');	 // hidden fields value....! 

						    if (isset($invoice_number_id[$i])) {
						    	// echo "<pre> update"; print_r($incv_num_data);
						    	$invc_num_record = $this->Production_model->update_record('tbl_invoice_number',$incv_num_data,array('invoice_number_id'=> $invoice_number_id[$i]));
						    }
						    else
						    {
						    	$number_allredy = $this->Production_model->get_all_with_where('tbl_invoice_number','','',array('challan_id'=> $challan_id,'invoice_number'=> $invoice_number[$i], 'user_id'=>$this->session->userdata('login_id')));

						    	if (count($number_allredy) > 0) {
						    		$this->session->set_flashdata('error', 'Invoice Number Allredy Exicuted....!');
									redirect($_SERVER['HTTP_REFERER']);
						    	}
						    	// echo "<pre> insert"; print_r($incv_num_data);

					    		$invoice_number_record = $this->Production_model->insert_record('tbl_invoice_number',$incv_num_data);
						    }
						}
					}
				}

				// ================================================================= //
				// ============= Challan Invoice Number update End ================= //
				// ================================================================= //

				// ================================================================= //
				// ================== Challan item update start =================== //
				// ================================================================= //

				$count_item = count($item_quantity);  
				if (isset($count_item)) {		
					for ($i = 0; $i < $count_item; $i++) {

						if ($item_id[$i] !='' && !empty($item_id[$i]) && isset($item_id[$i])) {
						   
						    $challan_item_id = $this->input->post('challan_item_id');	 // hidden fields value....! 

						    if (isset($challan_item_id[$i])) {
						    	// echo "<pre> update"; print_r($item_data);
					    		$item_data = array(
							        'challan_id' => $challan_id,
							        'user_id' => $this->session->userdata('login_id'),
							        'item_id' => $item_id[$i],
							        'item_quantity' => $item_quantity[$i]
							    );
							    
						    	$challan_item_record = $this->Production_model->update_record('tbl_challan_item',$item_data,array('challan_item_id'=> $challan_item_id[$i]));
						    }
						    else
						    {
						    	// echo "<pre> insert"; print_r($item_data);
						    	$add_item_data = array(
							        'challan_id' => $challan_id,
							        'user_id' => $this->session->userdata('login_id'),
							        'item_id' => $item_id[$i],
							        'item_quantity' => $item_quantity[$i]
							    );

						    	$item_allredy = $this->Production_model->get_all_with_where('tbl_challan_item','','',array('challan_id'=> $challan_id, 'item_id'=> $item_id[$i], 'user_id'=>$this->session->userdata('login_id')));

						    	if (count($item_allredy) > 0) {
						    		$this->session->set_flashdata('error', 'Item Allredy Exicuted....!');
									redirect($_SERVER['HTTP_REFERER']);
						    	}

					    		$child_record = $this->Production_model->insert_record('tbl_challan_item',$add_item_data);
						    }
						}
					}
				}
				// exit;
				// ================================================================= //
				// ================== Challan item update End ====================== //
				// ================================================================= //

	        	$data = array(
	           		'consignor_id' => $consignor_id,
		        	'consignee_id' => $consignee_id,
		        	'vehical_id' => $vehical_id,
		        	'warehouse_id' => $warehouse_id,
		        	'perticulars_goods' => $perticulars_goods,
		    		'e_way_bill_no' => $e_way_bill_no
	        	); 

				$record = $this->Production_model->update_record('tbl_genrate_challan',$data,array('challan_id'=>$challan_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'Challan Update Successfully....!');
					redirect(base_url('Genrate_challan'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Challan Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_challan($id)
		{
			$record = $this->Production_model->delete_record('tbl_genrate_challan',array('challan_id'=>$id));
					  $this->Production_model->delete_record('tbl_challan_item',array('challan_id'=>$id));
					  $this->Production_model->delete_record('tbl_invoice_number',array('challan_id'=>$id));

			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Challan Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Challan Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function delete_number($id)
		{
			$record = $this->Production_model->delete_record('tbl_invoice_number',array('invoice_number_id'=>$id));

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Number Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Number Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function delete_item($id)
		{
			$record = $this->Production_model->delete_record('tbl_challan_item',array('challan_item_id'=>$id));

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Item Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Item Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function get_consignee_address()
		{
			$consignee_id = $this->input->post('consignee_id');

			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','','',array('consignee_id'=> $consignee_id,'user_id'=> $this->session->userdata('login_id')));

			// echo "<pre>"; print_r($data); exit;

			if ($data['consignee_details'] !=null) {
				?>
					<div>
						<textarea name="consignee_address" id="consignee_address" class="form-control" value="" placeholder="Address" readonly><?= $data['consignee_details'][0]['consignee_address']?></textarea>
					</div>
				<?php
			}
			else
			{
				echo"";
			}
		}

		function get_consignor_address()
		{
			$consignor_id = $this->input->post('consignor_id');

			$data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','','',array('consignor_id'=> $consignor_id,'user_id'=> $this->session->userdata('login_id')));

			// echo "<pre>"; print_r($data); exit;

			if ($data['consigner_details'] !=null) {
				?>
					<div>
						<textarea name="consigner_address" id="consigner_address" class="form-control" value="" placeholder="Address" readonly><?= $data['consigner_details'][0]['consignor_address']?></textarea>
					</div>
				<?php
			}
			else
			{
				echo"";
			}
		}

		function get_warehouse_address()
		{
			$warehouse_id = $this->input->post('warehouse_id');

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','','',array('warehouse_id'=> $warehouse_id,'user_id'=> $this->session->userdata('login_id')));

			// echo "<pre>"; print_r($data); exit;

			if ($data['warehouse_details'] !=null) {
				?>
					<div>
						<textarea name="warehouse_address" id="warehouse_address" class="form-control" value="" placeholder="Address" readonly><?= $data['warehouse_details'][0]['warehouse_address']?></textarea>
					</div>
				<?php
			}
			else
			{
				echo"";
			}
		}

		function get_vehical_type()
		{
			$vehical_id = $this->input->post('vehical_id');

			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','','',array('vehical_id'=> $vehical_id,'user_id'=> $this->session->userdata('login_id')));

			// echo "<pre>"; print_r($data); exit;
			if ($data['vehical_details'] !=null) {
				?>
					<div>
						<input type="text" name="vehical_type" id="vehical_type" class="form-control" value="<?= $data['vehical_details'][0]['vehical_type']?>" placeholder="Vehical Type" readonly>
					</div>
				<?php
			}
			else
			{
				echo"";
			}
		}

		// pdf genrate to record start //

		public function create_pdf($id)
  		{ 
  			$data['invoice_number_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('challan_id'=>$id,'user_id'=>$this->session->userdata('login_id'))); 
  				
  			$where['tbl_genrate_challan.user_id'] = $this->session->userdata('login_id'); 
			$where['tbl_genrate_challan.challan_id'] = $id; 
			
			$join[0]['table_name'] = 'tbl_consignee';
			$join[0]['column_name'] = 'tbl_consignee.consignee_id = tbl_genrate_challan.consignee_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'tbl_consignor';
			$join[1]['column_name'] = 'tbl_consignor.consignor_id = tbl_genrate_challan.consignor_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'tbl_vehical';
			$join[2]['column_name'] = 'tbl_vehical.vehical_id = tbl_genrate_challan.vehical_id';
			$join[2]['type'] = 'left';

			$join[3]['table_name'] = 'tbl_warehouse';
			$join[3]['column_name'] = 'tbl_warehouse.warehouse_id = tbl_genrate_challan.warehouse_id';
			$join[3]['type'] = 'left';

			$data['create_challan_pdf'] = $this->Production_model->jointable_descending(array('tbl_genrate_challan.*','tbl_consignee.consignee_id','tbl_consignee.consignee_name','tbl_consignee.consignee_address','tbl_consignor.consignor_name','tbl_consignor.consignor_address','tbl_vehical.vehical_number','tbl_vehical.vehical_type','tbl_warehouse.warehouse_name','tbl_warehouse.warehouse_address'),'tbl_genrate_challan','',$join,'tbl_genrate_challan.challan_id','desc',$where);	

			// ================= warehouse item list Start =================== //

			$where1['tbl_challan_item.user_id'] = $this->session->userdata('login_id'); 
			$where1['tbl_challan_item.challan_id'] = $id; 

			$join1[3]['table_name'] = 'tbl_item';
			$join1[3]['column_name'] = 'tbl_item.item_id = tbl_challan_item.item_id';
			$join1[3]['type'] = 'left';

			$data['item_quantity'] = $this->Production_model->jointable_descending(array('tbl_challan_item.*','tbl_item.item_name'),'tbl_challan_item','',$join1,'tbl_challan_item.challan_item_id ','desc',$where1);	

			// echo"<pre>"; print_r($data); exit;

			// ================= warehouse item list End =================== //



			$this->load->library('m_pdf');

			$html = $this->load->view('pdf_genrate/challan',$data,TRUE); //load the pdf_output.php by passing our data and get all data in $html varriable.
			// exit;

	 		$html1 = mb_convert_encoding($html, "Windows-1252", "UTF-8");
			//this the the PDF filename that user will get to download
			$pdfFilePath ="Challan-".time().".pdf";
			//actually, you can pass mPDF parameter on this load() function
			$pdf = $this->m_pdf->load();

	 		$pdf->AddPage('L', // L - landscape, P - portrait 
	        '', '', '', '',
	        10, // margin_left
	        10, // margin right
	        10, // margin top
	        10, // margin bottom
	        10, // margin header
	        10); // margin footer
			//generate the PDF!
			
			$pdf->WriteHTML($html);
			//offer it to user via browser download! (The PDF won't be saved on your server HDD)
			// $pdf->Output($pdfFilePath, "D");
			$pdf->Output(); 	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>