<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Genrate_consignee extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			// $data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['despatch_details'] = $this->Production_model->get_all_with_where('tbl_despatch','despatch_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['destination_details'] = $this->Production_model->get_all_with_where('tbl_destination','destination_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['challan_details'] = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['item_details'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 


			$where['tbl_genrate_consignee.user_id'] = $this->session->userdata('login_id'); 
			
			$join[0]['table_name'] = 'tbl_warehouse';
			$join[0]['column_name'] = 'tbl_warehouse.warehouse_id = tbl_genrate_consignee.warehouse_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'tbl_despatch';
			$join[1]['column_name'] = 'tbl_despatch.despatch_id = tbl_genrate_consignee.despatch_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'tbl_destination';
			$join[2]['column_name'] = 'tbl_destination.destination_id = tbl_genrate_consignee.destination_id';
			$join[2]['type'] = 'left';

			$data['consignee_details'] = $this->Production_model->jointable_descending('*','tbl_genrate_consignee','',$join,'tbl_genrate_consignee.consignee_id','desc',$where);	

			// echo "<pre>"; echo $this->db->last_query(); print_r($data['invoice_number_details']); exit;

			$this->load->view('consignee_genrate',$data);	
		}

		function add(){
			$data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['despatch_details'] = $this->Production_model->get_all_with_where('tbl_despatch','despatch_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['destination_details'] = $this->Production_model->get_all_with_where('tbl_destination','destination_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['challan_details'] = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['item_details'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id'))); 

			$this->load->view('add_consignee',$data);	
		}

		function add_consignee()
		{
			$count_challan_id = count($this->input->post('challan_id')); 

			$challan_id = $this->input->post('challan_id');
			$invoice_date = $this->input->post('invoice_date');
			$amount = $this->input->post('amount');
			$gross_weight = $this->input->post('gross_weight');
			$freight_amount = $this->input->post('freight_amount');
			$remarks = $this->input->post('remarks');

			$data = array(
		    	'user_id' =>  $this->session->userdata('login_id'),
		    	'refrence_number' => rand(111,999),
		    	'warehouse_id' => $this->input->post('warehouse_id'),
		    	'despatch_id' => $this->input->post('despatch_name'),
		    	'destination_id' => $this->input->post('destination_name'),
		    	'create_date' => date('Y-m-d H:i:s')
		    );

			$record = $this->Production_model->insert_record('tbl_genrate_consignee',$data);

			if ($record !='') {	
				$resultSet = Array();

				if (isset($count_challan_id)) {		
					for ($i = 0; $i < $count_challan_id; $i++) {

					    $consignee_data = array(
					    	'consignee_id' => $record,
					    	'user_id'=>$this->session->userdata('login_id'),
					    	'challan_id' => $challan_id[$i],
					    	'invoice_date' => $invoice_date[$i],
					    	'amount' => $amount[$i],
					    	'gross_weight' => $gross_weight[$i],
					    	'freight_amount' => $freight_amount[$i],
					    	'remarks' => $remarks[$i]
					    );

					    if ($invoice_date[$i] !=null) {					    	

					    	// challan no check //
					    	
					    	$challan_no = $this->Production_model->get_all_with_where('tbl_consignee_details','','',array('consignee_id'=> $record, 'challan_id'=> $challan_id[$i], 'user_id'=>$this->session->userdata('login_id')));

					    	// echo"<pre>"; echo $this->db->last_query(); print_r($item_allredy); exit;
					    	foreach ($challan_no as $key => $challan_row) {

					    		$get_challan_no = $this->Production_model->get_all_with_where('tbl_genrate_challan','','',array('challan_id'=> $challan_row['challan_id'], 'user_id'=>$this->session->userdata('login_id')));

					    		foreach ($get_challan_no as $key => $challan_no_row) {
					    			$resultSet[] = $challan_no_row['refrence_number'];
					    		}
					    	}
					    	if (!empty($resultSet)) {
					    		$this->session->set_flashdata('error',"Same challan no. are not added...!");
	            				redirect(base_url('genrate_consignee'));
					    	}
					    	else{
					   			$challan_item = $this->Production_model->insert_record('tbl_consignee_details',$consignee_data);
					   		}
					    }
					}
				}
			
				$this->session->set_flashdata('success', 'Consignee Create Successfully....!');
				redirect(base_url('genrate_consignee'));
			}	
			else
			{
				$this->session->set_flashdata('error', 'Consignee Not Created....!');
				redirect($_SERVER['HTTP_REFERER']);
			}	
		}

		function duplicate_challan(){
			$consignee_id = $this->input->post('consignee_id');
			$challan_id = $this->input->post('challan_id');
			$user_id = $this->input->post('user_id');
			
			$item_allredy = $this->Production_model->get_all_with_where('tbl_consignee_details','','',array('consignee_id'=> $consignee_id, 'challan_id'=> $challan_id, 'user_id'=>$user_id));

	    	if (count($item_allredy) > 0) {
	    		echo "This challan number allredy selected...!";
	    	}
		}

		function edit_consignee($id)
		{
			$data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			// $data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['despatch_details'] = $this->Production_model->get_all_with_where('tbl_despatch','despatch_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['destination_details'] = $this->Production_model->get_all_with_where('tbl_destination','destination_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['challan_details'] = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['total_consignee_details'] = $this->Production_model->get_all_with_where('tbl_consignee_details','consignee_details_id','desc',array('consignee_id'=>$id,'user_id'=>$this->session->userdata('login_id')));

			// echo"<pre>"; print_r($data['challan_details']); exit;			
			
			$data['invoice_no_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$where['tbl_genrate_consignee.user_id'] = $this->session->userdata('login_id'); 
			$where['tbl_genrate_consignee.consignee_id'] = $id; 
			
			$join[0]['table_name'] = 'tbl_warehouse';
			$join[0]['column_name'] = 'tbl_warehouse.warehouse_id = tbl_genrate_consignee.warehouse_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'tbl_despatch';
			$join[1]['column_name'] = 'tbl_despatch.despatch_id = tbl_genrate_consignee.despatch_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'tbl_destination';
			$join[2]['column_name'] = 'tbl_destination.destination_id = tbl_genrate_consignee.destination_id';
			$join[2]['type'] = 'left';

			$data['consignee_details'] = $this->Production_model->jointable_descending(array('tbl_genrate_consignee.*','tbl_warehouse.warehouse_name','tbl_warehouse.warehouse_address','tbl_despatch.despatch_name','tbl_destination.destination_name'),'tbl_genrate_consignee','',$join,'tbl_genrate_consignee.consignee_id','desc',$where);	

			// echo"<pre>"; print_r($data['consignee_details']); exit;

			$this->load->view('consignee_edit_details',$data);
		}

		function update_consignee()
		{
			$consignee_id = $this->input->post('consignee_id');

			$warehouse_id = $this->input->post('warehouse_id');
			$despatch_id = $this->input->post('despatch_id');
			$destination_id = $this->input->post('destination_id');

			$this->form_validation->set_rules('warehouse_id', 'Warehouse Name', 'required');
			$this->form_validation->set_rules('despatch_id', 'Despatch Name', 'required');
			$this->form_validation->set_rules('destination_id', 'Destination Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {	
	        	$temp = $this->input->post('challan_id');
				$temp = $this->input->post('invoice_date');	        	
				$temp = $this->input->post('amount');
				$temp = $this->input->post('gross_weight');
				$temp = $this->input->post('freight_amount');
				$temp = $this->input->post('remarks');

				if (isset($temp)) {					
					$challan_id = $this->input->post('challan_id');
					$invoice_date = $this->input->post('invoice_date');
				}

				// echo"<pre>"; print_r($challan_id); exit;

				if (isset($temp) && !empty($temp))
				{
					$challan_id = array_filter($this->input->post('challan_id'));
					$invoice_date = array_filter($this->input->post('invoice_date'));
					$amount = array_filter($this->input->post('amount'));
					$gross_weight = array_filter($this->input->post('gross_weight'));
					$freight_amount = array_filter($this->input->post('freight_amount'));
					$remarks = array_filter($this->input->post('remarks'));
				}

				// if (isset($amount_temp) && !empty($amount_temp)) {					
				// 	$amount = array_filter($this->input->post('amount'));
				// }

				// echo"<pre>"; print_r($challan_id); exit;
	        	
				// ================================================================= //
				// ================== Consignee item update start =================== //
				// ================================================================= //

				$count_consignee = count($challan_id);
				
				if (isset($count_consignee)) {		
					for ($i = 0; $i < $count_consignee; $i++) {

						if (isset($challan_id[$i])) {
						   // echo"<pre>"; print_r($invoice_date[$i]);

						    $item_data = array(
						        'user_id' => $this->session->userdata('login_id'),
						        'consignee_id' => $consignee_id,
						        'challan_id' => $challan_id[$i],
						        'invoice_date' => $invoice_date[$i],
						        'amount' => $amount[$i],
						        'gross_weight' => $gross_weight[$i],
						        'freight_amount' => $freight_amount[$i],
						        'remarks' => $remarks[$i]
						    );

						    $consignee_details_id = $this->input->post('consignee_details_id');	 // hidden fields value....! 
						    // echo"<pre>"; print_r($consignee_details_id);

						    if (isset($consignee_details_id[$i]) && !empty($consignee_details_id)) {
						    	// echo"<pre>update "; print_r($item_data); 

						    // 	$item_allredy = $this->Production_model->get_all_with_where('tbl_consignee_details','','',array('consignee_id'=> $consignee_id, 'challan_id'=> $challan_id[$i], 'user_id'=>$this->session->userdata('login_id')));

						    // 	if (count($item_allredy) > 0) {
						    // 		if (isset($item_allredy)) {
						    // 			$this->session->set_flashdata('error', 'Challan No. Allredy Exicuted....!');
										// redirect($_SERVER['HTTP_REFERER']);
						    // 		}
						    // 	}
						    // 	else{
							    	$challan_item_record = $this->Production_model->update_record('tbl_consignee_details',$item_data,array('consignee_details_id'=> $consignee_details_id[$i]));
							    // }	
						    }
						    else
						    {
						       	// echo"<pre>insert "; print_r($item_data); 
						    	$item_allredy = $this->Production_model->get_all_with_where('tbl_consignee_details','','',array('consignee_id'=> $consignee_id, 'challan_id'=> $challan_id[$i], 'user_id'=>$this->session->userdata('login_id')));

						    	if (count($item_allredy) > 0) {
						    		$this->session->set_flashdata('error', 'Challan No. Allredy Selected....!');
									redirect($_SERVER['HTTP_REFERER']);
						    	}

					    		$child_record = $this->Production_model->insert_record('tbl_consignee_details',$item_data);
						    }
						}
					}
				}
				// exit;

				// ================================================================= //
				// ================== Consignee item update End ====================== //
				// ================================================================= //

	        	$data = array(
		        	'warehouse_id' => $warehouse_id,
	           		'despatch_id' => $despatch_id,
		        	'destination_id' => $destination_id
	        	); 

				$record = $this->Production_model->update_record('tbl_genrate_consignee',$data,array('consignee_id'=> $consignee_id));

				if ($record == 1) {

					$item_allredy = $this->Production_model->get_all_with_where('tbl_consignee_details','','',array('consignee_id'=> $consignee_id, 'challan_id'=> $challan_id[$i], 'user_id'=>$this->session->userdata('login_id')));

			    	if (count($item_allredy) > 0) {
			    		$this->session->set_flashdata('error', 'Challan No. Allredy Exicuted....!');
						redirect($_SERVER['HTTP_REFERER']);
			    	}

					$this->session->set_flashdata('success', 'Consignee Update Successfully....!');
					redirect(base_url('Genrate_consignee'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Consignee Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_consignee($id)
		{
			$record = $this->Production_model->delete_record('tbl_genrate_consignee',array('consignee_id'=>$id));
					  $this->Production_model->delete_record('tbl_consignee_details',array('consignee_id'=>$id));

			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Consignee Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Consignee Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function delete_consignee_item($id)
		{
			$record = $this->Production_model->delete_record('tbl_consignee_details',array('consignee_details_id '=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Consignee Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Consignee Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function get_challan_details()
		{
			$challan_id = $this->input->post('challan_id');

			$challan_details = $this->Production_model->get_all_with_where('tbl_genrate_challan','','',array('challan_id'=> $challan_id,'user_id'=> $this->session->userdata('login_id')));
			
			// echo"<pre>"; print_r($vehical_number); exit;

			// ====================== Ajax Responce Data =================== //

			if ($challan_details !=null) {	

				$consignee_details = $this->Production_model->get_all_with_where('tbl_consignee','','',array('consignee_id'=> $challan_details[0]['consignee_id'],'user_id'=> $this->session->userdata('login_id')));

				if ($consignee_details[0] !=null) {
				?>
					<div class="col-md-4">
					    <label for="">Consignee Name</label>
					    <input type="hidden" value="<?=$challan_details[0]['vehical_id']?>" class="vehical_id">
						<input type="text" readonly class="form-control" name="consignee_name" placeholder="Consignee Name" value="<?= ($consignee_details !='') ? $consignee_details[0]['consignee_name'] : '';?>">
					</div>

					<div class="col-md-4">
						<label for="">Consignee Address</label>
						<textarea name="consignee_address" id="consignee_address" class="form-control" placeholder="Address" readonly><?= ($consignee_details !='') ? $consignee_details[0]['consignee_address'] : '';?></textarea>
					</div>
				<?php }

				$consignor_details = $this->Production_model->get_all_with_where('tbl_consignor','','',array('consignor_id'=> $challan_details[0]['consignor_id'],'user_id'=> $this->session->userdata('login_id')));

				$invoice_no_details = $this->Production_model->get_all_with_where('tbl_invoice_number','','',array('challan_id'=> $challan_details[0]['challan_id'],'user_id'=> $this->session->userdata('login_id')));

				$where['tbl_genrate_challan.user_id'] = $this->session->userdata('login_id'); 
				$where['tbl_challan_item.user_id'] = $this->session->userdata('login_id'); 
				$where['tbl_genrate_challan.challan_id'] = $challan_id; 
				
				$join[0]['table_name'] = 'tbl_genrate_challan';
				$join[0]['column_name'] = 'tbl_genrate_challan.challan_id = tbl_challan_item.challan_id';
				$join[0]['type'] = 'left';

				$join[1]['table_name'] = 'tbl_item';
				$join[1]['column_name'] = 'tbl_item.item_id = tbl_challan_item.item_id';
				$join[1]['type'] = 'left';

				$total_challan_item = $this->Production_model->jointable_descending(array('tbl_item.item_name','tbl_challan_item.challan_item_id','tbl_challan_item.item_quantity','tbl_genrate_challan.e_way_bill_no'),'tbl_challan_item','',$join,'tbl_challan_item.challan_item_id','desc',$where);	

				// echo"<pre>"; print_r($data['challan_item_details']); exit;

				$invc_no = array();
				foreach ($invoice_no_details as $key => $invc_num_row) {
					array_push($invc_no, $invc_num_row['invoice_number']);
				}
				
				if ($consignor_details[0] !=null) {
				?>
					<div class="col-md-4">
					    <label for="">Consignor Name</label>
						<input type="text" readonly class="form-control" placeholder="Consignor Name" name="consignor_name" value="<?= ($consignor_details !='') ? $consignor_details[0]['consignor_name'] : '';?>">
					</div>

					<div class="col-md-4">
						<label for="">Consignor Address</label>
						<textarea name="consignor_address" id="consignor_address" class="form-control" placeholder="Address" readonly><?= ($consignor_details !='') ? $consignor_details[0]['consignor_address'] : '';?></textarea>
					</div>

					

					<div class="col-md-4">
                    	<label for="">Invoice No</label>
				  		<input type="text" name="invoice_number" id="invoice_number" placeholder="Invoice No" class="form-control" readonly value="<?= ($consignor_details !='') ? implode(' , ', $invc_no) : '';?>"></textarea>	
					</div>

					<?php
						if ($total_challan_item !=null) {
							foreach ($total_challan_item as $key => $item_row) {
							?>
								<div class="col-md-12 row">
									<div class="col-md-3">
				                    	<label for="">Item</label>
								  		<input type="text" placeholder="Item Name" class="form-control" value="<?= $item_row['item_name']?>" readonly></textarea>	
									</div>

									<div class="col-md-3">
				                    	<label for="">Item Quantity</label>
								  		<input type="text" placeholder="Item Quantity" class="form-control" value="<?= $item_row['item_quantity']?>" readonly></textarea>	
									</div>
								</div>	
							<?php }
						}
					?>
					<div class="col-md-4">
                    	<label for="">E-Way Bill No.</label>
				  		<input type="text" class="form-control" readonly value="<?= ($total_challan_item !=null) ? $total_challan_item[0]['e_way_bill_no'] : '';?>">
					</div>
				<?php }
			}
			else
			{
				?>
					<div class="col-md-12 row">
						<div class="col-md-4">
						    <label for="">Consinee Name</label>
							<input type="text" readonly class="form-control" placeholder="Consignee Name">
						</div>

						<div class="col-md-4">
							<label for="">Consignee Address</label>
							<textarea readonly class="form-control" placeholder="Address"></textarea>
						</div>

						<div class="col-md-4">
						    <label for="">Consignor Name</label>
							<input type="text" readonly class="form-control" placeholder="Consignor Name">
						</div>
					</div>

					<div class="col-md-12 row">				
						<div class="col-md-4">
							<label for="">Consignor Address</label>
							<textarea class="form-control" placeholder="Address" readonly></textarea>
						</div>

						<div class="col-md-4">
	                    	<label for="">Invoice No</label>
					  		<input type="text" placeholder="Invoice No" class="form-control" readonly></textarea>	
						</div>

						<div class="col-md-4">
	                    	<label for="">Item</label>
					  		<input type="text" placeholder="Item Name" class="form-control" readonly>	
						</div>
					</div>

					<div class="col-md-12 row">
						<div class="col-md-4">
	                    	<label for="">Item Quantity</label>
					  		<input type="text" placeholder="Item Quantity" class="form-control" readonly>
						</div>
					</div>

					<div class="col-md-4">
                    	<label for="">E-Way Bill No.</label>
				  		<input type="text" class="form-control" placeholder="E-Way Bill No" readonly>
					</div>
				<?php
			}	
			// ====================== Ajax Responce Data End ===================== //
		}
		function get_challan(){
			$PostData = $this->input->post();
			
			$challandata = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('user_id'=>$this->session->userdata('login_id'),'warehouse_id'=>$PostData['warehouse_id']));
			echo json_encode($challandata);
		}
		function get_consignor_address()
		{
			$consignor_id = $this->input->post('consignor_id');

			$data['consigner_details'] = $this->Production_model->get_all_with_where('tbl_consignor','','',array('consignor_id'=> $consignor_id,'user_id'=> $this->session->userdata('login_id')));

			// echo "<pre>"; print_r($data); exit;

			if ($data['consigner_details'] !=null) {
				?>
					<div>
						<textarea name="consigner_address" id="consigner_address" class="form-control" value="" placeholder="Address" readonly><?= $data['consigner_details'][0]['consignor_address']?></textarea>
					</div>
				<?php
			}
			else
			{
				echo"";
			}
		}

		function get_warehouse_address()
		{
			$warehouse_id = $this->input->post('warehouse_id');

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','','',array('warehouse_id'=> $warehouse_id,'user_id'=> $this->session->userdata('login_id')));

			// echo "<pre>"; print_r($data); exit;

			if ($data['warehouse_details'] !=null) {
				?>
					<div>
						<textarea name="warehouse_address" id="warehouse_address" class="form-control" value="" placeholder="Address" readonly><?= $data['warehouse_details'][0]['warehouse_address']?></textarea>
					</div>
				<?php
			}
			else
			{
				echo"";
			}
		}

		// pdf genrate to record start //

		public function create_pdf($id)
  		{ 
  			$data['invoice_no_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id')));

  			$data['total_item'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));

  			// get vehical type details in footer in consignee //

  			$data['vehical_type'] = $this->Production_model->get_all_with_where('tbl_consignee_details','consignee_details_id','desc',array('consignee_id'=>$id,'user_id'=>$this->session->userdata('login_id')));
  			
  			// end //

  			// echo"<pre>"; print_r($data['vehical_type']); exit;

  			$where['tbl_genrate_consignee.consignee_id'] = $id; 
  			$where['tbl_genrate_consignee.user_id'] = $this->session->userdata('login_id'); 
			
			$join[0]['table_name'] = 'tbl_warehouse';
			$join[0]['column_name'] = 'tbl_warehouse.warehouse_id = tbl_genrate_consignee.warehouse_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'tbl_despatch';
			$join[1]['column_name'] = 'tbl_despatch.despatch_id = tbl_genrate_consignee.despatch_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'tbl_destination';
			$join[2]['column_name'] = 'tbl_destination.destination_id = tbl_genrate_consignee.destination_id';
			$join[2]['type'] = 'left';

			$data['consignee_pdf'] = $this->Production_model->jointable_descending('*','tbl_genrate_consignee','',$join,'tbl_genrate_consignee.consignee_id','desc',$where);	

			 //echo"<pre>"; print_r($data); exit;

			// ============== Header part details ================ //

			$temp_consignor = $this->Production_model->get_all_with_where('tbl_consignee_details','consignee_details_id','desc',array('consignee_id'=>$id,'user_id'=>$this->session->userdata('login_id')));

			$con_name = array();
			if ($temp_consignor !=null) {
				foreach ($temp_consignor as $key => $value) {					
					$temp_consignor_name = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('challan_id'=>$value['challan_id'],'user_id'=>$this->session->userdata('login_id')));

					foreach ($temp_consignor_name as $key => $consignor_name_row) {
						
						$consignor_name = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('consignor_id'=>$consignor_name_row['consignor_id'],'user_id'=>$this->session->userdata('login_id')));

						$con_name = array_merge($con_name, $consignor_name);
					}
				}
			}
			$data['consignor_name'] = $con_name;

			// echo"<pre>"; print_r($data); exit;

			// ================= Header part details End =================== //

			// ===================== table data details start ===================== //

			$where1['tbl_consignee_details.consignee_id'] = $id; 
  			$where1['tbl_consignee_details.user_id'] = $this->session->userdata('login_id'); 
			
			$join1[0]['table_name'] = 'tbl_genrate_challan';
			$join1[0]['column_name'] = 'tbl_genrate_challan.challan_id = tbl_consignee_details.challan_id';
			$join1[0]['type'] = 'left';

			$join1[1]['table_name'] = 'tbl_consignee';
			$join1[1]['column_name'] = 'tbl_consignee.consignee_id = tbl_genrate_challan.consignee_id';
			$join1[1]['type'] = 'left';

			// $join[2]['table_name'] = 'tbl_destination';
			// $join[2]['column_name'] = 'tbl_destination.destination_id = tbl_genrate_consignee.destination_id';
			// $join[2]['type'] = 'left';

			$data['consignee_table_data'] = $this->Production_model->jointable_descending(array('tbl_consignee_details.*','tbl_genrate_challan.refrence_number','tbl_consignee.consignee_name','tbl_consignee.consignee_address'),'tbl_consignee_details','',$join1,'tbl_consignee_details.consignee_details_id','desc',$where1);

			// echo"<pre>"; print_r($data['consignee_table_data']); exit;
			// ===================== table data details End ===================== //

			$this->load->library('m_pdf');

			$html = $this->load->view('pdf_genrate/consignee',$data,TRUE); //load the pdf_output.php by passing our data and get all data in $html varriable.
			// exit;

	 		$html1 = mb_convert_encoding($html, "Windows-1252", "UTF-8");
			//this the the PDF filename that user will get to download
			$pdfFilePath ="Challan-".time().".pdf";
			//actually, you can pass mPDF parameter on this load() function
			$pdf = $this->m_pdf->load();

	 		$pdf->AddPage('L', // L - landscape, P - portrait 
	        '', '', '', '',
	        10, // margin_left
	        10, // margin right
	        10, // margin top
	        10, // margin bottom
	        10, // margin header
	        10); // margin footer
			//generate the PDF!
			
			$pdf->WriteHTML($html);
			//offer it to user via browser download! (The PDF won't be saved on your server HDD)
			// $pdf->Output($pdfFilePath, "D");
			$pdf->Output(); 	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>