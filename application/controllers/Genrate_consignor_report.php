<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Genrate_consignor_report extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if (!$this->session->userdata('login_id'))
			{
			  	redirect(base_url('Login'));
			}
		}

		function index()
		{	
			$warehouse_id = $this->input->post('warehouse_id');
			$consignor_id = $this->input->post('consignor_id');

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // use for dropdown list...

			$data['consignor_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // use for dropdown list...

			$data['challan_details'] = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('consignor_id'=>$consignor_id,'warehouse_id'=>$warehouse_id,'user_id'=>$this->session->userdata('login_id')));
			
			// echo"<pre>"; print_r($data['challan_details']); exit;
			$this->load->view('genrate_consignor_report',$data);	
		}

		function consignor_report_details(){

			$warehouse_id = $this->input->post('warehouse_id');
			$consignor_id = $this->input->post('consignor_id');

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // use for dropdown list...

			$data['consignor_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // use for dropdown list...

			$data['total_item'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			
			$data['challan_details'] = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('consignor_id'=>$consignor_id,'warehouse_id'=>$warehouse_id,'user_id'=>$this->session->userdata('login_id')));

			$data['invc_no_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id')));	

			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_genrate_consignee','consignee_id','desc',array('warehouse_id'=>$warehouse_id,'user_id'=>$this->session->userdata('login_id')));

			// echo"<pre>"; print_r($data['consignee_details']); exit;

			$data['despatch_details'] = $this->Production_model->get_all_with_where('tbl_despatch','despatch_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // get despatch name only...

			$data['destination_details'] = $this->Production_model->get_all_with_where('tbl_destination','destination_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // get ddestination name only...

			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['invoice_no_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['consignee_name'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['consignee_ref_no'] = $this->Production_model->get_all_with_where('tbl_consignee_details','consignee_details_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // user for refrence no get in consignee...

			$this->load->view('genrate_consignor_report',$data);
		}


		// excel genrate consignor_by_warehouse_report start //

		function excel_consignor_report(){

			$warehouse_id = $this->input->post('warehouse_id');
			$consignor_id = $this->input->post('consignor_id');

			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // use for dropdown list...

			$data['consignor_details'] = $this->Production_model->get_all_with_where('tbl_consignor','consignor_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // use for dropdown list...

			$data['total_item'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			
			$data['challan_details'] = $this->Production_model->get_all_with_where('tbl_genrate_challan','challan_id','desc',array('consignor_id'=>$consignor_id,'warehouse_id'=>$warehouse_id,'user_id'=>$this->session->userdata('login_id')));

			$data['invc_no_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id')));	

			$data['consignee_details'] = $this->Production_model->get_all_with_where('tbl_genrate_consignee','consignee_id','desc',array('warehouse_id'=>$warehouse_id,'user_id'=>$this->session->userdata('login_id')));

			// echo"<pre>"; print_r($data['consignee_details']); exit;

			$data['despatch_details'] = $this->Production_model->get_all_with_where('tbl_despatch','despatch_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // get despatch name only...

			$data['destination_details'] = $this->Production_model->get_all_with_where('tbl_destination','destination_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // get ddestination name only...

			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['invoice_no_details'] = $this->Production_model->get_all_with_where('tbl_invoice_number','invoice_number_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['consignee_name'] = $this->Production_model->get_all_with_where('tbl_consignee','consignee_id','desc',array('user_id'=>$this->session->userdata('login_id')));

			$data['consignee_ref_no'] = $this->Production_model->get_all_with_where('tbl_consignee_details','consignee_details_id','desc',array('user_id'=>$this->session->userdata('login_id'))); // user for refrence no get in consignee...

			$this->load->view('excel_genrate/consignor_by_warehouse_report',$data);
		}
		// excel genrate report end //
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>