<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Item extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();

			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['item_details'] = $this->Production_model->get_all_with_where('tbl_item','item_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('item_list',$data);	
		}

		function add_item()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$item_name = ucfirst($this->input->post('item_name'));

			$this->form_validation->set_rules('item_name', 'Item Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$get_item_name = $this->Production_model->get_all_with_where('tbl_item','','',array('item_name'=>$item_name));

				if ($item_name == $get_item_name[0]['item_name']) {
					$this->session->set_flashdata('error', 'Item Name Allredy Exicuted....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
		        	$data = array(
			        	'item_name' => $item_name, 
			        	'user_id' => $this->session->userdata('login_id'),
			        	'create_date' => date('Y-m-d H:i:s')
		        	);
		          	// echo "<pre>"; print_r($data); exit;

					$record = $this->Production_model->insert_record('tbl_item',$data);
					if ($record !='') {
						$this->session->set_flashdata('success', 'Item Add Successfully....!');
	            		redirect($_SERVER['HTTP_REFERER']);	
					}
					else
					{
						$this->session->set_flashdata('error', 'Item Not Added....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}	
			}	
		}

		function update_item()
		{
			$item_id = $this->input->post('item_id');
			$item_name = ucfirst($this->input->post('edit_item_name'));

			$this->form_validation->set_rules('edit_item_name', 'Item Name', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
		        	'item_name' => $item_name,
	        	);  
	            // echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->update_record('tbl_item',$data,array('item_id'=>$item_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'Item Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'Item Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_item($id)
		{
			$record = $this->Production_model->delete_record('tbl_item',array('item_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Item Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Item Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>