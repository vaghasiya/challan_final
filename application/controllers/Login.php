<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		error_reporting(0);
		parent::__construct();
		$this->load->library('encryption');	
	}
	
	public function index()
	{
		$this->load->view('login');
	}

	public function dologin()
	{	
		$name = $this->input->post('username');
		$user_email = $this->input->post('user_email');
		$password = $this->input->post('password');	

		// $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
		$this->form_validation->set_rules('user_email', 'User Email', 'required');
		// $this->form_validation->set_rules('warehouse_name', 'Warehouse Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		$user = '';
		if ($this->form_validation->run() == true )
		{
			$password = $this->input->post('password');	
			$admnlogin = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_email'=>$user_email)); 
			
			if (count($admnlogin) == 1){				
				
				$dbpass = $this->encryption->decrypt($admnlogin[0]['admin_password']);
				// echo "<pre>"; print_r($dbpass); print_r($admnlogin); exit;			 
				if($password == $dbpass)
				{ 
					$session = array(
						'login_id' => $admnlogin[0]['admin_id'],
						'user_name' => $admnlogin[0]['admin_name'],
						'user_email' => $admnlogin[0]['admin_email'],
						'status' => $admnlogin[0]['admin_status']
					);	
					$this->session->set_userdata($session);
					$user = 'admin';
					// echo "<pre>"; print_r($dbpass); exit;					 
				}		
			}
			else{
				$usrlogin = $this->Production_model->get_all_with_where('tbl_user','','',array('user_email'=>$user_email));

				if (count($usrlogin) == 1) {

					$usrdbpass = $this->encryption->decrypt($usrlogin[0]['user_password']);
					// echo $usrdbpass = $this->encryption->encrypt('123456');
					// echo"<pre>"; print_r($usrdbpass); exit;
					if($password == $usrdbpass)
					{ 					
						$session = array(
							'login_id' => $usrlogin[0]['user_id'],
							'user_name' => $usrlogin[0]['user_name'],
							'user_email' => $usrlogin[0]['user_email']
						);		
						$this->session->set_userdata($session);
						// echo "<pre>"; exit;
						$this->session->set_flashdata('success', 'User Login Successfully...!');
						redirect(base_url('Dashboard'));
					}							 
				} 
			}	
		}
		else{
			$this->session->set_flashdata('error','Please Enter Username and Passwords...!');
			redirect('Login');
		}	

		if($user == 'admin'){
			$this->session->set_flashdata('success', 'Admin Login Successfully...!');
			redirect(base_url('Dashboard'));
		// echo "string"; exit;	
		}
		elseif($user == 'user'){
			$this->session->set_flashdata('success', 'User Login Successfully...!');
			redirect(base_url('Dashboard'));
		}		
		else{
			$this->session->set_flashdata('error','User email and Passwords Not Match...!');
			redirect(base_url('Login'));	
		}
	}

	public function Logout ()
	{
		$array_items = array('login_id' => '', 'user_name' => '');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		if ($this->session->userdata('status') == '0') {
			redirect(base_url('admin'));		
		}
		else
		{
			redirect(base_url('Login'));		
		}
	}

	public function forgotPassword()
	{
		$user = "";
		$email = $this->input->post('UserEmail');
		
		$this->form_validation->set_rules('UserEmail', 'UserEmail', 'required');		
		if ($this->form_validation->run() == TRUE)
		{
			$admin = $this->common->get_all_record('tbl_admin',array('admin_email' => $email)); 
			
			// echo "<pre>"; print_r($admin); exit;	
			if (count($admin) == 1)
			{	
				$user = 'admin';
			}

			else{
				$this->session->set_flashdata('error','Your Email ' .$email.' is not in our record...!');
				redirect($_SERVER['HTTP_REFERER']);
			}	

			if($user == 'admin'){
				$get_record = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_email'=>$email));
				$pass = $this->encryption->decrypt($get_record[0]['admin_password']);
				// echo $user; echo"<pre>"; print_r($get_record); print_r($pass); exit;

				if($email == $get_record[0]['admin_email'])
				{
					$config = Array(
						'protocol' => 'smtp',
				        'smtp_host' => 'mail.technopus.com',
				        'smtp_port' => 25,
				        'smtp_user' => 'khushi@technopus.com',
				        'smtp_pass' => 'tech@123',
				        'mailtype'  => 'html', 
				        'charset' => 'utf-8',
				        'wordwrap' => TRUE
				    );
						
					$body = "Your Current Password Is => <h3>".$pass."</h3>";
					// echo"<pre>"; print_r($body); exit;

					$this->load->library('email',$config);
					$this->email->from('khushi@technopus.com', 'Khushi Finance');
					$this->email->to($email);						
					$this->email->subject('Forgot Password');
					$this->email->message($body);
					$res = $this->email->send();

					if($res == 1){
						$this->session->set_flashdata('success','Passwod is Successfully Send To Email Id Check Your Email.');
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$this->session->set_flashdata('error','Error occure while sending an Email!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
				else
				{
					$this->session->set_flashdata('error','Your Email'.$to_email.' is not in our record...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
				
			else{
				$this->session->set_flashdata('error','Your Email'.$to_email.' is not in our record...!');
				redirect($_SERVER['HTTP_REFERER']);	
			}
		}
		else
		{
			$this->session->set_flashdata('error','Please Enter Email First...!');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}

	function register()
	{
		$this->load->view('register');	
	}

	function add_register()
	{
		date_default_timezone_set('Asia/Kolkata');   

		$user_name = $this->input->post('user_name');
		$user_email = $this->input->post('user_email');
		$user_address = $this->input->post('user_address');
		$user_password = $this->input->post('user_password');

		$this->form_validation->set_rules('user_name', 'User Name', 'required');
		$this->form_validation->set_rules('user_email', 'User Email', 'required');
		$this->form_validation->set_rules('user_address', 'User Address', 'required');
		$this->form_validation->set_rules('user_password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE)
        {
        	$this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']);	
        }
        else
        {
        	$data = array(
           		'user_name' => $user_name,
	        	'user_email' =>$user_email, 
	        	'user_address' => $user_address,
	        	'user_password' => $this->encryption->encode($user_password),
	        	'create_date' => date('Y-m-d H:i:s')
        	);

          	// echo "<pre>"; print_r($data); exit;

          	$get_user_name = $this->Production_model->get_all_with_where('tbl_user','','',array('user_email'=>$user_email));

			if ($user_email == $get_user_name[0]['user_email']) {
				$this->session->set_flashdata('error', 'User Email Allredy Exicute....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$record = $this->Production_model->insert_record('tbl_user',$data);
				if ($record !='') {
					$this->session->set_flashdata('success', 'Register Successfully....!');
					redirect(base_url('Login'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Not Register....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}	
		}	
	}
}
?>
