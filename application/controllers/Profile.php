<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		error_reporting(0);
		parent::__construct();
		$this->load->model('Profile_Model');
		$this->load->library('encryption');

		if (!$this->session->userdata('login_id'))
		{
		  redirect(base_url('Login'));
		}
	}

	public function index()
	{
		// $data['profile_details'] = $this->Production_model->get_all_with_where('tbl_user',array('user_id' => $this->session->userdata('login_id'))); 
		$user = '';
		$admin = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_id' => $this->session->userdata('login_id'),'admin_email'=>$this->session->userdata('user_email'),'admin_status'=>'0')); 
		// echo"<pre>"; echo $this->db->last_query(); print_r($admin); exit;
		$user = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id' => $this->session->userdata('login_id'))); 

		if (count($admin) == 1)
		{	
			$user = 'admin';
		}

		elseif(count($user) == 1)
		{			
			$user = 'user';
		}

		if($user == 'admin'){		
			$data['get_details'] = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_id' => $this->session->userdata('login_id'),'admin_email'=>$this->session->userdata('user_email'),'admin_status'=>'0'));
			// echo $user; exit;
		}
		elseif($user == 'user'){			
			$data['get_details'] = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id' => $this->session->userdata('login_id'))); 
			// echo $user; exit;
		}
		// echo"<pre>"; print_r($data); exit;
		$this->load->view('profile',$data);
	}

	public function change_password()
	{
		// $data = $this->input->post();
		$old_password = $this->input->post('old_password');
		$new_password = $this->encryption->encrypt($this->input->post('new_password'));

		$this->form_validation->set_rules('old_password','Old Password','required');
		$this->form_validation->set_rules('new_password','New Password','required');

		if ($this->form_validation->run() == TRUE)
		{
			// $user = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id' => $this->session->userdata('login_id'))); 

			$user = '';	
			$admin = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_id' => $this->session->userdata('login_id'),'admin_status' => '0')); 

			$user = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id' => $this->session->userdata('login_id')));

			if (count($admin) == 1)
			{	
				$user = 'admin';
			}

			elseif(count($user) == 1)
			{			
				$user = 'user';
			}

			if($user == 'admin'){	
				$get_record = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_id'=>$this->session->userdata('login_id'),'admin_status'=>'0'));

				$check = $this->encryption->decrypt($get_record[0]['admin_password']);
				// echo $user; echo"<pre>"; print_r($get_record); print_r($check); exit;

				if($check == $old_password)
				{
					$data = array(
						'admin_password' => $new_password,
					);
					// echo"<pre>"; echo $this->db->last_query(); print_r($check); exit;
					$record = $this->Production_model->update_record('tbl_admin',$data,array('admin_id'=>$this->session->userdata('login_id'),'admin_status'=>'0'));

					$this->session->set_flashdata('success','Password Changed Successfully');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error','Your Old Password Should Not Matched');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}

			elseif($user == 'user'){
				
				$get_record = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id'=>$this->session->userdata('login_id')));
				$check = $this->encryption->decrypt($get_record[0]['user_password']);
				// echo $user; echo"<pre>"; print_r($get_record); print_r($check); exit;

				if($check == $old_password)
				{
					$data1 = array(
						'user_password' => $new_password,
					);
					// echo"<pre>"; echo $this->db->last_query(); print_r($check); exit;
					$record = $this->Production_model->update_record('tbl_user',$data1,array('user_id'=>$this->session->userdata('login_id')));

					$this->session->set_flashdata('success','Password Changed Successfully');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error','Your Old Password Should Not Matched');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}

			// if (count($user) == 1)
			// {	
			// 	$get_record = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id'=>$this->session->userdata('login_id')));

			// 	$check = $this->encryption->decrypt($get_record[0]['user_password']);
			// 	// echo $user; echo"<pre>"; print_r($get_record); print_r($check); exit;

			// 	if($check == $old_password)
			// 	{
			// 		// echo"<pre>"; echo $this->db->last_query(); print_r($check); exit;
			// 		$data = array(
			// 			'user_password' => $new_password,
			// 		);
			// 		$record = $this->Production_model->update_record('tbl_user',$data,array('user_id'=>$this->session->userdata('login_id')));
					
			// 		if ($record == 1) {
			// 			$this->session->set_flashdata('success','Password Changed Successfully');
			// 			redirect($_SERVER['HTTP_REFERER']);
			// 		}
			// 		else
			// 		{
			// 			$this->session->set_flashdata('error','Password Not Updated');
			// 			redirect($_SERVER['HTTP_REFERER']);
			// 		}
			// 	}
			// 	else
			// 	{
			// 		$this->session->set_flashdata('error','Your Old Password Should Not Matched');
			// 		redirect($_SERVER['HTTP_REFERER']);
			// 	}
			// }
		}
		else
		{
			$this->session->set_flashdata('error',validation_errors());
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function edit_profile()
	{
		$name = $this->input->post('Name');

		$this->form_validation->set_rules('Name', 'User Name', 'required');

		// echo "<pre>";print_r($data);exit;	

		if ($this->form_validation->run() == TRUE)
		{
			$user = '';

			$admin = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_id' => $this->session->userdata('login_id'),'admin_email'=>$this->session->userdata('user_email'),'admin_status' => '0')); 
			$user = $this->Production_model->get_all_with_where('tbl_user','','',array('user_id' => $this->session->userdata('login_id')));

			if (count($admin) == 1)
			{	
				$user = 'admin';
			}

			elseif(count($user) == 1)
			{			
				$user = 'user';
			}

			if($user == 'admin'){		
				$admin_data = array(
					'admin_name' => $this->input->post('Name'),
					'admin_email' => $this->input->post('Email')
				);

				$record = $this->Production_model->update_record('tbl_admin',$admin_data,array('admin_id'=>$this->session->userdata('login_id'),'admin_status'=>'0'));
				if ($record == 1) {
					$this->session->set_flashdata('success','Profile Updated Successfully');
					redirect(base_url('Login/logout'));
				}
				else{
					$this->session->set_flashdata('error','Profile Not Updated');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
			elseif($user == 'user'){
				$user_data = array(
					'user_name' => $this->input->post('Name'),
					'user_email' => $this->input->post('Email')
				);

				$record = $this->Production_model->update_record('tbl_user',$user_data,array('user_id'=>$this->session->userdata('login_id')));
				if ($record == 1) {
					$this->session->set_flashdata('success','Profile Updated Successfully');
					redirect(base_url('Login/logout'));
				}
				else{
					$this->session->set_flashdata('error','Profile Not Updated');
					redirect($_SERVER['HTTP_REFERER']);
				}
				// echo $user;
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect($_SERVER['HTTP_REFERER']);	
		}
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/Profile.php */
?>