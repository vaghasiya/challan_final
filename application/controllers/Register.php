<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		error_reporting(0);
		parent::__construct();
		$this->load->library('encryption');	
	}
	
	public function index()
	{
		$this->load->view('register');	
	}

	public function register_add()
	{
		$this->load->view('register_add');	
	}

	function add_register()
	{
		date_default_timezone_set('Asia/Kolkata');   

		$user_name = $this->input->post('user_name');
		$user_email = $this->input->post('user_email');
		$user_address = $this->input->post('user_address');
		$user_password = $this->input->post('user_password');

		$this->form_validation->set_rules('user_name', 'User Name', 'required');
		$this->form_validation->set_rules('user_email', 'User Email', 'required');
		$this->form_validation->set_rules('user_address', 'User Address', 'required');
		$this->form_validation->set_rules('user_password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE)
        {
        	$this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']);	
        }
        else
        {
        	$data = array(
           		'user_name' => $user_name,
	        	'user_email' =>$user_email, 
	        	'user_address' => $user_address,
	        	'user_password' => $this->encryption->encrypt($user_password),
	        	'create_date' => date('Y-m-d H:i:s')
        	);

          	// echo "<pre>"; print_r($data); exit;

          	$get_user_name = $this->Production_model->get_all_with_where('tbl_user','','',array('user_email'=>$user_email));

			if ($user_email == $get_user_name[0]['user_email']) {
				$this->session->set_flashdata('error', 'User Email Allredy Exicute....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$record = $this->Production_model->insert_record('tbl_user',$data);
				if ($record !='') {
					$this->session->set_flashdata('success', 'Register Successfully....!');
					redirect(base_url('admin'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Not Register....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}	
		}	
	}
}
?>
