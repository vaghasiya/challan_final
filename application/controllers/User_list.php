<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class User_list extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['user_details'] = $this->Production_model->get_all_with_where('tbl_user','user_id','desc',array());
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('user_list',$data);	
		}

		function delete_user($id)
		{
			$record = $this->Production_model->delete_record('tbl_user',array('user_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'User Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'User Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function add_user()
		{
			$user_name = $this->input->post('user_name');
			$user_email = $this->input->post('user_email');
			$user_password = $this->input->post('user_password');
			$user_address = $this->input->post('user_address');

			$this->form_validation->set_rules('user_name', 'Name', 'required');
			$this->form_validation->set_rules('user_email', 'Email', 'required');
			$this->form_validation->set_rules('user_password', 'Password', 'required');
			$this->form_validation->set_rules('user_address', 'Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	        		'user_name' => $user_name, 
		        	'user_email' => $user_email,
		        	'user_password' => $this->encryption->encrypt($user_password),
		        	'user_address' => $user_address,
		        	'create_date' => date('Y-m-d H:i:s')
	        	);  
	            // echo "<pre>"; print_r($data); exit;

	        	$get_user_name = $this->Production_model->get_all_with_where('tbl_user','','',array('user_email'=>$user_email));

				if ($user_email == $get_user_name[0]['user_email']) {
					$this->session->set_flashdata('error', 'User Email Allredy Exicute....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else{
					$record = $this->Production_model->insert_record('tbl_user',$data);

					if ($record !='') {
						$this->session->set_flashdata('success', 'User Add Successfully....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
					else
					{
						$this->session->set_flashdata('error', 'User Not Added....!');
						redirect($_SERVER['HTTP_REFERER']);
					}	
				}	
			}
		}

		function update_user()
		{
			$user_id = $this->input->post('user_id');

			$user_name = $this->input->post('edit_user_name');
			$user_email = $this->input->post('edit_user_email');
			$user_password = $this->input->post('edit_user_password');
			$user_address = $this->input->post('edit_user_address');

			$this->form_validation->set_rules('edit_user_name', 'Name', 'required');
			$this->form_validation->set_rules('edit_user_email', 'Email', 'required');
			$this->form_validation->set_rules('edit_user_password', 'Password', 'required');
			$this->form_validation->set_rules('edit_user_address', 'Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	        		'user_name' => $user_name, 
		        	'user_email' => $user_email,
		        	'user_password' => $this->encryption->encrypt($user_password),
		        	'user_address' => $user_address
	        	);  
	            // echo "<pre>"; print_r($data); exit;

	        	$get_user_name = $this->Production_model->get_all_with_where('tbl_user','','',array('user_email'=>$user_email));

				$record = $this->Production_model->update_record('tbl_user',$data,array('user_id'=>$user_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'User Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'User Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>