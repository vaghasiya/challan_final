<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Vehical extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();

			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		function index()
		{
			$data['vehical_details'] = $this->Production_model->get_all_with_where('tbl_vehical','vehical_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('vehical_list',$data);	
		}

		function add_vehical()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$vehical_number = $this->input->post('vehical_number');
			$vehical_type = $this->input->post('vehical_type');

			$this->form_validation->set_rules('vehical_number', 'Vehical Number', 'required');
			$this->form_validation->set_rules('vehical_type', 'Vehical Type', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$get_vehical_details = $this->Production_model->get_all_with_where('tbl_vehical','','',array('vehical_number'=>$vehical_number,'vehical_type'=>$vehical_type));
	        	
	        	// echo "<pre>"; echo count($get_vehical_details); print_r($get_vehical_details); exit;

				if (count($get_vehical_details) > 0) {
					$this->session->set_flashdata('error', 'Record Allredy Exicuted....!');
					redirect($_SERVER['HTTP_REFERER']);
				}

				$get_vehical_number = $this->Production_model->get_all_with_where('tbl_vehical','','',array('vehical_number'=>$vehical_number));
	        	
	        	// echo "<pre>"; echo count($get_vehical_details); print_r($get_vehical_details); exit;

				if (count($get_vehical_number) > 0) {
					$this->session->set_flashdata('error', 'Vehical Number Allredy Exicuted....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
		        	$data = array(
			        	'vehical_number' =>$vehical_number, 
			        	'vehical_type' =>$vehical_type, 
			        	'user_id' => $this->session->userdata('login_id'),
			        	'create_date' => date('Y-m-d H:i:s')
		        	);
		          	// echo "<pre>"; print_r($data); exit;

					$record = $this->Production_model->insert_record('tbl_vehical',$data);
					if ($record !='') {
						$this->session->set_flashdata('success', 'Vehical Add Successfully....!');
	            		redirect($_SERVER['HTTP_REFERER']);	
					}
					else
					{
						$this->session->set_flashdata('error', 'Vehical Not Added....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}	
			}	
		}

		function update_vehical()
		{
			$vehical_id = $this->input->post('vehical_id');
			$vehical_number = ucfirst($this->input->post('edit_vehical_number'));
			$vehical_type = $this->input->post('edit_vehical_type');

			$this->form_validation->set_rules('edit_vehical_number', 'Vehical Number', 'required');
			$this->form_validation->set_rules('edit_vehical_type', 'Vehical Type', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
		        	'vehical_number' => $vehical_number,
		        	'vehical_type' => $vehical_type
	        	);  
	            // echo "<pre>"; print_r($data); exit;

	        	$get_vehical_number = $this->Production_model->get_all_with_where('tbl_vehical','','',array('vehical_number'=>$vehical_number));
	        	
	        	// echo "<pre>"; echo count($get_vehical_details); print_r($get_vehical_details); exit;

				if (count($get_vehical_number) > 0) {
					$this->session->set_flashdata('error', 'Vehical Number Allredy Exicuted....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else{
					$record = $this->Production_model->update_record('tbl_vehical',$data,array('vehical_id'=>$vehical_id));

					if ($record == 1) {
						$this->session->set_flashdata('success', 'Vehical Update Successfully....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
					else
					{
						$this->session->set_flashdata('error', 'Vehical Not Updated....!');
						redirect($_SERVER['HTTP_REFERER']);
					}	
				}	
			}
		}

		function delete_vehical($id)
		{
			$record = $this->Production_model->delete_record('tbl_vehical',array('vehical_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 

			if ($record == 1) {
				$this->session->set_flashdata('success', 'Vehical Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Vehical Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		function record_exist(){

			$vehical_no = $this->input->post('edit_vehical_number');

			$get_vehical_no = $this->Production_model->get_all_with_where('tbl_vehical','','',array('vehical_number'=>$vehical_no));
        	// echo"<pre>"; print_r($get_category_name); exit;
			if (count($get_vehical_no) > 0) {
				echo "Number Allredy Exicuted....!";
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>