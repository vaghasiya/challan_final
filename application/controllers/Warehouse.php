<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Warehouse extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();

			if (!$this->session->userdata('login_id'))
			{
			  redirect(base_url('Login'));
			}
		}

		public function index()
		{
					
		}

		function warehouse_list()
		{
			$data['warehouse_details'] = $this->Production_model->get_all_with_where('tbl_warehouse','warehouse_id','desc',array('user_id'=>$this->session->userdata('login_id')));
			// echo "<pre>"; echo $this->db->last_query(); print_r($data); exit;

			$this->load->view('warehouse_list',$data);
		}

		function add_warehouse()
		{
			date_default_timezone_set('Asia/Kolkata');   

			$warehouse_name = ucfirst($this->input->post('warehouse_name'));
			$warehouse_address = $this->input->post('warehouse_address');

			$this->form_validation->set_rules('warehouse_name', 'Warehouse Name', 'required');
			$this->form_validation->set_rules('warehouse_address', 'Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	           		'warehouse_name' => $warehouse_name,
		        	'warehouse_address' =>$warehouse_address, 
		        	'user_id' => $this->session->userdata('login_id'),
		        	'create_date' => date('Y-m-d H:i:s')
	        	);

	          	// echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->insert_record('tbl_warehouse',$data);
				if ($record !='') {
					$this->session->set_flashdata('success', 'Warehouse Created Successfully....!');
            		redirect($_SERVER['HTTP_REFERER']);	
				}
				else
				{
					$this->session->set_flashdata('error', 'Warehouse Not Created....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}	
		}

		function update_warehouse()
		{
			$warehouse_id = $this->input->post('warehouse_id');

			$warehouse_name = ucfirst($this->input->post('edit_warehouse_name'));
			$warehouse_address = $this->input->post('edit_warehouse_address');
			
			$this->form_validation->set_rules('edit_warehouse_name', 'Warehouse Name', 'required');
			$this->form_validation->set_rules('edit_warehouse_address', 'Warehouse Address', 'required');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', validation_errors());
	            redirect($_SERVER['HTTP_REFERER']);	
	        }
	        else
	        {
	        	$data = array(
	           		'warehouse_name' => $warehouse_name,
		        	'warehouse_address' => $warehouse_address
	        	);  
	            // echo "<pre>"; print_r($data); exit;

				$record = $this->Production_model->update_record('tbl_warehouse',$data,array('warehouse_id'=>$warehouse_id));

				if ($record == 1) {
					$this->session->set_flashdata('success', 'Warehouse Update Successfully....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{
					$this->session->set_flashdata('error', 'Warehouse Not Updated....!');
					redirect($_SERVER['HTTP_REFERER']);
				}	
			}
		}

		function delete_warehouse($id)
		{
			$record = $this->Production_model->delete_record('tbl_warehouse',array('warehouse_id'=>$id));
			// echo "<pre>"; print_r($data); exit; 
			if ($record == 1) {
				$this->session->set_flashdata('success', 'Warehouse Deleted Successfully....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('error', 'Warehouse Not Deleted....!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>