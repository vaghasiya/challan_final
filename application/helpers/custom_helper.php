<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /************

    Function To get all menus data

    ************/

    if(!function_exists('get_all_with_helper')){
        function get_all_with_helper($tbl_name = "",$order_by_column = "",$order_by_value = "",$where = ""){
            $ci =& get_instance();

            if($where == ""){
                $where = array();
            }

            $ci->db->order_by($order_by_column,$order_by_value);
            $ci->load->database();
            $ci->db->select('*');
            $ci->db->from($tbl_name);
            $ci->db->where($where);
            $query = $ci->db->get();
            return $query->result_array();
        }
    }

    if(!function_exists('get_all_with_where')){
        function get_all_with_where($column="",$table_name,$order_by_column="",$order_by_value="",$where_array = array())
        {
            $ci =& get_instance();
            if ($column =='') {
                $column = '*';
            }
            $ci->db->order_by($order_by_column,$order_by_value);
            $ci->load->database();
            $ci->db->select($column);
            $ci->db->from($table_name);
            $ci->db->where($where_array);
            $query = $ci->db->get();
            return $query->result_array();
        }
    }



    /************

    Function To Count Rows

    ************/

    if(!function_exists('count_with_helper')){
        function count_with_helper($tbl_name = "",$where = ""){
            $ci =& get_instance();
            if($where == ""){
                $where = array();
            }
            $ci->load->database();
            $ci->db->select('*');
            $ci->db->from($tbl_name);
            $ci->db->where($where);
            $query = $ci->db->get();
            return $query->num_rows();
        }
    }
?>