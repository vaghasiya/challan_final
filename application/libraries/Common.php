<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common {
	public $CI;
	function __construct() {
		$this ->CI =& get_instance();
        $this->CI->load->database();
	}
	public function get_total_no($table,$col,$wh = array()){
		$this->CI->db->select('count('.$col.') as total');
		if(!empty($wh) || count($wh) > 0)
			$this->CI->db->where($wh);
		$query =$this->CI->db->get($table);
		$result= $query->result();
		$res = $result[0]->total;
		return !empty($res)?$res:array();
	}
	//model Query
	public function get_all_record($tablename,$wh = array())
	{
		$this->CI->db->select('*');
		$this->CI->db->from($tablename);
		if(count($wh) > 0 && $wh != '')
			$this->CI->db->where($wh);
		$res = $this->CI->db->get();
		$res = $res->result_array();
		return !empty($res)?$res:array();
	}
	public function get_all_record_selected($tablename,$wh = array(),$select = '*')
	{
		$this->CI->db->select($select);
		$this->CI->db->from($tablename);
		if(count($wh) > 0 && $wh != '')
			$this->CI->db->where($wh);
		$res = $this->CI->db->get();
		$res = $res->result_array();
		return !empty($res)?$res:array();
	}
	public function get_one_row($table,$wh)
	{
		$this->CI->db->select("*");
		$this->CI->db->from($table);
		if(count($wh) > 0 || $wh != '')
			$this->CI->db->where($wh);
	 	$query = $this->CI->db->get();
 		$results = $query->result_array();
 		return !empty($results)?$results[0]:'';
	}
	public function insert_record($tblname,$data)
	{
		$this->CI->db->insert($tblname,$data); 
		return $this->CI->db->insert_id();
	}
	public function update_record($table,$wh,$data)
	{
		if(count($wh) > 0 || $wh != ''){ 
			$this->CI->db->where($wh)
			             ->update($table, $data);
			return $this->CI->db->affected_rows();
		} else {
			return 0;
		}
	}
	public function send_email_to_user($to_email,$bcc = '',$sub,$msg)
	{
		require('assets/phpmailer/class.phpmailer.php');
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug = 0;
			$mail->SMTPAuth = TRUE;
			$mail->SMTPSecure = "tls";
			$mail->Port     = 587;  
			$mail->Username = "info.kaprat@gmail.com";
			$mail->Password = "Kaprat@29";
			$mail->Host     = "smtp.gmail.com";
			$mail->Mailer   = "smtp.gmail.com";
			$e="NoReplay@Mechshop.com";
			$u="Mechshop";
			$mail->SetFrom($e,$u);
			//$mail->AddReplyTo($e,$u);
			$mail->AddAddress($to_email);
			if(!empty($bcc))
				$mail->addBCC($bcc);
			$mail->Subject = $sub;
			$mail->WordWrap   = 80;
			$mail->MsgHTML($msg);
			$mail->IsHTML(true);
			if(!$mail->Send())
				 return false;
			else
				 return true;	
	}
	public function delete_record($table,$wh)
	{
		$this->CI->db->delete($table,$wh);
		$res = $this->CI->db->affected_rows();
		return $res;
	}
	public function cust_query($query){
		$res = $this->CI->db->query($query);
		return $res->result_array();
	}
	public function select_sum_of_colomn($table,$col,$wh = array())
    {
    	$this->CI->db->select_sum($col);
    	$this->CI->db->from($table);
    	if(count($wh) > 0 || $wh != '')
    	$this->CI->db->where($wh);
    	$res = $this->CI->db->get();
    	$res2 = $res->result_array();
    	return !empty($res2)?$res2[0][$col]:'';
    }
    public function batch_insert($table,$data)
	{
		if(count($data) > 0 || $data != ''){
			$this->CI->db-> insert_batch($table, $data);
        	return true;
		} else {
			return false;
		}
	}
	public function batch_update($table,$data)
	{
		if(count($data) > 0){
			return $this->CI->db->update_batch($table,$data);
		} else {
			return false;
		}
	}
	public function already_in_table($table,$colname,$wh,$wh2 = array(),$data = '')
	{
		$this->CI->db->select('*');
		$this->CI->db->from($table);
		if(!empty($colname) && count($wh) > 0)
			$this->CI->db->where_in($colname,$wh);
		if(count($wh2) > 0 || $wh2 != '')
			$this->CI->db->where($wh2);
		$res = $this->CI->db->get();
		$res = $res->result_array();
		if(!empty($data))
			return !empty($res)?$res:array();
		else
			return $this->CI->db->affected_rows();
	}
	public function not_in_table($table,$colname,$wh,$wh2 = array(),$data = '')
	{
		$this->CI->db->select('*');
		$this->CI->db->from($table);
		if(!empty($colname) && count($wh) > 0 || $wh != '')
			$this->CI->db->where_not_in($colname,$wh);
		if(count($wh2) > 0 || $wh2 != '')
			$this->CI->db->where($wh2);
		$res = $this->CI->db->get();
		$res = $res->result_array();
		if(!empty($data))
			return !empty($res)?$res:array();
		else
			return $this->CI->db->affected_rows();
	}
	public function prepare_array_from_table($table,$colname,$wh = array())
    {
    	$result = array();
    	$this->CI->db->select($colname);
    	$this->CI->db->from($table);
    	if(count($wh) > 0 || $wh != '')
    		$this->CI->db->where($wh);
    	$query = $this->CI->db->get();
    	$data = $query->result_array();
    	foreach ($data as $value) {
    		array_push($result, $value[$colname]);
    	}
    	return !empty($result)?$result:array();
    }
	public function jointwotable($table1,$table2,$condition,$where,$wh2 = array())
	{
		$this->CI->db->select('*');
		$this->CI->db->from($table1);
		$this->CI->db->join($table2,$condition,'left');
		$this->CI->db->where($where);
		if(count($wh2 > 0) || $wh2 != '')
			$this->CI->db->where($wh2);
		$res = $this->CI->db->get();
		$result = $res->result_array();
		return !empty($result)?$result:array();
	}
	public function get_one_value($table,$colname,$wh = array())
	{
		$this->CI->db->select('*');
		$this->CI->db->from($table);
		if(count($wh) > 0 || $wh != '')
			$this->CI->db->where($wh);
		$res = $this->CI->db->get();
		$type = $res->result_array();
		return !empty($type)?$type[0][$colname]:'';
	}
	public function get_last_record($table,$colname,$wh = array())
	{
		$this->CI->db->select('*');
		$this->CI->db->from($table);
		if(count($wh) > 0 || $wh != '')
			$this->CI->db->where($wh);
		$this->CI->db->order_by($colname,'DESC');
		$res = $this->CI->db->get();
		$type = $res->result_array();
		return !empty($type)?$type[0]:false;
	}
	public function get_record_in_between($table,$wh = '')
	{
	   $this->CI->db->select('*');
	   $this->CI->db->from($table);
	   if($wh != '' && is_string($wh))
	   		$this->CI->db->where($wh);
		$res = $this->CI->db->get();
		$type = $res->result_array();
		return !empty($type)?$type:array();
	}
}
?>