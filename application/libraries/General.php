<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Latest
class General {
	public $CI;
	function __construct() {
		$this ->CI =& get_instance();
		$this ->CI->load->library('common');
	}
	function active_class($controller){
		return $this->CI->router->fetch_class() == $controller?'selected':'';
	}
	public function CheckAuth()
	{
		$this->CI->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->CI->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->CI->output->set_header('Pragma: no-cache');
		$this->CI->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$controller = $this->CI->router->fetch_class();
		if($this->CI->session->userdata('AdminId')=='' && $this->CI->session->userdata('AdminId') == null){
			$method = $this->CI->router->fetch_method()!='index'?$this->CI->router->fetch_method():'';
			$this->CI->session->set_userdata("last_page", base_url().'/'.$controller.'/'.$method);
			redirect(base_url());
		}
	}
	public function check_day_difference($date,$format = 'Y-m-d')
	{
		$date1 = date_create(date($format));
		$date2 = date_create($date);
		$diff = date_diff($date1,$date2);
		$res = $diff->format("%R%a");
		return $res;
	}
	public function SendEmail($data)
    {
        // /$data = array('from_email'=>'noreply@kaprat.com','to_email'=>'mahendrabagada@gmail.com','subject'=>'Send Email Test','body'=>'Welcome User','from_name'=>'Test');
        $this->CI->load->library('email');
        $subject = $data['subject'];
        $body = $data['body'];
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.kaprat.com',
            'smtp_port' => 26,
            'smtp_user' => 'mahendra.bagada@kaprat.com',
            'smtp_pass' => 'mahendra@29',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8'
        );
        $this->CI->email->initialize($config);
        $from = $data['from_email'];
        $this->CI->email->from($from, $data['from_name']);
        $this->CI->email->to($data['to_email']);
        $this->CI->email->subject($subject);  
        $this->CI->email->message($body);
        if(isset($data["attachment"])){
            $this->CI->email->attach($data["attachment"]);
        }
        if ( !$this->CI->email->send())
        {
            echo $this->CI->email->print_debugger();exit;
            return false;
        } else {
            return true;
        }
    }
	//countroller Function
	public function processandredirect($res,$success,$error,$cname)
	{
		if($res > 0)
		{
			$this->CI->session->set_flashdata('phpsuccess', $success);
			redirect(base_url($cname));
		}
		else
		{
			$this->CI->session->set_flashdata('phperror',$error);
			redirect(base_url($cname));
		}
	}
	public function delete_record($data)
	{
		$arr = explode(",",base64_decode($data));
		$id = $arr[0];
		$table = $arr[1];
		$columnname = $arr[2];
		$controller = $arr[3];
		$aff_row = $this->CI->common->delete_record($table,array($columnname=>$id));
		$this->processandredirect($aff_row,'Record Deleted Successfully','Record Not Deleted !!!',$controller);
	}
	public function active_or_deactive($data)
	{
		//$delete = $user['UserId'].',tbluser,UserId,UserStatus,0,User';
		$arr = explode(",",base64_decode($data));
		$id = $arr[0];
		$table = $arr[1];
		$colname = $arr[2];
		$activecolname = $arr[3];
		$zeroorone = $arr[4];
		$controller=$arr[5];
		if($zeroorone == 0)
			$res = "DeActivated";
		else
			$res = "Activated";
		$data1 = array( $activecolname =>$zeroorone );
		$aff_row = $this->CI->common->update_record($table,array($colname=>$id),$data1);
		$this->processandredirect($aff_row,'Record '.$res.' Successfully !!!','Record Not '.$res.' !!!',$controller);
	}
	public function upload_file($field_name, $upload_path, $file_name, $allowed_types)
    {
    	$this->CI->load->library('upload');
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, TRUE);
             @chmod($upload_path,0777);
        }
        $config['file_name'] = $file_name;
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = $allowed_types;
        $config['overwrite'] = true;
        $config['remove_spaces'] = TRUE;
        $this->CI->upload->initialize($config);

        $FileName = "";
        if ($this->CI->upload->do_upload($field_name)) {
            $file = $this->CI->upload->data();
            $FileName = $file['file_name'];
            chmod($file['full_path'],0777);
        } else {
            $error = $this->CI->upload->display_errors();
            return $error;
        }
        return $FileName;
    }
    public function decodeImage($image_string,$path,$name){
		$image_string = base64_decode($image_string);
		$im = imagecreatefromstring($image_string);
		if ($im !== false) {
			if(!file_exists($path)){
				mkdir($path, 0777, true);
			}
			$full_path = $path.$name;
			header('Content-Type: image/png');
			imagepng($im,$full_path);
			imagedestroy($im);
			return $full_path;
		}
	}
}
?>