<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Authmodel extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function checkAuth($table,$wh = array() )
	{
		$this->db->select("*");
		$this->db->from($table);
		if(count($wh) > 0 || $wh != '')
			$this->db->where($wh);
	 	$query = $this->db->get();
 		$results = $query->result_array();
 		return !empty($results)?$results[0]:false;
	}
	public function selectPassFromEmail($table,$passcoll,$wh = array())
	{
		$this->db->select($passcoll);
        $this->db->from($table);
        $this->db->where($wh);
	    $query = $this->db->get();
	    if($query->num_rows() > 0){
		 	$data = $query->row_array();
			$value = $data[$passcoll];
			return $value;
		} else {
			return false;
		}
	}
	public function checkOldPass($table,$passcoll,$wh = array())
	{
        $this->db->select($passcoll);
        $this->db->from($table);
        $this->db->where($wh);
	    $query = $this->db->get();
	    if($query->num_rows() > 0)
		{
		 	$data = $query->row_array();
			$value = $data[$passcoll];
			return $value;
		}
	}
	public function saveNewPass($table,$data,$wh = array())
	{
	    $this->db->where($wh);
	    $this->db->update($table, $data);
	    $row = $this->db->affected_rows();
	    return $row;
	}
}

/* End of file Loginmodal.php */
/* Location: ./application/models/Loginmodal.php */