<?php 
class Crud extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $table_name
     * @param $data_array
     * @return bool
     */
    function insert($table_name,$data_array){
        if($this->db->insert($table_name,$data_array))
        {
            return $this->db->insert_id();
        }
        return false;
    }
        
        function insertFromSql($sql)
        {
            $this->db->query($sql);
            return $this->db->insert_id();
        }

        function execuetSQL($sql){
            $this->db->query($sql);
        }
    function getFromSQL($sql)
        {
        return $this->db->query($sql)->result();
    }

    /**
     * @param $table_name
     * @param $order_by_column
     * @param $order_by_value
     * @return bool
     */
    function get_all_records($table_name,$order_by_column = array(),$order_by_value = array()){
        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->order_by($order_by_column,$order_by_value);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    /**
     * @param $table_name
     * @param $order_by_column
     * @param $order_by_value
     * @param $where_array
     * @return bool
     */
    function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array = "",$limit = ''){
        
        // echo "<pre>";print_r($where_array);exit;
        $where_array = ($where_array != "") ? $where_array : array();
        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($where_array);
        $this->db->order_by($order_by_column,$order_by_value);
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    function get_all_with_like($table_name,$order_by_column,$order_by_value,$like_array,$limit = '',$where = array()){
        
        // echo "<pre>";print_r($where_array);exit;

        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->like($like_array);
        $this->db->order_by($order_by_column,$order_by_value);
        $this->db->where($where);
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    function get_all_with_where_group_by($table_name,$order_by_column,$order_by_value,$where_array,$group_by = ''){
        
        // echo "<pre>";print_r($where_array);exit;

        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($where_array);
        $this->db->order_by($order_by_column,$order_by_value);
        $this->db->group_by($group_by);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    /**
     * @param $tbl_name
     * @param $column_name
     * @param $where_id
     * @return mixed
     */


    function count_with_where($table_name,$where_array){
        
        // echo "<pre>";print_r($where_array);exit;

        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($where_array);
        $query = $this->db->get();
        return $query->num_rows();
    }


    function get_column_value_by_id($tbl_name,$column_name,$where_id)
    {               
        $this->db->select("*");
        $this->db->from($tbl_name);
        $this->db->where($where_id);        
        $this->db->last_query();
        $query = $this->db->get();
        return $query->row($column_name);
    }

    /**
     * @param $table_name
     * @param $where_id
     * @return mixed
     */
    function get_row_by_id($table_name,$where_id){
        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($where_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    /**
     * @param $table_name
     * @param $where_id
     * @return mixed
     */
    function get_rows_by_id($table_name, $column_name, $where_id){
        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where_in($column_name, $where_id);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * @param $table_name
     * @param $where_array
     * @return mixed
     */
    function delete($table_name,$where_array){      
        $result = $this->db->delete($table_name,$where_array);
        return $result;
    }

    /**
     * @param $table_name
     * @param $data_array
     * @param $where_array
     * @return mixed
     */
    function update($table_name,$data_array,$where_array){
        $this->db->where($where_array);
        $rs = $this->db->update($table_name, $data_array);
        return $rs;
    }

    /**
     * @param $name
     * @param $path
     * @return bool
     */
    function upload_file($name, $path)
    {
        $config['upload_path'] = $path;
        $config ['allowed_types'] = '*';
        $this->upload->initialize($config);
        if($this->upload->do_upload($name))
        {
            $upload_data = $this->upload->data();
            return $upload_data['file_name'];
        }
        return false;
    }

    
    /**
     * @param $table
     * @param $id_column
     * @param $column
     * @param $column_val
     * @return null
     */
    function get_id_by_val($table,$id_column,$column,$column_val){
        $this->db->select($id_column);
        $this->db->from($table);
        $this->db->where($column,$column_val);
        $this->db->limit('1');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row()->$id_column;
        } else {
            return null;
        }
    }
    
    
    function limit_words($string, $word_limit=30){
        $words = explode(" ",$string);
        return implode(" ", array_splice($words, 0, $word_limit));
    }
    function limit_character($string, $character_limit=30){
        if (strlen($string) > $character_limit) {
            return substr($string, 0, $character_limit).'...';
        }else{
            return $string;
        }
    }
    //select data 
    function get_select_data($tbl_name)
    {
        $this->db->select("*");
        $this->db->from($tbl_name);
        $query = $this->db->get();
        return $query->result();
    }
    function get_data_row_by_id($tbl_name,$where,$where_id)
    {
        $this->db->select("*");
        $this->db->from($tbl_name);
        $this->db->where($where,$where_id);
        $query = $this->db->get();
        return $query->row();
    }
    function get_result_where($tbl_name,$where,$where_id)
    {
        $this->db->select("*");
        $this->db->from($tbl_name);
        $this->db->where($where,$where_id);
        $query = $this->db->get();
        return $query->result();
    }
    function get_all_cat($table,$id,$name){
        $query = $this->db->query("SELECT `" . $id . "`  as id,`" . $name . "` as text FROM `" . $table . "` ORDER BY `" . $name . "` ASC");
        return $query->result();
    }
    function get_duplicate($tbl_name,$where)
    {
        $this->db->select("*");
        $this->db->from($tbl_name);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }
    function get_duplicate_no($tbl_name,$where)
    {
        $this->db->select("*");
        $this->db->from($tbl_name);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }

    // get all sub_category
    public function get_all_sub_category($where = "", $group_by = "", $order_by_column = ""){
        if($where == ""){
            $where = array();
        }
        if($order_by_column == ""){
            $order_by_column = "sub_category.sub_category_name";
        }
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->order_by($order_by_column,'ASC');
        $this->db->join('category','sub_category.category_id = category.category_id','left');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    // get all product
    public function get_all_product($where = "", $group_by = "", $order_by_column = "",$params = ""){
        if($where == ""){
            $where = array();
        }
        if($order_by_column == ""){
            $order_by_column = "product.product_name";
        }
        if($params != ""){
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
        }
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->order_by($order_by_column,'ASC');
        $this->db->join('sub_category','product.sub_category_id = sub_category.sub_category_id','left');
        $this->db->join('category','sub_category.category_id = category.category_id','left');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    // get all project_image
    public function get_all_project_image($where = "", $group_by = "", $order_by_column = ""){
        if($where == ""){
            $where = array();
        }
        if($order_by_column == ""){
            $order_by_column = "project_image.project_image_id";
        }
        $this->db->select('*');
        $this->db->from('project_image');
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->order_by($order_by_column,'DESC');
        $this->db->join('project','project_image.project_id = project.project_id','left');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    // get all product
    public function get_all_product_image($where = "", $group_by = "", $order_by_column = "",$params = ""){
        if($where == ""){
            $where = array();
        }
        if($order_by_column == ""){
            $order_by_column = "product.product_name";
        }
        if($params != ""){
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
        }
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->join('sub_category','product.sub_category_id = sub_category.sub_category_id','left');
        $this->db->join('product_image','product.product_id = product_image.product_id','left');
        $this->db->join('category','sub_category.category_id = category.category_id','left');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }
}
