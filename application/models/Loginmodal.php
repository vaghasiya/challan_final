<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loginmodal extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function adminlogin($name)
	{
		$this->db->select('*');
		$this->db->from('tbl_admin');
		$this->db->where('admin_name',$name);
		// $this->db->where('Password',$password);
		$select_query = $this->db->get();
		return $select_query->result_array();
	}

	public function userlogin($name)
	{
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('user_name',$name);
		$query = $this->db->get();
		// echo $this->db->last_query();	
		return $query->result_array();
	}

	// public function changepassword()
	// {
	// 	$var = $this->session->userdata;
	// 	$var['user_id'];
	// 	$this->db->select('user_password');
	// 	$this->db->from('tbl_user');
	// 	$this->db->where('user_id',$var['user_id']);
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }
}

/* End of file Loginmodal.php */
/* Location: ./application/models/Loginmodal.php */