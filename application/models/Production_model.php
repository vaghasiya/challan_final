<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production_model extends CI_Model {

	function insert_record($table_name,$data)
	{
		$record = $this->db->insert($table_name,$data);
		return $this->db->insert_id();
	}

	function get_all_with_where($table_name,$order_by_column="",$order_by_value="",$where_array)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->result_array();
	}

	function get_where_user($column="",$table_name,$order_by_column="",$order_by_value="",$where_array = array())
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->result_array();
	}

	function get_all_record($column="", $table_name, $group_by="")
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		$this->db->group_by($group_by);
		$record = $this->db->get();
		return $record->result_array();
	}

	function count_num_of_rows($column="",$table_name,$where_array)
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->num_rows();
	}

	function update_record($table_name,$data_array,$where_array)
	{
		$this->db->where($where_array);
		$this->db->update($table_name,$data_array);
		// echo $this->db->last_query();
		return TRUE;
	}

	function jointable($table,$join = "",$where = "",$or = "",$group_by = '')
	{
		$this->db->select('*');
		$this->db->from($table);
		if($where != ""){
			$this->db->where($where);
		}
		if($or != "")
		{
			$this->db->or_where($or);
		}
		if($group_by != ""){
			$this->db->group_by($group_by);
		}
		if($join != ""){
			foreach ($join as $join_row) {
				$this->db->join($join_row['table_name'],$join_row['column_name'],$join_row['type']);
			}
		}
		$record = $this->db->get();
		return $record->result_array();
	}

	function jointable_descending($column="",$table, $like_array="", $join = "",$order_by_column = "", $order_by_value = "", $where = "", $where_or = array(), $group_by = array())
	{
		if ($column =='') {
			$column = '*';
		}

		$this->db->select($column);
		$this->db->from($table);

		if($like_array != ""){
			$this->db->like($like_array);
		}

		if ($group_by !='') {
			$this->db->group_by($group_by);
		}
		if($where != ""){
			$this->db->where($where);
		}
		

		if($order_by_column != "" || $order_by_value !="")
		{
			$this->db->order_by($order_by_column,$order_by_value);
		}

		if($join != ""){
			foreach ($join as $join_row) {
				$this->db->join($join_row['table_name'],$join_row['column_name'],$join_row['type']);
			}
		}
		$record = $this->db->get();
		return $record->result_array();
	}

	//============ End ==========//

	function delete_record($table_name,$where_array){      
        $result = $this->db->delete($table_name,$where_array);
        // print_r($result); exit;
        return $result;
    }

    function get_all_with_like($table_name,$like_array){

        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->like($like_array);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }

    function search_product($search)
    {    	
    	$this->db->select('*');
		$this->db->from('product as p');
		$this->db->join('product_image as pimg','p.product_id = pimg.product_id');
		$this->db->join('tax_rate as rate','rate.tax_rate_id = p.tax_rate_id');
		$this->db->like('product_name',$search);
		$this->db->group_by('pimg.product_id');
		$record = $this->db->get();
		return $record->result_array();
    }
}

/* End of file Product_model.php */
/* Location: ./application/models/Product_model.php */