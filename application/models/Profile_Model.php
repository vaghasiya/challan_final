<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function get_result()
	{
		$res = $this->db->select('*')
				 ->from('tbl_admin')
				 ->where('admin_id',$this->session->userdata('login_id'))
				 ->get();
		$ar = $res->result_array();
		return $ar[0];
	}
	public function checkOldPass()
	{
        $this->db->select('*');
        $this->db->from('tbl_admin');
        $this->db->where('admin_id', $this->session->userdata('login_id'));
	    $query = $this->db->get();
	    if($query->num_rows() > 0)
		{
		 	$data = $query->row_array();
			$value = $data['Password'];
			return $value;
		}
	}
	public function saveNewPass($new_pass)
	{
	    $data = array('Password' => $new_pass);
	    $this->db->where('AdminId', $this->session->userdata('login_id'));
	    $this->db->update('tbl_admin', $data);
	    return true;
	}
	public function update_profile($data)
	{
		 $this->db->where('AdminId', $this->session->userdata('login_id'));
	    $this->db->update('tbl_admin', $data);
	    return true;
	}
	public function select_pass_from_email($email)
	{
		$this->db->select('*');
        $this->db->from('tbl_admin');
        $this->db->where('Email', $email);
	    $query = $this->db->get();
	    if($query->num_rows() > 0)
		{
		 	$data = $query->row_array();
			$value = $data['Password'];
			return $value;
		}
		else
			return false;
	}
}

/* End of file Profile_Model.php */
/* Location: ./application/models/Profile_Model.php */