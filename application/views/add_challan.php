<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i');
	body{font-family: 'Roboto', sans-serif;}
	.main-container{max-width: 90%;margin: auto;}
	.main-border{border: 1px solid #000000;}
	.b-bottom {border-bottom: 1px solid #000000;}
	.b-right {border-right: 1px solid #000000;}
	.lh-32{line-height: 32px;}
	.t-color{color: #f0ffff00;}
	.tb{border-spacing: 0px;}
	.tb-bordered td, .tb-bordered th {
		text-align: center;
		border-right: 1px solid #000000;
	    border-bottom: 1px solid #000000;
	}
	.b-right-0{border-right:0 !important;}
	.bg-1{background-color: antiquewhite;}
	* {box-sizing: border-box;}
	.line {display: flex;flex-wrap: wrap;}
	.header-c {max-width: 33.333333%;text-align: center;margin-left: 33.333333%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;}
	.header-r{max-width: 33.333333%;position: relative;width: 100%;min-height: 1px;padding: 29px;text-align:right}
	.heading-l{max-width: 50%;position: relative;width: 100%;padding: 15px;}
	.heading-l p{margin:5px 0;}
	.heading-r{max-width: 50%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;}
	.heading-r .first-p{display: inline;margin: 0;}
	.invoice{padding: 5px 20px;}
	.footer h4 {width:100%;}
	.footer{padding: 0 16px;text-align: right;width: 100%; background-color: #cacaca; }
	.item-name p{border-bottom: 1px solid;padding: 10px;margin: 0;}
	.bd-0{border-bottom:0 !important;}
</style>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Add Challan  
				</div>
				<div class="modal-body">
	                <div class="modal-body">
		                <div class="row">
		                	<div class="main-container">
								<div class="main-border">
									<form action="<?= base_url('Genrate_challan/add_challan')?>" method="post" id="add_challan">
										<div class="b-bottom">
											<div class="line bg-1">
												<div class="header-c">
													<h3>
														CHALLAN
													</h3>
													<div class="form-group">
				                                    	<label for="">Warehouse Name</label>
													  	<select class="form-control" id="warehouse_id" name="warehouse_id" style="border-radius: 0; border-style: solid;">
														    <option value="">Select</option>
														    <?php
														    	if ($warehouse_details !=null) {
														    		foreach ($warehouse_details as $key => $value) {
														    		?>
														    			<option value="<?= $value['warehouse_id']?>"><?= $value['warehouse_name']?></option>
														    		<?php }
														    	}
														    ?>
														</select>
													</div> 
													<div class="col-md-12">
						                                <label for="">Warehouse Address</label>
						                                <div class="form-group">
															<div class="warehouse_address_view">
																<textarea class="form-control" value="" placeholder="Address" readonly></textarea>
															</div>
						                                </div>
					                            	</div>
												</div>
											</div>
										</div>
										<div class="line b-bottom">
											<div class="heading-l b-right">
												<p>CONSIGNOR NAME: </p>
												<select class="form-control" id="consignor_id" name="consignor_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($consigner_details !=null) {
												    		foreach ($consigner_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['consignor_id']?>"><?= $value['consignor_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>

												<p class="t-center">CONSIGNOR ADDRESS: </p>
												<div class="address_view">
													<textarea class="form-control" value="" placeholder="Address" readonly></textarea>
												</div>
											</div>
											
											<div class="heading-r"><br>
												<p class="text-bold first-p">CONSIGNEE NAME:	</p>
												<select class="form-control" id="consignee_id" name="consignee_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($consignee_details !=null) {
												    		foreach ($consignee_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['consignee_id']?>"><?= $value['consignee_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select><br>
												<div class="form-group">
													<p class="text-bold first-p">CONSIGNEE ADDRESS:	</p>
													<div class="consignee_address_view">
														<textarea class="form-control" value="" placeholder="Address" readonly></textarea>
													</div>
				                                </div>
											</div>
										</div>
										<div class="line">
											<div class="col-md-6">
												<div class="container2">
		                            				<div class="append_item">
												  	<table class="" cellpadding="10" width="100%">
														<thead>
														  <tr>
															<th>Item name</th>
															<th>Qnty.</th>
															<!-- <th class="b-right-0">Particulars of Goods</th> -->
														  </tr>
														</thead>
														<tbody>
														
														  	<!-- <tr> -->
															  	<div class="col-md-2">
																	<td class="border-l-0 item-name" style="padding:0 29px 0 3px;">	
																		<select class="form-control" id="item_id" name="item_id[]">
																		    <option value="">Select</option>
																		    <?php
																		    	if ($item_details !=null) {
																		    		foreach ($item_details as $key => $value) {
																		    		?>
																		    			<option value="<?= $value['item_id']?>"><?= $value['item_name']?></option>
																		    		<?php }
																		    	}
																		    ?>
																		</select>
																	</td>
																</div>

																<div class="col-md-3">
																	<td class="border-l-0 item-name" style="padding:0 29px 0 0;">
																		<input type="text" class="form-control digits" name="item_quantity[]" value="" style="max-width: 50px;">
																	</td>
																</div>
															<!-- </tr> -->
															<!-- <td class="b-right-0">
														  		<textarea name="perticulars_goods" id="perticulars_goods" class="" placeholder="Perticulars Of Goods" style="min-width: 90%"></textarea>
															</td> -->
														</tbody>
												  	</table>	
													</div>
													<div style="margin: -25px 4px 0px 80%;position: absolute;">
				                            			<td class="border-l-0 item-name">
					                            			<label></label>
					                            			<button type="button" class="btn btn-primary newitem btn-xs" >Add +</button>
				                            			</td>
				                            		</div>
												</div>
											</div>

											<div class="col-md-6">
												<label>Perticulars Of Goods</label>
												<textarea name="perticulars_goods" id="perticulars_goods" class="" placeholder="Perticulars Of Goods" style="min-width: 90%; min-height: 100px;"></textarea>	
											</div>
										</div>
										<div class="line b-bottom invoice">
											<div class="container3">
				                            	<div class="append_invoice">
					                            	<div style="max-width: 200px; padding: 0px 10px 0px 13px; ">
					                            		<div class="form-group row">
					                                    	<label for="">Invoice Number</label>
														  	<input type="text" name="invoice_number[]" id="invoice_number" class="form-control digits" placeholder="Invoice Number">
														</div> 
													</div>
												</div>
											</div>
			                            	<div class="col-md-1" style="margin:5px 0 0 0;">
			                            		<label></label><br>
			                            		<button type="button" class="btn btn-primary btn-sm new_invoice">+</button>
			                            	</div>

			                            	<div class="col-md-3">
												<label>E-Way Bill no.</label>
												<input type="text" class="form-control" name="e_way_bill_no" id="e_way_bill_no">
											</div>
										</div>

										<div class="line b-bottom invoice">
											<div class="form-group">
												<label>Vehicle  No</label> 
												<select class="form-control" id="vehical_id" name="vehical_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($vehical_details !=null) {
												    		foreach ($vehical_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['vehical_id']?>"><?= $value['vehical_number']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div>
											<div class="col-md-4">
			                            		<div class="form-group">
			                                    	<label for="">Vehical Type</label>
			                                    	<div class="vehical_type_view">
												  		<input type="text" readonly class="form-control" placeholder="Vehical Type">
												  	</div>
												</div> 
											</div>

											<div class="col-md-12 col-lg-12 row">
				                                <button type="submit" id="EditC" class="btn btn-sm btn-success check">Submit</button>
				                                <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">Cancel</button>
					                        </div>
										</div>
									</form>
								</div>
							</div>
		                </div>
		            </div>
	            </div>
			</div>
		</div>
	</div>

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('warehouse_id') || isemptyfocus('consignor_id') || isemptyfocus('consignee_id') || isemptyfocus('item_id') ||  isemptyfocus('perticulars_goods')  || isemptyfocus('invoice_number') || isemptyfocus('e_way_bill_no') || isemptyfocus('vehical_id')){
                return false;
            }
        });  
    });

    $(".newitem").click(function () {
    	// alert();
	  	$(".container2").append($(".append_item").html());
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });
	});

	$(".new_invoice").click(function () {
	  	$(".container3").append($(".append_invoice").html());
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });
	});

	$("#warehouse_id").change(function(){
    	var id = $('#warehouse_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_warehouse_address')?>",
            data: {warehouse_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".warehouse_address_view").html(data); 
            }
        });
    });

    $("#consignee_id").change(function(){
    	var id = $('#consignee_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_consignee_address')?>",
            data: {consignee_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".consignee_address_view").html(data); 
            }
        });
    });

    $("#consignor_id").change(function(){
    	var id = $('#consignor_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_consignor_address')?>",
            data: {consignor_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".address_view").html(data); 
            }
        });
    });

    $("#vehical_id").change(function(){
    	var id = $('#vehical_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_vehical_type')?>",
            data: {vehical_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".vehical_type_view").html(data); 
            }
        });
    });
</script>
		