<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kamla</title>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i');
            body{font-family: 'Roboto', sans-serif;}
            .heading h3{text-align:center}
            .line{display: flex;
            flex-wrap: wrap;padding-right: 15px;padding-left: 15px;}
            .left-side{max-width: 80%;position: relative;width: 100%;}
            .right-side{max-width: 20%;position: relative;width: 100%;text-align: right;}
            .right-side span,.bottom span{border-bottom: 1px solid black;padding: 3px 0; color:transparent}
            .bottom{margin: 67px 0 0 0;}
            table td {text-align:center}
        </style>
    </head>
    <div id="main-container">
        <div class="padding-md">
            <div class="panel panel-default table-responsive">
                <?php $this->load->view('include/messages')?>
                <div class="panel-heading">
                    Add CONSIGNMENT NOTE
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="row">
                            <div class="main-container">
                                <div class="main-border">
                                    <form action="<?= base_url('Genrate_consignee/add_consignee')?>" method="post" id="add_challan">
                                        <div class="heading" style="margin: -35px 0 0 0;">
                                            <h3>CONSIGNMENT NOTE</h3>
                                        </div>
                                        <div class="line">
                                            <div class="left-side">
                                                <div class="col-md-4 row">
                                                    <div class="form-group">
                                                        <label for="">Warehouse Name</label>
                                                        <select class="form-control" id="warehouse_id" name="warehouse_id" onchange="GetChallanvalue(this)" style="border-radius: 0; border-style: solid;">
                                                            <option value="">Select</option>
                                                            <?php
                                                                if ($warehouse_details !=null) {
                                                                    foreach ($warehouse_details as $key => $value) {
                                                                    ?>
                                                                        <option value="<?= $value['warehouse_id']?>"><?= $value['warehouse_name']?></option>
                                                                    <?php }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div> 
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Warehouse Address</label>
                                                    <div class="form-group">
                                                        <div class="warehouse_address_view">
                                                            <textarea class="form-control" placeholder="Address" readonly></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <!-- <div class="line">
                                            <span>Consignor : <input type="checkbox" id="myCheck"> JK Tyre &amp; Industries Limited</span>
                                            <span><input type="checkbox" id="myCheck2"> CAVANDISH Industries Limited,Kenduguri,Jorhat - 785010(Assam)</span>
                                        </div><br> -->
                                        <div class="line">
                                            <!-- <p style="float:left;width: 50%;">Despatch From : </p> -->
                                            <div class="form-group row">
                                              <label class="control-label col-sm-5">Despatch From</label>
                                              <div class="col-sm-7">
                                                <select class="form-control" id="despatch_name" name="despatch_name" style="border-radius: 0; border-style: solid;">
                                                    <option value="">Select</option>
                                                    <?php
                                                        if ($despatch_details !=null) {
                                                            foreach ($despatch_details as $key => $value) {
                                                            ?>
                                                                <option value="<?= $value['despatch_id']?>"><?= $value['despatch_name']?></option>
                                                            <?php }
                                                        }
                                                    ?>
                                                </select>
                                              </div>
                                            </div>

                                            <div class="form-group row" style="margin-left: 51%;">
                                              <label class="control-label col-sm-5">Destination</label>
                                              <div class="col-sm-7">
                                                <select class="form-control" id="destination_name" name="destination_name" style="border-radius: 0; border-style: solid;">
                                                    <option value="">Select</option>
                                                    <?php
                                                        if ($destination_details !=null) {
                                                            foreach ($destination_details as $key => $value) {
                                                            ?>
                                                                <option value="<?= $value['destination_id']?>"><?= $value['destination_name']?></option>
                                                            <?php }
                                                        }
                                                    ?>
                                                </select>
                                              </div>
                                            </div>
                                            <!-- <p style="float: right;width: 50%;text-align: right;">Destination : <strong><u>JORHAT-DEPOT</u></strong></p> -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-sm-2 row">
                                                <label>Add Challan</label>
                                            </div>
                                            <div class="col-sm-2 row" style="margin-left: -48px;">
                                                <input type="hidden" name="temp_id" id="temp_id" value="0">
                                                <button type="button" class="btn btn-primary btn-sm new_challan">+</button>
                                            </div>    
                                        </div> <br><br><br>

                                        <!-------------- Append new div Start-------------->

                                        <div class="container2">
                                            <div id="challan_div1">
                                                <div class="append_challan">
                                                    <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <label for="">Challan Number</label>
                                                        <select onchange="GetSelectedValue(this)" class="form-control challan_id" id="challan_id1" name="challan_id[]" style="border-radius: 0; border-style: solid;">
                                                            <option value="">Select</option>
                                                            <?php
                                                                /*if ($challan_details !=null) {
                                                                    foreach ($challan_details as $key => $value) {
                                                                    ?>
                                                                        <option value="<?= $value['challan_id']?>"><?= $value['refrence_number']?></option>
                                                                    <?php }
                                                                }*/
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="all_challan_details">
                                                        <div class="col-md-4">
                                                            <label for="">Consignee Name</label>
                                                            <input type="text" readonly class="form-control" placeholder="Consignee Name">
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label for="">Consignee Address</label>
                                                            <textarea readonly class="form-control" placeholder="Address"></textarea>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Consignor Name</label>
                                                                <input type="text" readonly class="form-control" placeholder="Consignor Name">
                                                            </div> 
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label for="">Consignor address_view</label>
                                                            <div class="form-group">
                                                                <textarea class="form-control" placeholder="Address" readonly></textarea>
                                                            </div>
                                                        </div>  

                                                        <div class="col-md-4">
                                                            <label for="">Invoice No</label>
                                                            <input type="text" placeholder="Invoice No" class="form-control" readonly></textarea>   
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label for="">Item</label>
                                                            <input type="text" placeholder="Item Name" class="form-control" readonly></textarea>    
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label for="">Item Quantity</label>
                                                            <input type="text" placeholder="Item Quantity" class="form-control" readonly></textarea>    
                                                        </div>
                                                    </div>  
                                                </div>

                                                <!-- <div class="col-md-4">
                                                    <label for="">E-Way Bill No.</label>
                                                    <input type="text" class="form-control" readonly>
                                                </div> -->

                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <label for="">Invoice Date</label>
                                                        <input type="text" name="invoice_date[]" placeholder="DD/MM/YYYY" class="form-control date">    
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">amount</label>
                                                        <input type="text" name="amount[]" id="amount" placeholder="Amount" class="form-control digits"></textarea> 
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Gross Weight in KG</label>
                                                            <input type="text" name="gross_weight[]" id="gross_weight" placeholder="Gross Weight in KG" class="form-control digits">   
                                                        </div> 
                                                    </div> 
                                                </div>

                                                <div class="col-md-12 row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="freight_amount">Freight Amount</label>
                                                            <input type="text" name="freight_amount[]" id="freight_amount" placeholder="Freight Amount" maxlength="5" class="form-control digits">  
                                                        </div> 
                                                    </div> 
                                                    
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="remarks">Remarks</label>
                                                            <textarea name="remarks[]" id="remarks" placeholder="Remarks" class="form-control"></textarea>  
                                                        </div> 
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <hr style="background-color: #2d4651; height: 2px;">    
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-------------- Append new div End -------------->
                                        
                                        <div class="col-md-12 col-lg-12 footer">
                                            <button type="submit" id="EditC" class="btn btn-sm btn-success check">Submit</button>
                                            <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer')?>	
    <script>
        var countchallan = 1;
        function GetSelectedValue(id) {
               var selectedValue = id.value;
               var id = id.id;
               id = id.match(/\d+/);
               id = id[0];
               $.ajax({
                   url: "<?= base_url('Genrate_consignee/get_challan_details')?>",
                   data: {challan_id:selectedValue },
                   type: "POST",
                   dataType: "html",
        
                   success: function (data) {
                       //$(".all_challan_details").html(data);
                       $("#challan_div"+id+" .all_challan_details").html(data);
                   }
               });
           }
           function GetChallanvalue(id) {
           	// alert();
               var selectedValue = id.value;
               $.ajax({
                   url: "<?= base_url('Genrate_consignee/get_challan')?>",
                   data: {warehouse_id:selectedValue },
                   type: "POST",
                   dataType: 'json',
        
                   success: function (data) {
                       $(".challan_id").each(function(){
                       	$(this)
        		          .find('option')
        		          .remove()
        		          .end()
        		          .append('<option value="">Select</option>')
        		          .val('whatever');
        
        		          for(var i = 0; i < data.length; i++) {
        
        		            $(this).append($('<option>', { 
        		              value: data[i]['challan_id'],
        		              text : data[i]['refrence_number']
        		            }));
        
        		          }
                    })    
                   }
               });
           }
           
        $(document).ready(function() {
        
        	$('.append_challan').hide();
        
        	$('.new_challan').click( function() {
                   var counter = $('#temp_id').val();
                   counter++ ;
                   // alert(counter);
                   $('#temp_id').val(counter);
            });
        
            $('.check').click(function(){
            	var counter = $('#temp_id').val();
        
                   $('#temp_id').val(counter);
            	if (counter == 0) {
            		alert('Add Challan After Submit...!');
            		return false;
            	}
            	var vehical_id = $('.vehical_id').map(function() {return this.value;}).get();
            	if(!!vehical_id.reduce(function(a, b){ return (a === b) ? a : NaN; }) == false){
            		alert('All Challan vehical number should be same...!');
            		return false;
            	}            	
            });
        
           $('.check').click(function(){
               if(isemptyfocus('warehouse_id') || isemptyfocus('despatch_name') || isemptyfocus('destination_name') || isemptyfocus('despatch_name') || isemptyfocus('destination_id') || isemptyfocus('date') || isemptyfocus('gross_weight') || isemptyfocus('freight_amount') || isemptyfocus('remarks') || isemptyfocus('challan_id')){
                   return false;
               }
           }); 
        }); 
        
        $(".new_challan").click(function () {
          	//$(".container2").append($(".append_challan").html());
          	countchallan++;
          	$(".container2").append('<div id="challan_div'+countchallan+'">'+$(".append_challan").html()+'</div>');
          	$('.challan_id:last').attr('id','challan_id'+countchallan);
        	$(".digits").keypress(function (e) {
               	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                   	return false;
               	}
            });
        
            $('.date').datepicker({
              	format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true
             });
        });  
        
        $("#warehouse_id").change(function(){
           	var id = $('#warehouse_id').val();
           	// alert(id);
            $.ajax({
               url: "<?= base_url('Genrate_consignee/get_warehouse_address')?>",
               data: {warehouse_id:id },
               type: "POST",
               dataType: "html",
    
                success: function (data) {
               	// alert(data);
                   $(".warehouse_address_view").html(data); 
                }
            });
       });   
    </script>
</html>

