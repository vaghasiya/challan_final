<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from minetheme.com/Endless1.5.1/register.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 08 Jun 2016 07:01:12 GMT -->
<head>
    <meta charset="utf-8">
    <title>Create Warehouse</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url()?>assets/css/endless.min.css" rel="stylesheet">
	
  </head>

  <body>
	<div class="login-wrapper">
		<div class="text-center">
			<h2 class="fadeInUp animation-delay10" style="font-weight:bold">
				<span class="text-success">Admin</span>
			</h2>
	    </div>
		<div class="login-widget animation-delay1">	
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-plus-circle fa-lg"></i> Register
				</div>

				<?php $this->load->view('include/messages')?>
				<div class="panel-body">

					<form class="form-login" enctype="multipart/form-data" method="post" action="<?= base_url('Login/add_warehouse')?>">

						<div class="form-group">
							<label>User Name</label>
							<input type="text" name="user_name" id="user_name" placeholder="User Name" class="form-control txtOnly" value="<?php echo set_value('user_name')?>">
						</div>

						<div class="form-group valid_email">
							<label>User Email</label>
							<input type="text" name="user_email" id="user_email" placeholder="User Email" class="form-control valid_email" value="<?php echo set_value('user_email')?>">
						</div>
						<div id="valid" style="color: red"></div>


						<div class="form-group">
							<label>Address</label>
							<textarea name="user_address" id="user_address" placeholder="Address" class="form-control"></textarea>
						</div>

						<div class="form-group">
							<label>Password</label>
							<input type="password" name="user_password" id="user_password" placeholder="Password" class="form-control">
						</div><!-- /form-group -->

						<div class="form-group">
							<div class="controls">
								<button type="submit" class="btn btn-success check">Sign up</button>
								<a href="<?= base_url()?>"><button type="button" class="btn btn-danger pull-right">Cancel</button></a>
							</div>
						</div><!-- /form-group -->
					</form>
				</div>
			</div><!-- /panel -->
		</div><!-- /login-widget -->
	</div><!-- /login-wrapper -->
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <!-- Jquery -->
	<script src="<?= base_url()?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url('assets/plugins/validation/common.js') ?>"></script>

    <script type="text/javascript">
    	$(document).ready(function() {
	        $('.check').click(function(){
	            if(isemptyfocus('user_name') || isvalidemail('user_email') || isemptyfocus('user_address') || isemptyfocus('user_password')){
	                return false;
	            }
	        }); 

			$('.valid_email input:first').on('keyup', function(){
			    var valid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value) && this.value.length;
			    $('#valid').html('It\'s'+ (valid?'':' not') +' valid');
			});   
	    });
    </script>

    <script type="text/javascript">
	    $(document).ready(function () {
	        $(".digits").keypress(function (e) {
	        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	            	return false;
	        	}
	        });

	        $('.txtOnly').bind('keydown', function(event) {
			  var key = event.which;
			  if (key >=48 && key <= 57) {
			    event.preventDefault();
			  }
			});
	    });
	</script>
  </body>

<!-- Mirrored from minetheme.com/Endless1.5.1/register.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 08 Jun 2016 07:01:12 GMT -->
</html>
