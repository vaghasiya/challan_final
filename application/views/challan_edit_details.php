<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Edit Challan Details 
				</div>
				<div class="modal-body">
	                <div class="modal-body">
		                <div class="row">
		                	<?php
		                		if ($challan_details !=null) {
		                			foreach ($challan_details as $key => $value) {
		                			?>
					                	<div class="col-md-12">
					                        <form action="<?= base_url('Genrate_challan/update_challan')?>" method="post">
					                        	
					                        	<input type="hidden" name="challan_id" value="<?= $value['challan_id']?>">

					                            <div class="col-md-12 col-lg-12 row">
					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Warehouse Name</label>
														  	<select class="form-control" id="edit_warehouse_id" name="warehouse_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($warehouse_details !=null) {
															    		foreach ($warehouse_details as $key => $warehouse_dtls_row) {
															    		?>
															    			<option value="<?= $warehouse_dtls_row['warehouse_id']?>" <?= ($warehouse_dtls_row['warehouse_id'] == $value['warehouse_id']) ?'selected' : '';?>><?= $warehouse_dtls_row['warehouse_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
														</div> 
													</div>

													<div class="col-md-4">
						                                <label for="">Warehouse Address</label>
						                                <div class="form-group">
															<div class="warehouse_address_view">
																<textarea class="form-control" value="" placeholder="Address" readonly><?= $value['warehouse_address']?></textarea>
															</div>
						                                </div>
					                            	</div>	

					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Consinee Name</label>
														  	<select class="form-control" id="edit_consignee_id" name="consignee_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($consignee_details !=null) {
															    		foreach ($consignee_details as $key => $cone_dtls_row) {
															    		?>
															    			<option value="<?= $cone_dtls_row['consignee_id']?>" <?= ($cone_dtls_row['consignee_id'] == $value['consignee_id']) ?'selected' : '';?>><?= $cone_dtls_row['consignee_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
														</div> 
													</div>
					                            </div>	

					                            <div class="col-md-12 row">
					                            	<div class="col-md-4">
						                                <label for="">Consignee Address</label>
						                                <div class="form-group">
															<div class="consignee_address_view">
																<textarea class="form-control" id="edit_consignee_address" name="consignee_address" value="" placeholder="Address" readonly><?= $value['consignee_address']?></textarea>
															</div>
						                                </div>
					                            	</div>

					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Consigner Name</label>
														  	
														  	<select class="form-control" id="edit_consignor_id" name="consignor_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($consigner_details !=null) {
															    		foreach ($consigner_details as $key => $con_dtls_row) {
															    		?>
															    			<option value="<?= $con_dtls_row['consignor_id']?>" <?= ($con_dtls_row['consignor_id'] == $value['consignor_id']) ?'selected' : '';?>><?= $con_dtls_row['consignor_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
														</div> 
													</div>

													<div class="col-md-4">
						                                <label for="">Consigner Address</label>
						                                <div class="form-group">
															<div class="address_view">
																<textarea class="form-control" id="edit_consignor_address" name="consignor_address" value="" placeholder="Address" readonly><?= $value['consignor_address']?></textarea>
															</div>
						                                </div>
					                            	</div>
					                            </div>

					                            <div class="col-md-12 row">
					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Vehical Number</label>
														  	<select class="form-control" id="edit_vehical_id" name="vehical_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($vehical_details !=null) {
															    		foreach ($vehical_details as $key => $vehcl_row) {
															    		?>
															    			<option value="<?= $vehcl_row['vehical_id']?>" <?= ($vehcl_row['vehical_id'] == $value['vehical_id']) ?'selected' : '';?>><?= $vehcl_row['vehical_number']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
														</div> 
													</div>

					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Vehical Type</label>
					                                    	<div class="vehical_type_view">
														  		<input type="text" id="edit_vehical_type" name="vehical_type" class="form-control" placeholder="Vehical Type" value="<?= $value['vehical_type']?>" readonly>
														  	</div>
														</div> 
													</div>
													<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Perticulars Of Goods</label>
					                                    	<div>
														  		<textarea style="min-width: 90%; min-height: 100px;" name="edit_perticulars_goods" id="edit_perticulars_goods" class="form-control" placeholder="Perticulars Of Goods"><?= $value['perticulars_goods']?></textarea>
														  	</div>
														</div> 
													</div>											
					                            </div>

					                            <div class="col-md-12 row">
					                            	<?php
														if ($invoice_number_details !=null) {
															foreach ($invoice_number_details as $key => $num_row)
															{
																$id = $num_row['invoice_number_id'];
															?>
																<input type="hidden" name="invoice_number_id[]" value="<?= $num_row['invoice_number_id']?>">

													        	<div class="col-md-3">
													        		<div class="form-group">
													                	<label for="">Invoice Number</label>
																	  	<input type="text" name="edit_invoice_number[]" id="edit_invoice_number" class="form-control digits" placeholder="Invoice Number" value="<?= $num_row['invoice_number']?>">
																	</div> 

																</div>
																<div class="col-md-1" style="padding: 25px 0 0 0">
																	<a href="<?= base_url('Genrate_challan/delete_number/'.$id)?>"><button type="button" class="btn btn-warning btn-xs" onclick="return ConfirmDelete();">Delete</button></a>
																</div>
															<?php }
														}
													?>
													<div class="col-md-4"></div>
													<div class="col-md-4"></div>
													<div class="container3 col-md-3">
														<div class="append_invoice col-md-12" style="padding: 0;">
					                                    	<label for="">Invoice Number</label>
															<input type="text" name="edit_invoice_number[]" id="edit_invoice_number" class="form-control digits" placeholder="Invoice Number" value="">
						                            	</div>
						                            </div>

													<div class="col-md-1" style="margin:5px 0 0 0;">
					                            		<label></label><br>
					                            		<button type="button" class="btn btn-primary btn-sm new_invoice">+</button>
					                            	</div>

					                            	<div class="col-md-3">
														<label>E-Way Bill no.</label>
														<input type="text" class="form-control" name="e_way_bill_no" id="e_way_bill_no" value="<?= $value['e_way_bill_no']?>">
													</div>	
					                            </div>

					                            <?php
					                            	if ($challan_item_details !=null) {
					                            		foreach ($challan_item_details as $key => $challan_item_row) {
					                            			$id = $challan_item_row['challan_item_id'];
					                            		?>
                        									<input type="hidden" name="challan_item_id[]" value="<?= $challan_item_row['challan_item_id']?>">

					                            			<div style="margin: 20px 0 0 0;">
								                            	<div class="col-md-2" style="margin: 20px 0 0 0;">
							                                    	<label for="">Item</label>
							                                    	<select class="form-control" id="edit_item_id" name="edit_item_id[]">
																	    <option value="">Select</option>
																	    <?php
																	    	if ($total_item !=null) {
																	    		foreach ($total_item as $key => $item_row) {
																	    		?>
																	    			<option value="<?= $item_row['item_id']?>" <?= ($item_row['item_id'] == $challan_item_row['item_id']) ?'selected' : '';?>><?= $item_row['item_name']?></option>
																	    		<?php }
																	    	}
																	    ?>
																	</select>
								                            	</div>

								                            	<div class="col-md-2" style="margin: 20px 0 0 0;">
								                                    <label for="">Quantity</label>
									                                <div class="form-group">
																	  	<input type="text" class="form-control digits" id="edit_item_quantity" name="edit_item_quantity[]" value="<?= $challan_item_row['item_quantity']?>" style="max-width: 50px;">

																	  	<div style="padding: 2px 0 0 0">
																			<a href="<?= base_url('Genrate_challan/delete_item/'.$id)?>"><button type="button" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();">Delete</button></a>
																		</div>	
																	</div> 
								                            	</div>
								                            </div>
					                            		<?php }
					                            	}
					                            ?>

					                            <div class="container2">
					                            	<div class="append_item">
						                            	<div class="col-md-2" style="margin: 20px 0 0 0;">
					                                    	<label for="">Item</label>
					                                    	<select class="form-control" id="edit_item_id" name="edit_item_id[]">
															    <option value="">Select</option>
															    <?php
															    	if ($total_item !=null) {
															    		foreach ($total_item as $key => $item_row) {
															    		?>
															    			<option value="<?= $item_row['item_id']?>"><?= $item_row['item_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
						                            	</div>

						                            	<div class="col-md-2" style="margin: 20px 0 0 0;">
						                                    <label for="">Quantity</label>
							                                <div class="form-group">
															  	<input type="text" class="form-control digits" id="edit_item_quantity" name="edit_item_quantity[]" style="max-width: 50px;">
															</div> 
						                            	</div>
						                            </div>	
					                            </div>

					                            <div class="col-md-12 col-lg-12 row ">
					                            	<div class="col-md-1">
					                            		<label for=""></label>
					                            		<div class="form-group">
					                            			<button type="button" class="btn btn-primary newitem btn-xs" >Add +</button>
					                            		</div>
					                            	</div>
					                            </div>	

					                            <div class="col-md-12 col-lg-12 footer">
					                                <button type="submit" class="btn btn-sm btn-success edit_check">Save</button>
					                                <button type="reset" class="btn btn-sm btn-danger pull-right">Cancel</button>
						                        </div>
					                        </form>
					                    </div>
		                    		<?php } 
		                		}
		                	?>
		                </div>
		            </div>
	            </div>
			</div>
		</div>

	</div>

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_warehouse_id') || isemptyfocus('edit_consignee_id') || isemptyfocus('edit_consignor_id') || isemptyfocus('edit_vehical_id') || isemptyfocus('edit_perticulars_goods')  || isemptyfocus('edit_invoice_number') || isemptyfocus('e_way_bill_no')){
                return false;
            }
        });  
    });

    $(".new_invoice").click(function () {
	  	// $(".container3").show();
	  	$(".container3").append($(".append_invoice").html());
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });
	});

    $(".newitem").click(function () {
	  	$(".container2").append($(".append_item").html());
	  	$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });
	});

	$("#edit_warehouse_id").change(function(){
    	var id = $('#edit_warehouse_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_warehouse_address')?>",
            data: {warehouse_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".warehouse_address_view").html(data); 
            }
        });
    });

	$("#edit_consignee_id").change(function(){
    	var id = $('#edit_consignee_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_consignee_address')?>",
            data: {consignee_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".consignee_address_view").html(data); 
            }
        });
    });

    $("#edit_consignor_id").change(function(){
    	var id = $('#edit_consignor_id').val();
    	
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_consignor_address')?>",
            data: {consignor_id:id},
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(id);
                $(".address_view").html(data); 
            }
        });
    });

    $("#edit_vehical_id").change(function(){
    	var id = $('#edit_vehical_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_vehical_type')?>",
            data: {vehical_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".vehical_type_view").html(data); 
            }
        });
    });
</script>
		