<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>
<?php 
	$temp = array();
?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Challan List
				</div>
				<div class="modal-body" style="overflow-x: auto;">
	                <div class="row">
	                	<div class="col-md-12">
	                		<a href="<?= base_url('genrate_challan/challan_add')?>"><button type="button" class="btn btn-success btn-sm">Add Challan</button></a><br><br><br>

	                        <table class="table table-striped" id="dataTable">
								<thead>
									<tr>
										<th>No</th>
										<th>Warehouse Name</th>
										<th>Consignor Name</th>
										<th>Consignor Address</th>
										<th>Consignee Name</th>
										<th>Consignee Address</th>
										<th>Vehical Number</th>
										<th>Vehical Type</th>
										<th>Challan Number</th>
										<th>Invoice Number</th>
										<th>Create Date</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody>
									<?php 
										if ($challan_details !=null) {
											foreach ($challan_details as $key => $value) {
												$id = $value['challan_id'];
											?>
												<tr> 
													<td><?= $key+1;?></td>
													<td><?= $value['warehouse_name']?></td>	
													<td><?= $value['consignor_name']?></td>	
													<td><?= $value['consignor_address']?></td>	
													<td><?= $value['consignee_name']?></td>	
													<td><?= $value['consignee_address']?></td>	
													<td><?= $value['vehical_number']?></td>	
													<td><?= $value['vehical_type']?></td>	
													<td><?= $value['refrence_number']?></td>	
													<td>
														<?php 
															if ($invoice_number_details !=null) {
																foreach ($invoice_number_details as $key => $num_row) {
																	if ($num_row['challan_id'] == $id) {
																	?>
																		<?= $num_row['invoice_number'].' ,'?>	
																	<?php }	
																}
															}
														?>
													</td>	
													<td><?= $value['create_date']?></td>	
													<td style="width: 120px;">   

							                            <a href="<?= base_url('Genrate_challan/edit_challan/'.$id)?>">
							                            	<button type="button" title="Edit Challan" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button>
							                            </a>

							                            <!-- <a href="javascript:void(0)" onclick="edit_challan('<?=$value['challan_id']?>','<?=$value['consignee_address']?>');">

					                            			<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button>
					                            		</a> -->
							                            
							                            <a href="<?= base_url('Genrate_challan/delete_challan/'.$id)?>"><button type="button" title="Delete Challan" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>

							                            <a href="<?= base_url('Genrate_challan/create_pdf/'.$id)?>" target="_blank"><button type="button" title="Print Challan" class="btn btn-primary btn-xs">Print</button></a>
							                        </td> 
												</tr>
											<?php }
										}	
									?>
								</tbody>
							</table>
	                    </div>
	                </div>
	            </div>
			</div>
		</div>

		<!-- Add Menu -->
		<?php /*?>
		<div class="modal fade" id="add_challan" tabindex="-1">
		    <div class="modal-dialog modal-lg" role="document" overflow-y: auto;>
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
		                <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title">Add Challan</h4>
		            </div>

		            <div class="modal-body" style="max-height: calc(100vh - 210px);overflow-y: auto;">
		                <div class="row">
		                	<div class="col-md-12">

		                        <form action="<?= base_url('Genrate_challan/add_challan')?>" method="post" id="add_challan">
	                           
		                            <div class="col-md-12 col-lg-12 row">
		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Warehouse Name</label>
											  	<select class="form-control" id="warehouse_id" name="warehouse_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($warehouse_details !=null) {
												    		foreach ($warehouse_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['warehouse_id']?>"><?= $value['warehouse_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div> 
										</div>

										<div class="col-md-4">
			                                <label for="">Warehouse Address</label>
			                                <div class="form-group">
												<div class="warehouse_address_view">
													<textarea class="form-control" value="" placeholder="Address" readonly></textarea>
												</div>
			                                </div>
		                            	</div>

		                            	<div class="col-md-6">
		                            		<div class="form-group">
		                                    	<label for="">Consinee Name</label>
											  	<select class="form-control" id="consignee_id" name="consignee_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($consignee_details !=null) {
												    		foreach ($consignee_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['consignee_id']?>"><?= $value['consignee_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div> 
										</div>
		                            </div>	

		                            <div class="col-md-12 col-lg-12 row">
		                            	<div class="col-md-4">
			                                <label for="">Consignee Address</label>
			                                <div class="form-group">
												<div class="consignee_address_view">
													<textarea class="form-control" value="" placeholder="Address" readonly></textarea>
												</div>
			                                </div>
		                            	</div>

		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Consigner Name</label>
											  	<select class="form-control" id="consignor_id" name="consignor_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($consigner_details !=null) {
												    		foreach ($consigner_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['consignor_id']?>"><?= $value['consignor_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div> 
										</div>

										<div class="col-md-4">
			                                <label for="">Consigner Address</label>
			                                <div class="form-group">
												<div class="address_view">
													<textarea class="form-control" value="" placeholder="Address" readonly></textarea>
												</div>
			                                </div>
		                            	</div>	
		                            </div>	

		                            <div class="col-md-12 row">
		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Vehical Number</label>
											  	<select class="form-control" id="vehical_id" name="vehical_id" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($vehical_details !=null) {
												    		foreach ($vehical_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['vehical_id']?>"><?= $value['vehical_number']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div> 
										</div>

		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Vehical Type</label>
		                                    	<div class="vehical_type_view">
											  		<input type="text" readonly class="form-control" placeholder="Vehical Type">
											  	</div>
											</div> 
										</div>

										<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Perticulars Of Goods</label>
		                                    	<div>
											  		<textarea name="perticulars_goods" id="perticulars_goods" class="form-control" placeholder="Perticulars Of Goods"></textarea>
											  	</div>
											</div> 
										</div>	                            
		                            </div>

		                            <div class="col-md-12 row">
			                            <!-- <div class="container3">
			                            	<div class="append_invoice">
				                            	<div class="col-md-4">
				                            		<div class="form-group">
				                                    	<label for="">Invoice Number</label>
				                                    	<div>
													  		<input type="text" name="invoice_number[]" id="invoice_number" class="form-control digits" placeholder="Invoice Number">
													  	</div>
													</div> 
												</div>
											</div>	
			                            </div> -->
			                            <div class="container3">
			                            	<div class="append_invoice">
				                            	<div class="col-md-3">
				                            		<div class="form-group">
				                                    	<label for="">Invoice Number</label>
													  	<input type="text" name="invoice_number[]" id="invoice_number" class="form-control digits" placeholder="Invoice Number">
													</div> 
												</div>
											</div>
										</div>
		                            	<div class="col-md-1" style="margin:5px 0 0 0;">
		                            		<label></label><br>
		                            		<button type="button" class="btn btn-primary btn-sm new_invoice">+</button>
		                            	</div>
			                        </div>    

	                            	<div class="container2">
		                            	<div class="append_item">
			                            	<div class="col-md-2">
		                                    	<label for="">Item</label>
		                                    	<select class="form-control" id="item_id" name="item_id[]">
												    <option value="">Select</option>
												    <?php
												    	if ($item_details !=null) {
												    		foreach ($item_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['item_id']?>"><?= $value['item_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
			                            	</div>

			                            	<div class="col-md-2">
			                                    <label for="">Quantity</label>
				                                <div class="form-group">
												  	<input type="text" class="form-control digits" name="item_quantity[]" value="" style="max-width: 50px;">
												</div> 
			                            	</div>
			                            </div>	
		                            </div>

		                            <div class="col-md-12 col-lg-12 row ">
		                            	<div class="col-md-1">
		                            		<label for=""></label>
		                            		<div class="form-group">
		                            			<button type="button" class="btn btn-primary newitem btn-xs" >Add +</button>
		                            		</div>
		                            	</div>
		                            </div>	

		                            <div class="col-md-12 col-lg-12 footer">
		                                <button type="submit" id="EditC" class="btn btn-sm btn-success check">Submit</button>
		                                <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">Cancel</button>
			                        </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
		            <div class="modal-footer">
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<?php */?>
		<!-- Add Menu -->
	</div>

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('warehouse_id') || isemptyfocus('consignee_id') || isemptyfocus('consignor_id') || isemptyfocus('vehical_id') || isemptyfocus('perticulars_goods') ||  isemptyfocus('invoice_number') || isemptyfocus('item_id')){
                return false;
            }
        });  
    });

    $(".newitem").click(function () {
	  	$(".container2").append($(".append_item").html());
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });
	});

	$(".new_invoice").click(function () {
	  	$(".container3").append($(".append_invoice").html());
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });
	});

	$("#warehouse_id").change(function(){
    	var id = $('#warehouse_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_warehouse_address')?>",
            data: {warehouse_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".warehouse_address_view").html(data); 
            }
        });
    });

    $("#consignee_id").change(function(){
    	var id = $('#consignee_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_consignee_address')?>",
            data: {consignee_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".consignee_address_view").html(data); 
            }
        });
    });

    $("#consignor_id").change(function(){
    	var id = $('#consignor_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_consignor_address')?>",
            data: {consignor_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".address_view").html(data); 
            }
        });
    });

    $("#vehical_id").change(function(){
    	var id = $('#vehical_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_challan/get_vehical_type')?>",
            data: {vehical_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".vehical_type_view").html(data); 
            }
        });
    });
</script>
		