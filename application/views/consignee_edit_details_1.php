<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Edit Challan Details 
				</div>
				<div class="modal-body">
	                <div class="modal-body">
		                <div class="row">
		                	<?php
		                		if ($consignee_details !=null) {
		                			foreach ($consignee_details as $key => $value) {
		                			?>
					                	<div class="col-md-12">
					                        <form action="<?= base_url('Genrate_consignee/update_consignee')?>" method="post">
					                        	
					                        	<input type="hidden" name="consignee_id" value="<?= $value['consignee_id']?>">

					                            <div class="col-md-12 col-lg-12 row">
					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Warehouse Name</label>
														  	<select class="form-control" id="edit_warehouse_id" name="warehouse_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($warehouse_details !=null) {
															    		foreach ($warehouse_details as $key => $warehouse_dtls_row) {
															    		?>
															    			<option value="<?= $warehouse_dtls_row['warehouse_id']?>" <?= ($warehouse_dtls_row['warehouse_id'] == $value['warehouse_id']) ?'selected' : '';?>><?= $warehouse_dtls_row['warehouse_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
														</div> 
													</div>

													<div class="col-md-4">
						                                <label for="">Warehouse Address</label>
						                                <div class="form-group">
															<div class="warehouse_address_view">
																<textarea class="form-control" value="" placeholder="Address" readonly><?= $value['warehouse_address']?></textarea>
															</div>
						                                </div>
					                            	</div>	

					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Despatch From</label>
														  	<select class="form-control" id="despatch_id" name="despatch_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($despatch_details !=null) {
															    		foreach ($despatch_details as $key => $despatch_row) {
															    		?>
															    			<option value="<?= $despatch_row['despatch_id']?>" <?= ($despatch_row['despatch_id'] == $value['despatch_id']) ?'selected' : '';?>><?= $despatch_row['despatch_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
														</div> 
													</div>
					                            </div>	

					                            <div class="col-md-12 row">
					                            	<div class="col-md-4">
						                                <div class="form-group">
						                                	<label for="">Destination</label>
															<select class="form-control" id="destination_id" name="destination_id" style="border-radius: 0; border-style: solid;">
															    <option value="">Select</option>
															    <?php
															    	if ($destination_details !=null) {
															    		foreach ($destination_details as $key => $destination_row) {
															    		?>
															    			<option value="<?= $destination_row['destination_id']?>" <?= ($destination_row['destination_id'] == $value['destination_id']) ?'selected' : '';?>><?= $destination_row['destination_name']?></option>
															    		<?php }
															    	}
															    ?>
															</select>
						                                </div>
					                            	</div>
					                            </div>

					                            <?php
					                            	if ($total_consignee_details !=null) {
					                            		foreach ($total_consignee_details as $key => $consignee_row) {
					                            			$id = $consignee_row['consignee_details_id'];

					                            			$invc_no = array();
															foreach ($invoice_no_details as $key => $invc_num_row) {
																if ($invc_num_row['challan_id'] === $consignee_row['challan_id']) {
																	array_push($invc_no, $invc_num_row['invoice_number']);
																}
															}
					                            		?>
                        								<input type="hidden" name="consignee_details_id[]" value="<?= $consignee_row['consignee_details_id']?>">

									                	<div class="col-md-12 row">
									                    	<div class="col-md-4">
									                        	<label for="">Challan Number</label>
															  	<select onchange="GetSelectedValue(this)" class="form-control" id="challan_id" name="challan_id[]" style="border-radius: 0; border-style: solid;">
																    <option value="">Select</option>
																    <?php
																    	if ($challan_details !=null) {
																    		foreach ($challan_details as $key => $challan_row) {
																    		?>
																    			<option value="<?= $challan_row['challan_id']?>" <?= ($challan_row['challan_id'] == $consignee_row['challan_id']) ?'selected' : '';?>><?= $challan_row['refrence_number']?></option>
																    		<?php }
																    	}
																    ?>
																</select>
															</div>

															<div class="all_challan_details">
																<?php
																	if ($challan_details !=null) {
																		foreach ($challan_details as $key => $challan_details_row) {
																			if ($challan_details_row['challan_id'] == $consignee_row['challan_id']) {

																				// ============ load edit buttion click fetch data ============ //
																				$consignee_dtls = get_all_with_helper('tbl_consignee','','',array('consignee_id' => $challan_details_row['consignee_id'],'user_id' => $this->session->userdata('login_id')));

																				$consignor_dtls = get_all_with_helper('tbl_consignor','','',array('consignor_id' => $challan_details_row['consignor_id'],'user_id' => $this->session->userdata('login_id')));

																				$challan_item = get_all_with_helper('tbl_challan_item','','',array('challan_id' => $challan_details_row['challan_id'],'user_id' => $this->session->userdata('login_id')));

																				// echo"<pre>"; print_r($challan_item); 
																			?>
																				<div class="col-md-4">
													                                <label for="">Consignee Name</label>
													                                <input type="text" readonly class="form-control" placeholder="Consignee Name" value="<?= ($consignee_dtls !=null) ? $consignee_dtls[0]['consignee_name'] : '';?>">
																				</div>

																				<div class="col-md-4">
													                                <label for="">Consignee Address</label>
																					<textarea readonly class="form-control" placeholder="Address"><?= ($consignee_dtls !=null) ? $consignee_dtls[0]['consignee_address'] : '';?></textarea>
													                        	</div>

													                        	<div class="col-md-4">
													                        		<div class="form-group">
													                                	<label for="">Consignor Name</label>
																						<input type="text" readonly class="form-control" placeholder="Consignor Name" value="<?= ($consignor_dtls !=null) ? $consignor_dtls[0]['consignor_name'] : '';?>">
																					</div> 
																				</div>

																				<div class="col-md-4">
													                                <label for="">Consignor address_view</label>
													                                <div class="form-group">
																						<textarea class="form-control" placeholder="Address" readonly><?= ($consignor_dtls !=null) ? $consignor_dtls[0]['consignor_address'] : '';?></textarea>
													                                </div>
													                        	</div>	

																				<div class="col-md-4">
													                            	<label for="">Invoice No</label>
																			  		<input type="text" placeholder="Invoice No" class="form-control" readonly value="<?= ($total_consignee_details !='') ? implode(' , ', $invc_no) : '';?>"></textarea>	
																				</div>

													                        	<?php
													                        		if ($challan_item !=null) {
													                        			foreach ($challan_item as $key => $item_row) {
													                        				$item_name = get_all_with_helper('tbl_item','','',array('item_id'=> $item_row['item_id'],'user_id' => $this->session->userdata('login_id')));	
													                        			?>
													                        				<div class="col-md-12 row">
																								<div class="col-md-3">
																	                            	<label for="">Item</label>
																							  		<input type="text" placeholder="Item Name" class="form-control" readonly value="<?= ($item_name !=null) ? $item_name[0]['item_name'] : '';?>"></textarea>	
																								</div>

																								<div class="col-md-3">
																	                            	<label for="">Item Quantity</label>
																							  		<input type="text" placeholder="Item Quantity" class="form-control" readonly value="<?= ($challan_item !=null) ? $item_row['item_quantity'] : '';?>"></textarea>	
																								</div>
																							</div>
													                        			<?php }
													                        		}
													                        	?>
																			<?php }
																		}
																	}
																?>
									                        </div>	
									                    </div>

									                    <div class="col-md-12 row">
									                        <div class="col-md-4">
									                        	<label for="">Invoice Date</label>
									                        	<input type="text" name="invoice_date[]" value="<?= date('d-m-Y',strtotime($consignee_row['invoice_date']))?>" placeholder="DD/MM/YYYY" class="form-control date">	
															</div>

															<div class="col-md-4">
									                        	<label for="">amount</label>
														  		<input type="text" name="amount[]" value="<?= $consignee_row['amount']?>" id="amount" placeholder="Amount" class="form-control digits"></textarea>	
															</div>

									                    	<div class="col-md-4">
									                    		<div class="form-group">
									                            	<label for="">Gross Weight in KG</label>
																	<input type="text" name="gross_weight[]" value="<?= $consignee_row['gross_weight']?>" id="gross_weight" placeholder="Gross Weight in KG" class="form-control">	
																</div> 
															</div> 
														</div>

														<div class="col-md-12 row">
															<div class="col-md-4">
									                    		<div class="form-group">
									                            	<label for="freight_amount">Freight Amount</label>
																	<input type="text" name="freight_amount[]" value="<?= $consignee_row['freight_amount']?>" id="freight_amount" placeholder="Freight Amount" maxlength="5" class="form-control digits">	
																</div> 
															</div> 
															
															<div class="col-md-4">
									                    		<div class="form-group">
									                            	<label for="remarks">Remarks</label>
																	<textarea name="remarks[]" id="remarks" placeholder="Remarks" class="form-control"><?= $consignee_row['remarks']?></textarea>	
																</div> 
															</div>

															<div class="col-md-4">
									                    		<div class="form-group">
									                            	<a href="<?= base_url('Genrate_consignee/delete_consignee_item/'.$id)?>" class="btn btn-danger btn-xs" style="margin-top: 20px;">Delete</a>
																</div> 
															</div>
														</div>

														<div class="col-md-12">
															<hr style="background-color: #2d4651; height: 2px;">
														</div>
					                            		<?php }
					                            	}
					                            ?>

					                            <div class="col-md-12" style="margin:5px 0 0 0;">
				                            		<label></label><br>
				                            		<button type="button" class="btn btn-primary btn-sm new_challan">Add New Challan +</button>
				                            	</div>

				                    <!-------------- Append new div Start-------------->

		                            <div class="container2">
			                            <div class="append_challan">
			                            	<div class="col-md-12 row" style="margin-top: 10px">
				                            	<div class="col-md-4">
			                                    	<label for="">Challan Number</label>
												  	<select onchange="GetSelectedValue(this)" class="form-control" id="challan_id" name="challan_id[]" style="border-radius: 0; border-style: solid;">
													    <option value="">Select</option>
													    <?php
													    	if ($challan_details !=null) {
													    		foreach ($challan_details as $key => $challan_row) {
													    		?>
													    			<option value="<?= $challan_row['challan_id']?>"><?= $challan_row['refrence_number']?></option>
													    		<?php }
													    	}
													    ?>
													</select>
												</div>

												<div class="all_challan_details">
													<div class="col-md-4">
					                                    <label for="">Consignee Name</label>
					                                    <input type="text" readonly class="form-control" placeholder="Consignee Name">
													</div>

													<div class="col-md-4">
						                                <label for="">Consignee Address</label>
														<textarea readonly class="form-control" placeholder="Address"></textarea>
					                            	</div>

					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Consignor Name</label>
															<input type="text" readonly class="form-control" placeholder="Consignor Name">
														</div> 
													</div>

													<div class="col-md-4">
						                                <label for="">Consignor address_view</label>
						                                <div class="form-group">
															<textarea class="form-control" placeholder="Address" readonly></textarea>
						                                </div>
					                            	</div>	

													<div class="col-md-4">
				                                    	<label for="">Invoice No</label>
												  		<input type="text" placeholder="Invoice No" class="form-control" readonly></textarea>	
													</div>

													<div class="col-md-3">
				                                    	<label for="">Item</label>
												  		<input type="text" placeholder="Item Name" class="form-control" readonly></textarea>	
													</div>

													<div class="col-md-3">
				                                    	<label for="">Item Quantity</label>
												  		<input type="text" placeholder="Item Quantity" class="form-control" readonly></textarea>	
													</div>
					                            </div>	
				                            </div>

				                            <div class="col-md-12 row">
					                            <div class="col-md-4">
			                                    	<label for="">Invoice Date</label>
			                                    	<input type="text" name="invoice_date[]" placeholder="DD/MM/YYYY" class="form-control date">	
												</div>

												<div class="col-md-4">
			                                    	<label for="">amount</label>
											  		<input type="text" name="amount[]" id="amount" placeholder="Amount" class="form-control digits"></textarea>	
												</div>

				                            	<div class="col-md-4">
				                            		<div class="form-group">
				                                    	<label for="">Gross Weight in KG</label>
														<input type="text" name="gross_weight[]" id="gross_weight" placeholder="Gross Weight in KG" class="form-control">	
													</div> 
												</div> 
											</div>

											<div class="col-md-12 row">
												<div class="col-md-4">
				                            		<div class="form-group">
				                                    	<label for="freight_amount">Freight Amount</label>
														<input type="text" name="freight_amount[]" id="freight_amount" placeholder="Freight Amount" maxlength="5" class="form-control digits">	
													</div> 
												</div> 
												
												<div class="col-md-4">
				                            		<div class="form-group">
				                                    	<label for="remarks">Remarks</label>
														<textarea name="remarks[]" id="remarks" placeholder="Remarks" class="form-control"></textarea>	
													</div> 
												</div>
											</div>
											<div class="col-md-12">
												<hr style="background-color: #2d4651; height: 2px;">	
											</div>
			                            </div>
		                            </div>

		                            <!-------------- Append new div End -------------->
					                            

					                            <div class="col-md-12 col-lg-12 footer">
					                                <button type="submit" class="btn btn-sm btn-success edit_check">Save</button>
					                                <button type="reset" class="btn btn-sm btn-danger pull-right">Cancel</button>
						                        </div>
					                        </form>
					                    </div>
		                    		<?php } 
		                		}
		                	?>
		                </div>
		            </div>
	            </div>
			</div>
		</div>

	</div>

<?php $this->load->view('include/footer')?>	

<script>
	function GetSelectedValue(id) {
        var selectedValue = id.value;
        $.ajax({
            url: "<?= base_url('Genrate_consignee/get_challan_details')?>",
            data: {challan_id:selectedValue },
            type: "POST",
            dataType: "html",

            success: function (data) {
                $(".all_challan_details").html(data);
            }
        });
    }

	$(document).ready(function() {
		// $('.append_challan').hide();
			// alert();
        $('.edit_check').click(function(){
            if(isemptyfocus('edit_warehouse_id') || isemptyfocus('despatch_id') || isemptyfocus('destination_id'))
            {
                return false;
            }
        });  
    });

    $(".new_challan").click(function () {
	  	$(".container2").append($(".append_challan").html());
	  	
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });

        $('.date').datepicker({
      	 	format: 'dd/mm/yyyy',
	        todayHighlight: true,
	        autoclose: true
      	});
	}); 

	$("#edit_warehouse_id").change(function(){
    	var id = $('#edit_warehouse_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_consignee/get_warehouse_address')?>",
            data: {warehouse_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".warehouse_address_view").html(data); 
            }
        });
    }); 

</script>
		