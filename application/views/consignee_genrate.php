<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Consigenment Note List
				</div>
				<div class="modal-body" style="overflow-x: auto;">
	                <div class="row">
	                	<div class="col-md-12">
	                		<a href="<?= base_url('genrate_consignee/add')?>"><button type="button" class="btn btn-success btn-sm">Add consigenment note</button></a><br><br><br>

	                        <table class="table table-striped" id="dataTable">
								<thead>
									<tr>
										<th>No</th>
										<th>Warehouse Name</th>
										<th>Despatch Name</th>
										<th>Destination Name</th>
										<th>Create Date</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody>
									<?php 
										if ($consignee_details !=null) {
											foreach ($consignee_details as $key => $value) {
												$id = $value['consignee_id'];
											?>
												<tr> 
													<td><?= $key+1;?></td>
													<td><?= $value['warehouse_name']?></td>	
													<td><?= $value['despatch_name']?></td>	
													<td><?= $value['destination_name']?></td>
													<td><?= $value['create_date']?></td>	
													<td style="width: 120px;">   

							                            <a href="<?= base_url('Genrate_consignee/edit_consignee/'.$id)?>">
							                            	<button type="button" title="Edit Consignee" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button>
							                            </a>
							                            
							                            <a href="<?= base_url('Genrate_consignee/delete_consignee/'.$id)?>"><button type="button" title="Delete Consignee" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>

							                            <a href="<?= base_url('Genrate_consignee/create_pdf/'.$id)?>" target="_blank"><button type="button" title="Print Consignee" class="btn btn-primary btn-xs">Print</button></a>
							                        </td> 
												</tr>
											<?php }
										}	
									?>
								</tbody>
							</table>
	                    </div>
	                </div>
	            </div>
			</div>
		</div>

		<!-- Add Menu -->
		
		<div class="modal fade" id="add_challan" tabindex="-1">
		    <div class="modal-dialog modal-lg" role="document" overflow-y: auto;>
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
		                <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title">Add Consigenment</h4>
		            </div>

		            <div class="modal-body" style="max-height: calc(100vh - 210px); overflow-y: auto;">
		                <div class="row">
		                	<div class="col-md-12">

		                        <form action="<?= base_url('Genrate_consignee/add_consignee')?>" method="post">

		                            <div class="col-md-12 col-lg-12 row">
		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Warehouse Name</label>
											  	<select class="form-control" id="warehouse_id" name="warehouse_id" onchange="GetChallanvalue(this)" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($warehouse_details !=null) {
												    		foreach ($warehouse_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['warehouse_id']?>"><?= $value['warehouse_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div> 
										</div>

										<div class="col-md-4">
			                                <label for="">Warehouse Address</label>
			                                <div class="form-group">
												<div class="warehouse_address_view">
													<textarea class="form-control" placeholder="Address" readonly></textarea>
												</div>
			                                </div>
		                            	</div>	

		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Despatch From</label>
											  	<select class="form-control" id="despatch_name" name="despatch_name" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($despatch_details !=null) {
												    		foreach ($despatch_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['despatch_id']?>"><?= $value['despatch_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											</div> 
										</div>	                            	
		                            </div>	

		                            <div class="col-md-12 row">
		                            	<div class="col-md-4">
		                            		<div class="form-group">
		                                    	<label for="">Destination</label>
		                                    	<div class="">
											  		<select class="form-control" id="destination_name" name="destination_name" style="border-radius: 0; border-style: solid;">
												    <option value="">Select</option>
												    <?php
												    	if ($destination_details !=null) {
												    		foreach ($destination_details as $key => $value) {
												    		?>
												    			<option value="<?= $value['destination_id']?>"><?= $value['destination_name']?></option>
												    		<?php }
												    	}
												    ?>
												</select>
											  	</div>
											</div> 
										</div>  

										<div class="col-md-4" style="margin:5px 0 0 0;">
		                            		<label>Add Challan</label><br>
		                            		<input type="hidden" name="temp_id" id="temp_id" value="0">
		                            		<button type="button" class="btn btn-primary btn-sm new_challan">+</button>
		                            	</div>                          
		                            </div>

		                            <!-------------- Append new div Start-------------->

		                            <div class="container2">
		                            	<div id="challan_div1">
				                            <div class="append_challan">
				                            	<div class="col-md-12 row">
					                            	<div class="col-md-4">
				                                    	<label for="">Challan Number</label>
													  	<select onchange="GetSelectedValue(this)" class="form-control challan_id" id="challan_id1" name="challan_id[]" style="border-radius: 0; border-style: solid;">
														    <option value="">Select</option>
														    <?php
														    	/*if ($challan_details !=null) {
														    		foreach ($challan_details as $key => $value) {
														    		?>
														    			<option value="<?= $value['challan_id']?>"><?= $value['refrence_number']?></option>
														    		<?php }
														    	}*/
														    ?>
														</select>
													</div>

													<div class="all_challan_details">
														<div class="col-md-4">
						                                    <label for="">Consignee Name</label>
						                                    <input type="text" readonly class="form-control" placeholder="Consignee Name">
														</div>

														<div class="col-md-4">
							                                <label for="">Consignee Address</label>
															<textarea readonly class="form-control" placeholder="Address"></textarea>
						                            	</div>

						                            	<div class="col-md-4">
						                            		<div class="form-group">
						                                    	<label for="">Consignor Name</label>
																<input type="text" readonly class="form-control" placeholder="Consignor Name">
															</div> 
														</div>

														<div class="col-md-4">
							                                <label for="">Consignor address_view</label>
							                                <div class="form-group">
																<textarea class="form-control" placeholder="Address" readonly></textarea>
							                                </div>
						                            	</div>	

														<div class="col-md-4">
					                                    	<label for="">Invoice No</label>
													  		<input type="text" placeholder="Invoice No" class="form-control" readonly></textarea>	
														</div>

														<div class="col-md-3">
					                                    	<label for="">Item</label>
													  		<input type="text" placeholder="Item Name" class="form-control" readonly></textarea>	
														</div>

														<div class="col-md-3">
					                                    	<label for="">Item Quantity</label>
													  		<input type="text" placeholder="Item Quantity" class="form-control" readonly></textarea>	
														</div>
						                            </div>	
					                            </div>

					                            <div class="col-md-12 row">
						                            <div class="col-md-4">
				                                    	<label for="">Invoice Date</label>
				                                    	<input type="text" name="invoice_date[]" placeholder="DD/MM/YYYY" class="form-control date">	
													</div>

													<div class="col-md-4">
				                                    	<label for="">amount</label>
												  		<input type="text" name="amount[]" id="amount" placeholder="Amount" class="form-control digits"></textarea>	
													</div>

					                            	<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="">Gross Weight in KG</label>
															<input type="text" name="gross_weight[]" id="gross_weight" placeholder="Gross Weight in KG" class="form-control">	
														</div> 
													</div> 
												</div>

												<div class="col-md-12 row">
													<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="freight_amount">Freight Amount</label>
															<input type="text" name="freight_amount[]" id="freight_amount" placeholder="Freight Amount" maxlength="5" class="form-control digits">	
														</div> 
													</div> 
													
													<div class="col-md-4">
					                            		<div class="form-group">
					                                    	<label for="remarks">Remarks</label>
															<textarea name="remarks[]" id="remarks" placeholder="Remarks" class="form-control"></textarea>	
														</div> 
													</div>
												</div>

												<div class="col-md-12">
													<hr style="background-color: #2d4651; height: 2px;">	
												</div>
				                            </div>
				                        </div>
		                            </div>

		                            <!-------------- Append new div End -------------->

		                            <div class="col-md-12 col-lg-12 footer">
		                                <button type="submit" id="EditC" class="btn btn-sm btn-success check">Submit</button>
		                                <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">Cancel</button>
			                        </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
		            <div class="modal-footer">
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		</div>
		<!-- Add Menu -->
	</div>

<?php $this->load->view('include/footer')?>	


<script>
	var countchallan = 1;
	function GetSelectedValue(id) {
        var selectedValue = id.value;
        var id = id.id;
        id = id.match(/\d+/);
        id = id[0];
        $.ajax({
            url: "<?= base_url('Genrate_consignee/get_challan_details')?>",
            data: {challan_id:selectedValue },
            type: "POST",
            dataType: "html",

            success: function (data) {
                //$(".all_challan_details").html(data);
                $("#challan_div"+id+" .all_challan_details").html(data);
            }
        });
    }
    function GetChallanvalue(id) {
        var selectedValue = id.value;
        $.ajax({
            url: "<?= base_url('Genrate_consignee/get_challan')?>",
            data: {warehouse_id:selectedValue },
            type: "POST",
            dataType: 'json',

            success: function (data) {
                $(".challan_id").each(function(){
                	$(this)
			          .find('option')
			          .remove()
			          .end()
			          .append('<option value="">Select</option>')
			          .val('whatever');

			          for(var i = 0; i < data.length; i++) {

			            $(this).append($('<option>', { 
			              value: data[i]['challan_id'],
			              text : data[i]['refrence_number']
			            }));

			          }
	            })    
            }
        });
    }
    
	$(document).ready(function() {

		$('.append_challan').hide();

		$('.new_challan').click( function() {
            var counter = $('#temp_id').val();
            counter++ ;
            // alert(counter);
            $('#temp_id').val(counter);
	    });

	    $('.check').click(function(){
	    	var counter = $('#temp_id').val();

            $('#temp_id').val(counter);
	    	if (counter == 0) {
	    		alert('Add Challan After Submit...!');
	    		return false;
	    	}
	    	var vehical_id = $('.vehical_id').map(function() {return this.value;}).get();
	    	if(!!vehical_id.reduce(function(a, b){ return (a === b) ? a : NaN; }) == false){
	    		alert('All Challan vehical number should be same...!');
	    		return false;
	    	}
	    	
	    });

        $('.check').click(function(){
            if(isemptyfocus('warehouse_id') || isemptyfocus('despatch_name') || isemptyfocus('destination_name') || isemptyfocus('despatch_name') || isemptyfocus('destination_id') || isemptyfocus('date') || isemptyfocus('gross_weight') || isemptyfocus('freight_amount') || isemptyfocus('remarks') || isemptyfocus('challan_id')){
                return false;
            }
        }); 
    }); 

    $(".new_challan").click(function () {
	  	//$(".container2").append($(".append_challan").html());
	  	countchallan++;
	  	$(".container2").append('<div id="challan_div'+countchallan+'">'+$(".append_challan").html()+'</div>');
	  	$('.challan_id:last').attr('id','challan_id'+countchallan);
		$(".digits").keypress(function (e) {
        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            	return false;
        	}
        });

        $('.date').datepicker({
      	 	format: 'dd/mm/yyyy',
	        todayHighlight: true,
	        autoclose: true
      	});
	});  

	$("#warehouse_id").change(function(){
    	var id = $('#warehouse_id').val();
    	// alert(id);
        $.ajax({
            url: "<?= base_url('Genrate_consignee/get_warehouse_address')?>",
            data: {warehouse_id:id },
            type: "POST",
            dataType: "html",

            success: function (data) {
            	// alert(data);
                $(".warehouse_address_view").html(data); 
            }
        });
    });   
</script>
		