<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Consignee List
					<!-- <span class="label label-info pull-right">10 Items</span> -->
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_consignee">Add Consignee</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Consignee Name</th>
								<th>Consignee Address</th>
								<th>Create Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($consignee_details !=null) {
									foreach ($consignee_details as $key => $value) {
										$id = $value['consignee_id'];
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['consignee_name']?></td>	
											<td><?= $value['consignee_address']?></td>	
											<td><?= $value['create_date']?></td>										
											<td>              
					                            <a href="javascript:void(0)" onclick="edit_consignee('<?=$value['consignee_id']?>','<?=$value['consignee_name']?>','<?=$value['consignee_address']?>');">

					                            	<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button></a>
					                            
					                            <a href="<?= base_url('Consignee/delete_consignee/'.$id)?>" target="_blank"><button type="button" title="Delete Consignee" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Modal -->

	<!-- Add Menu -->
	
	<div class="modal fade" id="add_consignee" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add Consignee</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                	<div class="col-md-12">

	                        <form action="<?= base_url('Consignee/add_consignee') ?>" method="post" enctype="multipart/form-data" id="coupon">
	                           
	                            <div class="col-md-12 col-lg-12">

	                            	<div class="form-group">
	                                    <label for="">Consignee Name</label>
	                                    <input type="text" name="consignee_name" id="consignee_name" class="form-control" value="" placeholder="Consignee Name">
	                                    <label id="consignee_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Consignee Address</label>
	                                    <textarea name="consignee_address" id="consignee_address" class="form-control" value="" placeholder="Consignee Address"></textarea>
	                                    <label id="consignee_address-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<!-- /.modal -->
	<!-- Add Menu -->

	<!-- edit Menu -->

	<div class="modal fade" id="editwarehouse" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit Consignee</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('Consignee/update_consignee') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="consignee_id" type="hidden" name="consignee_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">

	                            	<div class="form-group">
	                                    <label for="">Consignee Name</label>
	                                    <input type="text" name="consignee_name" id="edit_consignee_name" class="form-control" value="" placeholder="Consignee Name">
	                                    <label id="consignee_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Consignee Address</label>
	                                    <textarea name="consignee_address" id="edit_consignee_address" class="form-control" value="" placeholder="Consignee Address"></textarea>
	                                    <label id="consignee_address-error" class="text-danger pull-right"></label>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- edit Menu -->

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('consignee_name') || isemptyfocus('consignee_address')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_consignee_name') || isemptyfocus('edit_consignee_address')){
                return false;
            }
        });     
    });

    function edit_consignee(id,name,address)
    {
    	// alert(id);
	    $('#consignee_id').val(id);
	    $('#edit_consignee_name').val(name);
	    $('#edit_consignee_address').val(address);
      	$("#editwarehouse").modal('show');
    } 
</script>
		