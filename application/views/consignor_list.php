<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Consignor List
					<!-- <span class="label label-info pull-right">10 Items</span> -->
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_consignor">Add Consignor</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Consignor Name</th>
								<th>Consignor Address</th>
								<th>Create Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($consignor_details !=null) {
									foreach ($consignor_details as $key => $value) {
										$id = $value['consignor_id'];
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['consignor_name']?></td>
											<td><?= $value['consignor_address']?></td>	
											<td><?= $value['create_date']?></td>										
											<td>              
					                            <a href="javascript:void(0)" onclick="edit_consignor('<?=$value['consignor_id']?>','<?=$value['consignor_name']?>','<?=$value['consignor_address']?>');">

					                            	<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button></a>
					                            
					                            <a href="<?= base_url('Consignor/delete_consignor/'.$id)?>"><button type="button" title="Delete Consignor" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Modal -->

	<!-- Add Menu -->
	
	<div class="modal fade" id="add_consignor" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add Consignor</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                	<div class="col-md-12">

	                        <form action="<?= base_url('Consignor/add_consignor') ?>" method="post" enctype="multipart/form-data" id="coupon">
	                           
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Consignor Name</label>
	                                    <input type="text" name="consignor_name" id="consignor_name" class="form-control txtonly" value="" placeholder="Consignor Name" />
	                                    <label id="consignor_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Consignor Address</label>
	                                    <textarea name="consignor_address" id="consignor_address" class="form-control" value="" placeholder="Address"></textarea>
	                                    <label id="consignor_address-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<!-- /.modal -->
	<!-- Add Menu -->

	<!-- edit Menu -->

	<div class="modal fade" id="editwarehouse" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit Consignor</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('Consignor/update_consignor') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="consignor_id" type="hidden" name="consignor_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Consignor Name</label>
	                                    <input type="text" name="edit_consignor_name" id="edit_consignor_name" class="form-control txtonly" value="" placeholder="Consignor Name" />
	                                    <label id="consignor_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Consignor Address</label>

	                                    <textarea name="edit_consignor_address" id="edit_consignor_address" class="form-control" value="" placeholder="Address"></textarea>
	                                    <label id="consignor_address-error" class="text-danger pull-right"></label>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- edit Menu -->

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('consignor_name') || isemptyfocus('consignor_address')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_consignor_name') || isemptyfocus('edit_consignor_address')){
                return false;
            }
        });     
    });

    $('.txtonly').keypress(function(){
        if(alfabet_with_special_char('consignor_name') || alfabet_with_special_char('edit_consignor_name'))
        {
            return true;
        }
    }); 

    function edit_consignor(id,w_name,address,u_name)
    {
    	// alert(id);
	    $('#consignor_id').val(id);
	    $('#edit_consignor_name').val(w_name);
	    $('#edit_consignor_address').val(address);
      	$("#editwarehouse").modal('show');
    } 
</script>
		