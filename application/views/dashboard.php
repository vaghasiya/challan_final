<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div id="breadcrumb">
			<ul class="breadcrumb">
				 <li><i class="fa fa-home"></i><a href="index-2.html"> Home</a></li>
				 <li class="active">Dashboard</li>	 
			</ul>
		</div><!-- /breadcrumb-->
		<div class="main-header clearfix">
			<div class="page-title">
				<h3 class="no-margin">Dashboard</h3>
				<span>Welcome back Mr.<?= $this->session->userdata('user_name');?></span>
			</div><!-- /page-title -->				
		</div><!-- /main-header -->
		
		<div class="padding-md">
			<?php $this->load->view('include/messages')?>
			
			<div class="row">
				<?php
					if ($this->session->userdata('status') != '0') {
					?>
						<div class="col-sm-6 col-md-3">
							<a href="<?= base_url('warehouse')?>">
								<div class="panel-stat3 bg-danger">
									<h2 class="m-top-none" id="userCount"><?= ($total_warehouse !=null) ? count($total_warehouse) : '';?></h2>
									<h5>Total Warehouse</h5>
									<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">5% Higher than last week</span> -->
									<div class="stat-icon">
										<i class="fa fa-user fa-3x"></i>
									</div>
									<div class="refresh-button">
										<i class="fa fa-refresh"></i>
									</div>
									<div class="loading-overlay">
										<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
									</div>
								</div>
							</a>
						</div><!-- /.col -->

						<div class="col-sm-6 col-md-3">
							<a href="<?= base_url('consignor')?>">
								<div class="panel-stat3 bg-info">
									<h2 class="m-top-none"><span id="serverloadCount"><?= ($total_consignor !=null) ? count($total_consignor) : '';?></span></h2>
									<h5>Total Consignor</h5>
									<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">1% Higher than last week</span> -->
									<div class="stat-icon">
										<i class="fa fa-hdd-o fa-3x"></i>
									</div>
									<div class="refresh-button">
										<i class="fa fa-refresh"></i>
									</div>
									<div class="loading-overlay">
										<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
									</div>
								</div>
							</a>
						</div><!-- /.col -->

						<div class="col-sm-6 col-md-3">
							<a href="<?= base_url('consignee')?>">					
								<div class="panel-stat3 bg-warning">
									<h2 class="m-top-none" id="orderCount"><?= ($total_consignee !=null) ? count($total_consignee) : '';?></h2>
									<h5>Total Consignee</h5>
									<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">3% Higher than last week</span> -->
									<div class="stat-icon">
										<i class="fa fa-shopping-cart fa-3x"></i>
									</div>
									<div class="refresh-button">
										<i class="fa fa-refresh"></i>
									</div>
									<div class="loading-overlay">
										<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
									</div>
								</div>
							</a>
						</div><!-- /.col -->

						<div class="col-sm-6 col-md-3">
							<a href="<?= base_url('item')?>">
								<div class="panel-stat3 bg-success">
									<h2 class="m-top-none" id="visitorCount"><?= ($total_item !=null) ? count($total_item) : '';?></h2>
									<h5>Total Item</h5>
									<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">15% Higher than last week</span> -->
									<div class="stat-icon">
										<i class="fa fa-bar-chart-o fa-3x"></i>
									</div>
									<div class="refresh-button">
										<i class="fa fa-refresh"></i>
									</div>
								</div>
							</a>
						</div><!-- /.col -->
					<?php } 
					elseif ($this->session->userdata('status') == '0') {
						?>
							<div class="col-sm-6 col-md-3">
								<a href="<?= base_url('user_list')?>">
									<div class="panel-stat3 bg-danger">
										<h2 class="m-top-none" id="userCount"><?= ($total_user !=null) ? count($total_user) : '';?></h2>
										<h5>Total User</h5>
										<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">5% Higher than last week</span> -->
										<div class="stat-icon">
											<i class="fa fa-user fa-3x"></i>
										</div>
										<div class="refresh-button">
											<i class="fa fa-refresh"></i>
										</div>
										<div class="loading-overlay">
											<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
										</div>
									</div>
								</a>
							</div><!-- /.col -->
						<?php
					}
				?>
			</div>

			<!-- <div class="row">
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-body" id="trafficWidget">
							<div id="placeholder" class="graph" style="height:250px"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div id="lineChart" style="height: 150px;"></div>
					<div id="barChart" style="height: 150px;"></div>
					<div id="donutChart" style="height: 250px;"></div>
				</div>
			</div> -->
		</div><!-- /.padding-md -->
	</div>

<?php $this->load->view('include/footer')?>	