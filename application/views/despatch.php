<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Despatch List
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_item">Add Despatch</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Despatch Name</th>
								<th>Create Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($despatch_details !=null) {
									foreach ($despatch_details as $key => $value) {
										$id = $value['despatch_id'];
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['despatch_name']?></td>	
											<td><?= $value['create_date']?></td>										
											<td>              
					                            <a href="javascript:void(0)" onclick="edit_despatch('<?=$value['despatch_id']?>','<?=$value['despatch_name']?>');">

					                            	<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button></a>
					                            
					                            <a href="<?= base_url('Despatch/delete_despatch/'.$id)?>"><button type="button" title="Delete Despatch" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Modal -->

	<!-- Add Menu -->
	
	<div class="modal fade" id="add_item" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add Despatch</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                	<div class="col-md-12">

	                        <form action="<?= base_url('Despatch/add_despatch') ?>" method="post" enctype="multipart/form-data">
	                           
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Despatch Name</label>
	                                    <input type="text" name="despatch_name" id="despatch_name" class="form-control txtOnly" value="" placeholder="Despatch Name"></textarea>
	                                    <label id="despatch_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<!-- /.modal -->
	<!-- Add Menu -->

	<!-- edit Menu -->

	<div class="modal fade" id="editwarehouse" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit Despatch</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('Despatch/update_despatch') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="despatch_id" type="hidden" name="despatch_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Despatch Name</label>

	                                    <input type="text" name="edit_despatch_name" id="edit_despatch_name" class="form-control txtOnly" value="" placeholder="Despatch Name"></textarea>
	                                    <label id="consignee_address-error" class="text-danger pull-right"></label>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- edit Menu -->

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('despatch_name')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_despatch_name')){
                return false;
            }
        });   
    });

    $('.txtOnly').keypress(function(){
        if(onlyalfabetpress('despatch_name') || onlyalfabetpress('edit_despatch_name'))
        {
            return true;
        }
    }); 

    function edit_despatch(id,despatch_name)
    {
    	// alert(id);
	    $('#despatch_id').val(id);
	    $('#edit_despatch_name').val(despatch_name);
      	$("#editwarehouse").modal('show');
    } 
</script>
		