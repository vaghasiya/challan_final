<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Destination List
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_item">Add Destination</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Destination Name</th>
								<th>Create Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($destination_details !=null) {
									foreach ($destination_details as $key => $value) {
										$id = $value['destination_id'];
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['destination_name']?></td>	
											<td><?= $value['create_date']?></td>										
											<td>              
					                            <a href="javascript:void(0)" onclick="edit_despatch('<?=$value['destination_id']?>','<?=$value['destination_name']?>');">

					                            	<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button></spatch>
					                            
					                            <a href="<?= base_url('Destination/delete_destination/'.$id)?>"><button type="button" title="Delete Destination" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Modal -->

	<!-- Add Menu -->
	
	<div class="modal fade" id="add_item" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add Destination</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                	<div class="col-md-12">

	                        <form action="<?= base_url('Destination/add_destination') ?>" method="post" enctype="multipart/form-data">
	                           
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Destination Name</label>
	                                    <input type="text" name="destination_name" id="destination_name" class="form-control txtOnly" value="" placeholder="Destination Name"></textarea>
	                                    <label id="destination_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<!-- /.modal -->
	<!-- Add Menu -->

	<!-- edit Menu -->

	<div class="modal fade" id="editwarehouse" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit Destination</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('Destination/update_destination') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="destination_id" type="" name="destination_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Destination Name</label>

	                                    <input type="text" name="edit_destination_name" id="edit_destination_name" class="form-control txtOnly" value="" placeholder="Destination Name"></textarea>
	                                    <label id="consignee_address-error" class="text-danger pull-right"></label>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- edit Menu -->

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('destination_name')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_destination_name')){
                return false;
            }
        });   
    });

    $('.txtOnly').keypress(function(){
        if(onlyalfabetpress('destination_name') || onlyalfabetpress('edit_destination_name'))
        {
            return true;
        }
    }); 

    function edit_despatch(id,destination_name)
    {
    	// alert(id);
	    $('#destination_id').val(id);
	    $('#edit_destination_name').val(destination_name);
      	$("#editwarehouse").modal('show');
    } 
</script>
		