<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kamla</title>   

        <style>
            @import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i');
            body{font-family: 'Roboto', sans-serif;}
            .heading h1{text-align:center}
            .line{display: flex;
            flex-wrap: wrap;padding-right: 15px;padding-left: 15px;}
            .left-side{max-width: 80%;position: relative;width: 100%;}
            .right-side{max-width: 20%;position: relative;width: 100%;text-align: right; margin-top: -70px;}
            .right-side span,.bottom span{border-bottom: 1px solid black;padding: 3px 0;}
            .bottom{margin: 67px 0 0 0;}
            table td {text-align:center}
            table th {height: 30px; text-align: center;}
        </style>    
    </head>

    <div id="main-container">
        <div class="padding-md">
            <div class="panel panel-default table-responsive">
                <?php $this->load->view('include/messages')?>
                
                <div class="panel-heading">
                    Genrate Consignor Report
                </div>

                <div class="modal-body">
                    <div class="modal-body">
                        <div class="row">
                            <div class="main-container">
                                <div class="main-border">
                                    <form action="<?= base_url('Genrate_consignor_report/consignor_report_details')?>" method="post">
                                        <div class="line">
                                            <div class="left-side">
                                                <div class="col-md-4 row">
                                                    <div class="form-group">
                                                        <label for="">Consignor Name</label>
                                                        <select class="form-control" id="consignor_id" name="consignor_id" style="border-radius: 0; border-style: solid;">
                                                            <option value="">Select</option>
                                                            <?php
                                                                if ($consignor_details !=null) {
                                                                    foreach ($consignor_details as $key => $value) {
                                                                    ?>
                                                                        <option value="<?= $value['consignor_id']?>" <?php echo set_select('consignor_id',$value['consignor_id']); ?>><?= $value['consignor_name']?></option>
                                                                    <?php }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div> 
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Warehouse Name</label>
                                                        <select class="form-control" id="warehouse_id" name="warehouse_id" style="border-radius: 0; border-style: solid;">
                                                            <option value="">Select</option>
                                                            <?php
                                                                if ($warehouse_details !=null) {
                                                                    foreach ($warehouse_details as $key => $value) {
                                                                    ?>
                                                                        <option value="<?= $value['warehouse_id']?>" <?php echo set_select('warehouse_id',$value['warehouse_id']); ?>><?= $value['warehouse_name']?></option>
                                                                    <?php }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div> 
                                                </div>

                                                <div class="col-md-4 row" style="margin-top: 5px; margin-left: 2px;">
                                                    <label></label>
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-sm btn-success check" value="submit">
                                                    </div>
                                                </div>

                                                <?php
                                                    if ($challan_details !=null) { ?>
                                                        <div class="row" style="margin-top: 5px; margin-left: 963px;">
                                                            <label></label>
                                                            <div class="form-group">
                                                                <input type="submit" class="btn btn-sm btn-primary check" value="Export Excel" formaction="<?= base_url('Genrate_consignor_report/excel_consignor_report')?>">
                                                            </div>
                                                        </div>
                                                    <?php }
                                                ?>
                                            </div>
                                        </div>                                                     
                                    </form>
                                </div>
                            </div>
                        </div>

                        <?php
                            if ($challan_details !=null) {

                                $total_no_qty = 0;
                                $total_weight = 0;
                                $total_invoice_amount = 0;
                                $totalitem = array();
                            ?>
                                <div class="line row" style="overflow:auto;">
                                    <table style="width:100%;" border="1" id="resultsTable" data-tableName="Test Table 2" class="table table-bordered">
                                        <tr>
                                            <th rowspan="2" style="width: 40px;">
                                                Sr No
                                            </th>

                                            <th colspan="3">
                                                Invoice Reference  
                                            </th>

                                            <th rowspan="2">
                                                Date of Dispath.
                                            </th>

                                            <th rowspan="2">
                                                Name of the Party
                                            </th>

                                            <th rowspan="2" style="min-width:100px;">
                                                From / Destination
                                            </th>

                                            <th rowspan="2">
                                                Challan No.
                                            </th>
                                            
                                            <th rowspan="2">
                                                Consignment No
                                            </th>

                                            <th rowspan="2">
                                                E-Way Bill
                                            </th>

                                            <th rowspan="2">
                                                Truck No
                                            </th>

                                            <th colspan="<?= count($total_item)?>">
                                                Details of goods
                                            </th>
                                            <th rowspan="2">
                                                Total No Qty
                                            </th>
                                            <th rowspan="2">
                                                Total Weight(kg)
                                            </th>
                                            <th rowspan="2" style="min-width: 200px;">
                                                Remarks
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="min-width: 30px;">No</td>
                                            <td style="min-width: 30px;">Date</td>
                                            <td style="min-width: 30px;">Amount</td>
                                            
                                            <?php 
                                                if($total_item !=null){
                                                    foreach ($total_item as $key => $item_row) {
                                                    ?>
                                                        <td><?= $item_row['item_name']?></td>
                                                    <?php }
                                                }
                                            ?>
                                        </tr>

                                        <?php
                                            if ($challan_details !=null) {
                                                foreach ($challan_details as $key => $challan_row) {

                                                    $invoice_no = array();
                                                    $invoice_date = array();
                                                    $invoice_amount = array();
                                                    $from_destination = array();
                                                    $consignment_no = array();
                                                    $weight_kg = array();

                                                    // invoice no start //

                                                    if ($invc_no_details !=null) {
                                                        foreach ($invc_no_details as $invc_no)
                                                        {
                                                            if ($challan_row['challan_id'] == $invc_no['challan_id'])
                                                            {  
                                                                array_push($invoice_no,$invc_no['invoice_number']);
                                                            }   
                                                        } 
                                                    }
                                                    // invoice no end //

                                                    // invoice date start //

                                                    if ($consignee_ref_no !=null) {
                                                        foreach ($consignee_ref_no as $con_ref_no)
                                                        {
                                                            if ($challan_row['challan_id'] == $con_ref_no['challan_id'])
                                                            { 
                                                                foreach ($consignee_details as $con_row)
                                                                {                       
                                                                    if ($con_row['consignee_id'] == $con_ref_no['consignee_id'])
                                                                    {
                                                                        array_push($invoice_date,date('d-m-Y',strtotime($con_ref_no['invoice_date'])));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // invoice date end //


                                                    // invoice amount start //

                                                    if ($consignee_ref_no !=null) {
                                                        foreach ($consignee_ref_no as $con_ref_no)
                                                        {
                                                            if ($challan_row['challan_id'] == $con_ref_no['challan_id'])
                                                            { 
                                                                foreach ($consignee_details as $con_row)
                                                                {                       
                                                                    if ($con_row['consignee_id'] == $con_ref_no['consignee_id'])
                                                                    {
                                                                        $total_invoice_amount = $total_invoice_amount + $con_ref_no['amount'];

                                                                        array_push($invoice_amount,$con_ref_no['amount']);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }        
                                                    // invoice amount end //

                                                    // from destination start //

                                                    if ($despatch_details !=null) {
                                                        foreach ($despatch_details as $despatch_row)
                                                        {
                                                            foreach ($consignee_ref_no as $con_ref_no)
                                                            {
                                                                if ($challan_row['challan_id'] == $con_ref_no['challan_id'])
                                                                { 
                                                                    foreach ($consignee_details as $con_row)
                                                                    {
                                                                        if ($despatch_row['despatch_id'] == $con_row['despatch_id'])
                                                                        {                                 
                                                                            if ($con_row['consignee_id'] == $con_ref_no['consignee_id'])
                                                                            {
                                                                                array_push($from_destination,$despatch_row['despatch_name']);
                                                                            }    
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }            
                                                    // from destination end //

                                                    // consignee_ref_no start //

                                                    if ($consignee_ref_no !=null) {
                                                        foreach ($consignee_ref_no as $con_ref_no)
                                                        {
                                                            if ($challan_row['challan_id'] == $con_ref_no['challan_id'])
                                                            {                                 
                                                                foreach ($consignee_details as $con_row)
                                                                {
                                                                    if ($con_row['consignee_id'] == $con_ref_no['consignee_id'])
                                                                    { 
                                                                        array_push($consignment_no,$con_row['refrence_number']);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }        
                                                    // consignee_ref_no end //


                                                    // total weight start //

                                                    if ($consignee_ref_no !=null) {
                                                        foreach ($consignee_ref_no as $con_ref_no)
                                                        {
                                                            if ($challan_row['challan_id'] == $con_ref_no['challan_id'])
                                                            {   
                                                                foreach ($consignee_details as $con_row)
                                                                {    
                                                                    
                                                                    if ($con_row['consignee_id'] == $con_ref_no['consignee_id'])
                                                                    {
                                                                        $total_weight = $total_weight + $con_ref_no['gross_weight'];
                                                                        array_push($weight_kg,$con_ref_no['gross_weight']);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } 
                                                    
                                                    // total weight end //
                                                ?>
                                                    <tr>
                                                        <td style="min-width: 30px;"><?= $key+1;?></td>
                                                        
                                                        <td style="min-width: 100px;"><?= implode(' , ', $invoice_no);?></td>

                                                        <td style="min-width: 100px;"><?= implode(' , ', $invoice_date);?></td>

                                                        <td style="min-width: 70px;"><?= implode(' , ', $invoice_amount);?></td>

                                                        <td style="min-width: 100px;"><?= $challan_row['create_date']?></td>

                                                        <td style="min-width: 100px;">
                                                            <?php
                                                                if ($consignee_name !=null) {
                                                                    foreach ($consignee_name as $key => $consignee_name_row)
                                                                    {
                                                                        if ($consignee_name_row['consignee_id'] == $challan_row['consignee_id']) {
                                                                            echo $consignee_name_row['consignee_name'];
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                        </td>

                                                        <td style="min-width: 30px;"><?= implode(' , ', $from_destination);?></td>

                                                        <td style="min-width: 30px;"><?= $challan_row['refrence_number']?></td>

                                                        <td style="min-width: 30px;"><?= implode(' , ', $consignment_no);?></td>

                                                        <td style="min-width: 100px;"><?= $challan_row['e_way_bill_no']?></td>

                                                        <td style="min-width: 100px;">
                                                            <?php
                                                                if ($vehical_details !=null) {
                                                                    foreach ($vehical_details as $key => $vehical_row) {
                                                                        if ($vehical_row['vehical_id'] == $challan_row['vehical_id']) {
                                                                            echo $vehical_row['vehical_type'];
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                        </td>
                                                        
                                                        <?php 
                                                            if($total_item !=null){

                                                                $item_qty = get_all_with_helper('tbl_challan_item','item_id','desc',array('challan_id'=>$challan_row['challan_id'],'user_id' => $this->session->userdata('login_id')));

                                                                $total_qty = 0;
                                                                $status = 0;

                                                                $td ='<td>-</td>';
                                                                
                                                                for ($i=0; $i < count($total_item); $i++) {                             
                                                                    $valid = 0;

                                                                    foreach ($item_qty as $key => $item_qty_row) {

                                                                        if ($total_item[$i]['item_id'] == $item_qty_row['item_id']  && $status = 1)
                                                                        {  
                                                                            $total_qty = $total_qty+$item_qty_row['item_quantity'];

                                                                            $total_no_qty = $total_no_qty + $item_qty_row['item_quantity']; // footer total use...

                                                                            echo '<td style="min-width: 30px;">'.$item_qty_row['item_quantity'].'</td>';
                                                                            $totalitem[$i][] = $item_qty_row['item_quantity'];
                                                                            $valid = 1;
                                                                            // echo"<pre>"; print_r($totalitem);
                                                                            break;
                                                                        }
                                                                    }
                                                                    if($valid==0){
                                                                        $totalitem[$i][] = 0;
                                                                        echo '<td style="min-width: 30px;">-</td>';
                                                                    }
                                                                }
                                                                ?>
                                                                    <td style="min-width: 70px;"><?= ($total_qty !=null) ? $total_qty : '0';?></td>
                                                                <?php
                                                            } 
                                                        ?> 
                                                        
                                                        <td><?= implode(' , ', $weight_kg);?></td>

                                                        <td>
                                                            <?php                  
                                                                if ($consignee_ref_no !=null) {
                                                                    foreach ($consignee_ref_no as $key => $con_ref_no)
                                                                    {
                                                                        if ($challan_row['challan_id'] == $con_ref_no['challan_id'])
                                                                        {   
                                                                            foreach ($consignee_details as $key => $con_row)
                                                                            {    
                                                                                
                                                                                if ($con_row['consignee_id'] == $con_ref_no['consignee_id'])
                                                                                {
                                                                                    echo $con_ref_no['remarks'];
                                                                                }
                                                                            } 
                                                                        }   
                                                                    } 
                                                                }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php } 
                                            }
                                        ?>
                                        <tr>
                                            <td style="font-color: #ffffff;"></td>
                                            <td></td>                                            
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                           
                                            <?php 
                                                if($total_item !=null){
                                                    for ($i=0; $i < count($total_item); $i++) { 
                                                        ?>
                                                            <td></td>
                                                        <?php
                                                    }
                                                }
                                            ?>                                            
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td></td>

                                            <td></td>
                                            <td></td>
                                            <td style="color: red;"><?= $total_invoice_amount;?></td>

                                            <td></td>
                                            <td></td>                                           
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                           
                                            <?php
                                                if($total_item !=null){
                                                    for ($i=0; $i < count($total_item); $i++) {    
                                                        ?>
                                                            <td style="color: red;"><?= array_sum(array_map(function($item) { return $item; },$totalitem[$i]))?></td>
                                                        <?php
                                                    }
                                                }
                                            ?>                                            
                                            <td style="color: red;"><?= $total_no_qty ?></td>
                                            <td style="color: red;">
                                                <?= ($total_weight !=null && $total_weight) ? $total_weight : '0';?>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            <?php } 
                            else{
                                ?>
                                    <center><h3>Record Not Available...!</h3></center>
                                <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer')?> 
</html>

 <!-- end of excel sheet code -->
<script>
    $(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('consignor_id') || isemptyfocus('warehouse_id')){
                return false;
            }
        });

        $(document).ready(function() {
            ResultsToTable();
            
            function ResultsToTable(){    
                $("#resultsTable").table2excel({
                    exclude: ".noExl",
                    name: "Results"
                });
            }
        }); 
    });
</script>