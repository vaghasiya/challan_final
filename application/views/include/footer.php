		<!-- Footer
		================================================== -->
		<footer>
			<div class="row">
				<div class="col-sm-6">
					<span class="footer-brand">
						<strong class="text-danger">Crystal</strong> Admin
					</span>
					<p class="no-margin">
						&copy; <?= date('Y')?> <strong>Crystal Admin</strong>. ALL Rights Reserved. 
					</p>
				</div><!-- /.col -->
			</div><!-- /.row-->
		</footer>
		
		
		<!--Modal-->
		<div class="modal fade" id="newFolder">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4>Create new folder</h4>
      				</div>
				    <div class="modal-body">
				        <form>
							<div class="form-group">
								<label for="folderName">Folder Name</label>
								<input type="text" class="form-control input-sm" id="folderName" placeholder="Folder name here...">
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button class="btn btn-sm btn-success" data-dismiss="modal" aria-hidden="true">Close</button>
						<a href="#" class="btn btn-danger btn-sm">Save changes</a>
				    </div>
			  	</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div><!-- /wrapper -->

	<a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
	
	<!-- Logout confirmation -->
	<div class="custom-popup width-100" id="logoutConfirm">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to logout?</h4>
		</div>

		<div class="text-center">
			<a class="btn btn-success m-right-sm" href="<?= base_url('Login/Logout')?>">Logout</a>
			<a class="btn btn-danger logoutConfirm_close">Cancel</a>
		</div>
	</div>
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
	<!-- Jquery -->
	<script src="<?= base_url()?>assets/js/jquery-1.10.2.min.js"></script>

	<!-- Bootstrap -->
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.js"></script>
	
	<!-- Colorbox -->
	<script src='<?= base_url()?>assets/js/jquery.colorbox.min.js'></script>	

	<!-- Sparkline -->
	<script src='<?= base_url()?>assets/js/jquery.sparkline.min.js'></script>
	
	<!-- Pace -->
	<script src='<?= base_url()?>assets/js/uncompressed/pace.js'></script>
	
	<!-- Pace -->
	<script src='<?= base_url()?>assets/js/pace.min.js'></script>

	<!-- Popup Overlay -->
	<script src='<?= base_url()?>assets/js/jquery.popupoverlay.min.js'></script>
	
	<!-- Slimscroll -->
	<script src='<?= base_url()?>assets/js/jquery.slimscroll.min.js'></script>
	
	<!-- Modernizr -->
	<script src='<?= base_url()?>assets/js/modernizr.min.js'></script>
	
	<!-- Cookie -->
	<script src='<?= base_url()?>assets/js/jquery.cookie.min.js'></script>
	
	<!-- Endless -->
	<!-- <script src="<?= base_url()?>assets/js/endless/endless_dashboard.js"></script> -->
	<script src="<?= base_url()?>assets/js/endless/endless.js"></script>

	<script src="<?= base_url()?>assets/js/table2excel.js"></script>

	<!-- new add -->
	<script src='<?= base_url()?>assets/js/jquery.dataTables.min.js'></script>
	<script src="<?= base_url('assets/plugins/validation/common.js') ?>"></script>	

 	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>

 	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

	<script>
		$(document).ready(function () {
	        $(".digits").keypress(function (e) {
	        	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	            	return false;
	        	}
	        });

	        $('.txtOnly').bind('keydown', function(event) {
			  var key = event.which;
			  if (key >=48 && key <= 57) {
			    event.preventDefault();
			  }
			});
	    });

		$(function(){
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"scrollX": true,
				"sPaginationType": "full_numbers"
			});
		});

		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete?");
		  if (x)
		      return true;
		  else
		    return false;
		}

		$('.date').datepicker({
      	 	format: 'dd-mm-yyyy',
	        todayHighlight: true,
	        autoclose: true
      	});

		$(".alert").fadeTo(2000, 500).slideUp(500, function(){
		    $(".alert").slideUp(500);
		});
	</script>	
  </body>
<!-- Mirrored from minetheme.com/Endless1.5.1/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 08 Jun 2016 07:01:12 GMT -->
</html>
