<aside class="fixed skin-6">
	<div class="sidebar-inner scrollable-sidebar">
		<div class="size-toggle">
			<a class="btn btn-sm" id="sizeToggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
				<i class="fa fa-power-off"></i>
			</a>
		</div><!-- /size-toggle -->	
		<div class="user-block clearfix">
			<img src="<?= base_url()?>assets/img/user.jpg" alt="User Avatar">
			<div class="detail">
				<strong><?= $this->session->userdata('user_name');?></strong><span class="badge badge-danger m-left-xs bounceIn animation-delay4"></span>
				<!-- <ul class="list-inline">
					<li><a href="<?= base_url('Profile');?>">Profile</a></li>
					<li><a href="#" class="no-margin">Inbox</a></li>
				</ul> -->
			</div>
		</div><!-- /user-block -->

		<div class="main-menu">
			<ul>
				<li class="<?php echo ($this->uri->segment(1) == 'Dashboard') ? 'active':''?>">
					<a href="<?= base_url('Dashboard')?>">
						<span class="menu-icon">
							<i class="fa fa-desktop fa-lg"></i> 
						</span>
						<span class="text">
							Dashboard
						</span>
						<span class="menu-hover"></span>
					</a>
				</li>
				<?php
					if ($this->session->userdata('status') != '0') {
					?>
						<li class="openable open <?php echo ($this->uri->segment(1) == 'Warehouse' || $this->uri->segment(1) == 'Consignor' || $this->uri->segment(1) == 'Consignee' || $this->uri->segment(1) == 'Item' || $this->uri->segment(1) == 'Vehical') ? 'active openable open':''?>">
							<a href="#">
								<span class="menu-icon">
									<i class="fa fa-list fa-lg"></i> 
								</span>
								<span class="text">
									Setting
								</span>
								<span class="menu-hover"></span>
							</a>
							<ul class="submenu">
								<li class="<?php echo ($this->uri->segment(2) == 'warehouse_list') ? 'active':''?>">
									<a href="<?= base_url('Warehouse/warehouse_list')?>">
										<span class="menu-icon">
											<i class="fa fa-linode fa-lg"></i> 
										</span>
										<span class="text">&nbsp;Warehouse List</span> 
										<span class="menu-hover"></span>
									</a>
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'Consignor') ? 'active':''?>">
									<a href="<?= base_url('Consignor')?>">
										<span class="menu-icon">
											<i class="fa fa-file-text fa-lg"></i> 
										</span>
										<span class="text">&nbsp;Consignor</span> 
										<span class="menu-hover"></span>
									</a>					
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'Consignee') ? 'active':''?>">
									<a href="<?= base_url('Consignee')?>">
										<span class="menu-icon">
											<i class="fa fa-lastfm fa-lg"></i> 
										</span>
										<span class="text">&nbsp;Consignee</span>
										<span class="menu-hover"></span>
									</a>					
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'Item') ? 'active':''?>">
									<a href="<?= base_url('Item')?>">
										<span class="menu-icon">
											<i class="fa fa-sitemap fa-lg"></i> 
										</span>
										<span class="text">&nbsp;Item</span>
										<span class="menu-hover"></span>
									</a>					
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'Vehical') ? 'active':''?>">
									<a href="<?= base_url('Vehical')?>">
										<span class="menu-icon">
											<i class="fa fa-motorcycle fa-lg"></i> 
										</span>
										<span class="text">&nbsp;Vehical</span>
										<span class="menu-hover"></span>
									</a>					
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'despatch') ? 'active':''?>">
									<a href="<?= base_url('despatch')?>">
										<span class="menu-icon">
											<i class="fa fa-adjust fa-lg"></i> 
										</span>
										<span class="text">Despatch</span>
										<span class="menu-hover"></span>
									</a>					
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'destination') ? 'active':''?>">
									<a href="<?= base_url('destination')?>">
										<span class="menu-icon">
											<i class="fa fa-grav fa-lg"></i> 
										</span>
										<span class="text">Destination</span>
										<span class="menu-hover"></span>
									</a>					
								</li>
							</ul>
						</li>

						<li class="<?php echo ($this->uri->segment(1) == 'Genrate_challan' || $this->uri->segment(2) == 'challan_add') ? 'active':''?>">
							<a href="<?= base_url('Genrate_challan')?>">
								<span class="menu-icon">
									<i class="fa fa-ravelry fa-lg"></i> 
								</span>
								<span class="text">
									Genrate Challan
								</span>
								<span class="menu-hover"></span>
							</a>					
						</li>

						<li class="<?php echo ($this->uri->segment(1) == 'Genrate_consignee' || $this->uri->segment(2) == 'add') ? 'active':''?>">
							<a href="<?= base_url('Genrate_consignee')?>">
								<span class="menu-icon">
									<i class="fa fa-wpexplorer fa-lg"></i> 
								</span>
								<span class="text">
									Consigenment note
								</span>
								<span class="menu-hover"></span>
							</a>					
						</li>

						<li class="openable open <?php echo ($this->uri->segment(1) == 'genrate_report' || $this->uri->segment(1) == 'genrate_consignor_report' || $this->uri->segment(2) == 'report_details' || $this->uri->segment(2) == 'excel_report' || $this->uri->segment(2) == 'consignor_report_details' || $this->uri->segment(2) == 'excel_consignor_report') ? 'active openable open':''?>">
							<a href="#">
								<span class="menu-icon">
									<i class="fa fa-list fa-lg"></i> 
								</span>
								<span class="text">
									Genrate report
								</span>
								<span class="menu-hover"></span>
							</a>
							<ul class="submenu">
								<li class="<?php echo ($this->uri->segment(1) == 'genrate_report' || $this->uri->segment(2) == 'report_details' || $this->uri->segment(2) == 'excel_report') ? 'active':''?>">
									<a href="<?= base_url('genrate_report')?>">
										<span class="menu-icon">
											<i class="fa fa-chain-broken fa-lg"></i> 
										</span>
										<span class="text">
											Genrate by warehouse
										</span>
										<span class="menu-hover"></span>
									</a>					
								</li>

								<li class="<?php echo ($this->uri->segment(1) == 'genrate_consignor_report' || $this->uri->segment(2) == 'consignor_report_details' || $this->uri->segment(2) == 'excel_consignor_report') ? 'active':''?>">
									<a href="<?= base_url('genrate_consignor_report')?>">
										<span class="menu-icon">
											<i class="fa fa-chain-broken fa-lg"></i> 
										</span>
										<span class="text">
											Genrate by consignor
										</span>
										<span class="menu-hover"></span>
									</a>					
								</li>
							</ul>	
						</li>
					<?php } 
				?>			

				<?php
					if ($this->session->userdata('status') == '0') {
					?>
						<li class="<?php echo ($this->uri->segment(1) == 'user_list') ? 'active':''?>">
							<a href="<?= base_url('user_list')?>">
								<span class="menu-icon">
									<i class="fa fa-wpexplorer fa-lg"></i> 
								</span>
								<span class="text">
									User List
								</span>
								<span class="menu-hover"></span>
							</a>					
						</li>	
					<?php }
				?>
			</ul>
		</div><!-- /main-menu -->
	</div><!-- /sidebar-inner -->
</aside>