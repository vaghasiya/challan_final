<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <link rel="stylesheet" href="<?= base_url()?>assets/challan/css/style.css"> --> 	

  	<style type="text/css">
  		@import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i');
		body{font-family: 'Roboto', sans-serif;}
		.main-container{max-width: 90%;margin: auto;}
		.main-border{border: 1px solid #000000;}
		.b-bottom {border-bottom: 1px solid #000000;}
		.b-right {border-right: 1px solid #000000;}
		.lh-32{line-height: 32px;}
		.t-color{color: #f0ffff00;}
		.tb{border-spacing: 0px;}
		.tb-bordered td, .tb-bordered th {
			text-align: center;
			border-right: 1px solid #000000;
		    border-bottom: 1px solid #000000;
		}
		.b-right-0{border-right:0 !important;}
		.bg-1{background-color: antiquewhite;}
		* {box-sizing: border-box;}
		.line {display: flex;flex-wrap: wrap;}
		.header-c {max-width: 33.333333%;text-align: center;margin-left: 33.333333%;position: relative;width: 100%;padding-right: 380px;padding-left: 0;}

		.header-r{max-width: 33.333333%;position: relative;width: 100%;min-height: 1px; text-align:right; padding: 0 20px 0 0; margin-top: -90px;}
		.heading-l{max-width: 25%;position: relative;width: 100%;padding: 15px; font-weight:bold}
		.heading-l p{margin:5px 0;}
		.heading-r{max-width: 75%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;}
		.heading-r .first-p{display: inline;margin: 0;}
		.invoice{padding: 5px 20px;}
		.footer h4 {width:100%;}
		.footer{padding: 0 16px;text-align: right;width: 100%; background-color: #cacaca; }
		.item-name p{border-bottom: 1px solid;padding: 10px;margin: 0;}
		.bd-0{border-bottom:0 !important;}
  	</style>
</head>
<body>
	<div class="main-container">
		<?php
			if ($create_challan_pdf !=null) {
				foreach ($create_challan_pdf as $key => $value) {
				?>
					<div class="main-border">
						<div class="b-bottom">
							<div class="line bg-1">
								<div class="header-c" >
									<h5>
										CHALLAN
									</h5>
									<h3><?= $value['warehouse_name']?></h3>
									<h4><?= $value['warehouse_address']?></h4>
								</div>

								<div class="header-r">
									<p>Challan No :<span><u><?= $value['refrence_number'];?></u></span></p>
									<p>Date :<span><u><?= date('d-m-Y')?></u></span></p>
								</div>
							</div>
						</div>

						<div class="line b-bottom">
							<div style="padding: 10px; width: 500px; height: 180px; border-style: ridge; border-style: double; border-width: 2px; padding: 10px;">
								<p><b>CONSIGNOR :</b>
								<?= $value['consignor_name'];?></p>
								<!-- <p class="t-center">or</p> -->
								<p><?= $value['consignor_address'];?></p>
							</div>

							<div style="margin-left: 50%; margin-top: -200px; width: 700px; height: 180px; border-style: ridge; border-style: double; border-width: 2px; padding: 10px;">
								<p class="text-bold first-p"><b>CONSIGNEE : </b>
									<?= $value['consignee_name']?></p>
									<p><?= $value['consignee_address']?></p>
								</p>
								<!-- <p class="first-p b-bottom lh-32"></p> -->
							</div>
						</div>

						<div class="line">
							<!--<table class="tb tb-bordered" cellpadding="10" width="100%">
								<thead>
								  <tr>
									<th class="border-l-0">No. of packages</th>
									<th>Item name</th>
									<th>Qnty.</th>
									<th class="border-r-0">Particulars of Goods</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td rowspan="5" class="border-l-0">6</td>
								  </tr>
								  <tr>
									<td>TYRES</td>
									<td>2</td>
									<td rowspan="4" class="border-r-0">Description</td>
								  </tr>
								  <tr>
									<td>TUBE</td>
									<td>2</td>
								  </tr>
								  <tr>
									<td>FALP.</td>
									<td>2</td>
								  </tr>
								  <tr>
									<td>OTHERS</td>
									<td>1</td>
								  </tr>
								</tbody>
						  </table>-->
						  <table class="tb tb-bordered" cellpadding="10" width="100%">
								<thead>
								  <tr>
									<th>No. of packages</th>
									<th>Item name</th>
									<th>Qnty.</th>
									<th class="b-right-0">Particulars of Goods</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
								  	<?php
								  		$item_quantity1 = 0;

										if ($item_quantity !=null) {
											foreach ($item_quantity as $key => $item_qut_row) {
												$item_quantity1 = $item_quantity1+$item_qut_row['item_quantity'];
											}
											?><td><?= $item_quantity1?></td><?php
										}
									?>

									<td class="border-l-0 item-name" style="padding:0">
										<?php
											if ($item_quantity !=null) {
												foreach ($item_quantity as $key => $item_qut_row) {
												?>
													<p><?= $item_qut_row['item_name']?></p>
												<?php }
											}
										?>
									</td>

									<td class="border-l-0 item-name" style="padding:0">
										<?php
											if ($item_quantity !=null) {
												foreach ($item_quantity as $key => $item_qut_row) {
												?>
													<p class="bd-0"><?= $item_qut_row['item_quantity']?></p>
												<?php }
											}
										?>
									</td>
									<td class="b-right-0"><?= $value['perticulars_goods']?></td>
								  </tr>								  
								</tbody>
						  </table>	
						</div>
						<div class="line b-bottom invoice">
							<p class="">
								Received goods in good Condition Againts</p>
							<p class="">
								<strong>Invoice  No. </strong>
								<?php 
									if ($invoice_number_details !=null) {
										foreach ($invoice_number_details as $key => $invc_no_row) {
											?>
												<?= $invc_no_row['invoice_number'].','?>
											<?php
										}
									}
								?><br><br>
								<strong>E-Way Bill no.</strong>
								<?= $value['e_way_bill_no']?>
							</p>
							<p class="">
								<strong>Vehicle  No. </strong> <?= $value['vehical_number']?> (<?= $value['vehical_type']?>)
							</p>
						</div>
						<div class="line footer" style="min-height:100px;">
								<h4 class="t-right">
									<?= $value['warehouse_name']?>
								</h4>
							</div>
						</div>
					</div>
				<?php } 
			}
		?>
	</div>
</body>
</html>
