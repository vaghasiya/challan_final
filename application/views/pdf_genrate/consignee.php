<!DOCTYPE html>
<html lang="en">
<head>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Kamla</title>
	<style>
	@import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i');
	body{font-family: 'Roboto', sans-serif;}
	.heading h1{text-align:center}
	.line{display: flex;
    flex-wrap: wrap;padding-right: 15px;padding-left: 15px;}
	.left-side{max-width: 80%;position: relative;width: 100%;}
	.right-side{max-width: 20%;position: relative;width: 100%;text-align: right; margin-top: -70px;}
	.right-side span,.bottom span{border-bottom: 1px solid black;padding: 3px 0;}
	.bottom{margin: 20px 0 0 0;}
	table td {text-align:center}
	</style>
</head>

<body>
	<div class="heading">
		<h1>CONSIGNMENT NOTE</h1>
	</div>
	<div class="line">
		<div class="left-side">
			<h2 style="margin: 0;"><?= ($consignee_pdf !=null) ? $consignee_pdf[0]['warehouse_name'] : '';?></h2>
			<h4 style="margin: 5px 0;"><?= ($consignee_pdf !=null) ? $consignee_pdf[0]['warehouse_address'] : '';?></h4>
			<!-- <p style="margin-top: 5px;margin-bottom: 6px;">C &amp; F Agent :- J.K.TYRE &amp; INDUSTRIES LIMITED &amp; CAVENDISH INDUSTRIES LIMITED. </p> -->
		</div>
		<div class="right-side">
			<p>No &nbsp;&nbsp;&nbsp;: <span><?= ($consignee_pdf !=null) ? $consignee_pdf[0]['refrence_number'] : '';?></span> </p>
			<p>Date : <span><?= date('d-m-Y')?></span> </p>
		</div>
	</div>
	<hr>
	<div class="line">
		<span>Consignor : 
			<?php
				if ($consignor_name !=null) {
					$consignor = array();
					foreach ($consignor_name as $key => $value) {
						$consignorname = $value['consignor_name'].' , '.$value['consignor_address'];
						if(!in_array($consignorname, $consignor)){
							echo '<input type="checkbox" id="myCheck" checked="checked"> '.$consignorname.'</span>';
							$consignor[] = $consignorname;	
						}
						

					}
					?>
						<!-- <input type="checkbox" id="myCheck" checked="checked"> <?= $value['consignor_name']?> , <?= $value['consignor_address']?></span> -->
					<?php 
				}
			?>
		<!-- <span><input type="checkbox" id="myCheck2"> CAVANDISH Industries Limited,Kenduguri,Jorhat - 785010(Assam)</span> -->
	</div>
	<div class="line">
		<p style="float:left;width: 50%;">Despatch From : <strong><?= ($consignee_pdf !=null) ? $consignee_pdf[0]['despatch_name'] : '';?></strong></p>
		<p style="float: right;width: 50%;text-align: right; margin-top: -1px">Destination : <strong><?= ($consignee_pdf !=null) ? $consignee_pdf[0]['destination_name'] : '';?></strong></p>
	</div>
	<div class="line">
		<table style="width:100%"  border="1">
			<tr>
				<th rowspan="2">
					Challan number
				</th>
				<th rowspan="2">
					Consignee Address
				</th>
				<th rowspan="2">
					Invoice No.
				</th>
				<th rowspan="2">
					Invoice Date
				</th>
				<th rowspan="2">
					Amount
				</th>
				<th colspan="<?= count($total_item)?>">
					Details of goods
				</th>
				<th rowspan="2">
					Quantity
				</th>
				<th rowspan="2">
					Gross Weight in KG
				</th>
				<th rowspan="2">
					Freight Amt.
				</th>
				<th rowspan="2">
					Remarks
				</th>
			</tr>	
			<tr>
				<?php 
					if($total_item !=null){
						foreach ($total_item as $key => $item_row) {
						?>
							<td><?= $item_row['item_name']?></td>
						<?php }
					}
				?>
			</tr>

			<?php
				if ($consignee_table_data) {

					$temp_final_qty = array();
					
					$final_amount_total = 0;
					$final_quantity_total = 0;
					$final_gross_weight_total = 0;
					$final_freight_total = 0;
					$final_details_goods_total = 0;
                    $totalitem = array();
                   //echo $this->db->last_query();
					//echo "<pre>"; print_r($consignee_table_data);exit;
					foreach ($consignee_table_data as $key => $value) {
					
						$final_gross_weight_total = $final_gross_weight_total + $value['gross_weight'];
						$final_freight_total = $final_freight_total + $value['freight_amount'];
						$final_amount_total = $final_amount_total+$value['amount'];

						$invc_no = array();
						foreach ($invoice_no_details as $key => $invc_num_row) {
							if ($invc_num_row['challan_id'] === $value['challan_id']) {
								array_push($invc_no, $invc_num_row['invoice_number']);
							}
						}
					?>
						<tr>
							<td><?= $value['refrence_number']?></td>
							<td><?= $value['consignee_address']?></td>
							<td><?= ($consignee_table_data !='') ? implode(' , ', $invc_no) : '';?></td>
							<td><?= $value['invoice_date']?></td>
							<td><?= $value['amount']?></td>

							<?php 								
								if($total_item !=null){
									
									$item_qty = get_all_with_helper('tbl_challan_item','item_id','desc',array('challan_id'=>$value['challan_id'],'user_id' => $this->session->userdata('login_id')));

									//echo"<pre>"; print_r($item_qty); exit;
									$total_qty = 0;
									$status = 0;

									$temp_final_qty = array_merge($temp_final_qty, $item_qty);
									// echo"<pre>"; print_r($temp_final_qty);
                                    $td='<td>-</td>';
									// foreach ($item_qty as $key => $item_qty_row) {	
									    
										
									// 	$final_quantity_total = $final_quantity_total+$item_qty_row['item_quantity'];
                                        
									// 	if ($item_qty_row['challan_id'] == $value['challan_id']) 
									// 	{
										    
									// 		foreach ($total_item as $key => $item_row) {
											 
         //                                    //echo '<td>'.$status.'</td>';exit;
									// 		/*	if ($item_row['item_id'] == $item_qty_row['item_id']  && $status = 1)*/
									// 		if ($item_row['item_id'] == $item_qty_row['item_id']  && $status = 1)
									// 			{	
									// 			    // echo"<pre>"; print_r($item_qty_row);
									// 				// $final_details_goods_total = $final_details_goods_total+$item_qty_row['item_quantity'];
													
									// 				$total_qty = $total_qty+$item_qty_row['item_quantity'];
												
									// 				echo '<td>'.$item_qty_row['item_quantity'].'</td>';
													
									// 			}else{
									// 			    echo '<td>-</td>';
									// 			}
											
									// 			/*if ($item_row['item_id'] != $item_qty_row['item_id']) {
									// 				echo '<td>-</td>';
									// 			}*/
									// 		}
									// 	} 
									// }
                                    
                                    for ($i=0; $i < count($total_item); $i++) { 
                                    	
                                    	$valid = 0;

                                    	foreach ($item_qty as $key => $item_qty_row) {
                                    		if ($total_item[$i]['item_id'] == $item_qty_row['item_id']  && $status = 1){	
                                    			$final_quantity_total = $final_quantity_total+$item_qty_row['item_quantity'];
												$total_qty = $total_qty+$item_qty_row['item_quantity'];
												echo '<td>'.$item_qty_row['item_quantity'].'</td>';
												$totalitem[$i][] = $item_qty_row['item_quantity'];
												$valid = 1;
												break;
											}
                                    	}
                                    	if($valid==0){
                                    		$totalitem[$i][] = 0;
                                    		echo '<td>-</td>';
                                    	}
                                  		
                                    }



								// 	exit;
									?>
										<td><?= ($total_qty !=null) ? $total_qty : '0';?></td>
									<?php
								} 
							?>
							<td><?= $value['gross_weight']?></td>
							<td><?= $value['freight_amount']?></td>
							<td><?= $value['remarks']?></td>
						</tr>
					<?php }
				}
				?>
				<tr>
					<td style="color: #fff;">00000</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<?php
						if($total_item !=null){
							for ($i=0; $i < count($total_item); $i++) { 
								?>
									<td></td>
								<?php
							}
						}
					?>
				</tr>

				<tr>
					<td style="color: #fff;">00000</td>
					<td></td>
					<td></td>
					<td></td>
					<td style="color: red;"><?= ($consignee_table_data !=null) ? $final_amount_total : '0';?></td>
					<!-- <td>5</td>
					<td>6</td>
					<td>7</td> -->
					
					<?php
						if($total_item !=null){
							for ($i=0; $i < count($total_item); $i++) { 
								?>
									<td><?=array_sum(array_map(function($item) { return $item; },$totalitem[$i]))?></td>
								<?php
							}
						}
					?>
					<td style="color: red;">
						<?= ($consignee_table_data !=null && $final_quantity_total !=null && $total_item) ? $final_quantity_total : '0';?>
					</td>
					<td style="color: red;"><?= ($consignee_table_data !=null) ? $final_gross_weight_total : '0';?></td>
					<td style="color: red;"><?= ($consignee_table_data !=null) ? $final_freight_total : '0';?></td>
					<td></td>
				</tr>
		</table>
		<?php 
			// echo"<pre>"; print_r($temp_final_qty); exit;
		?>
	</div>
	<div class="line">
		<p>Service Tax payable by Consignor / Consignee</p>
	</div>
	<hr>
	<div class="line">
		<div class="left-side">
			<span>Type of Vehicle -- 
			<?php
				if ($vehical_type !=null) {
					$vehicaltype = array();
					foreach ($vehical_type as $key => $vehical_type_row) {						
						$challan = get_all_with_helper('tbl_genrate_challan','','',array('challan_id' => $vehical_type_row['challan_id'],'user_id' => $this->session->userdata('login_id')));

						$get_vehical_type = get_all_with_helper('tbl_vehical','','',array('vehical_id' => $challan[0]['vehical_id'],'user_id' => $this->session->userdata('login_id')));

						// echo"<pre>"; print_r($get_vehical_type); 
						if(!in_array($get_vehical_type[0]['vehical_type'], $vehicaltype)){
							echo ($get_vehical_type !=null) ? $get_vehical_type[0]['vehical_type'] : '';
							$vehicaltype[] = $get_vehical_type[0]['vehical_type'];
							echo '<input type="checkbox" checked="checked" disabled>';
						}
						?> 
						<?php
					}
				}
			?>
			</span>
			<!-- <span>Mini ACE<input type="checkbox" id="myCheck5"></span> -->
			
			<p class="bottom">Vehicle No. &nbsp;&nbsp;&nbsp;: 
				<?php
					if ($vehical_type !=null) {
					$vehical = array();
					foreach ($vehical_type as $key => $vehical_type_row) {						
						$challan = get_all_with_helper('tbl_genrate_challan','','',array('challan_id' => $vehical_type_row['challan_id'],'user_id' => $this->session->userdata('login_id')));

						$get_vehical_type = get_all_with_helper('tbl_vehical','','',array('vehical_id' => $challan[0]['vehical_id'],'user_id' => $this->session->userdata('login_id')));

						// echo"<pre>"; print_r($get_vehical_type); 
						if(!in_array($get_vehical_type[0]['vehical_number'], $vehical)){
							//echo ($get_vehical_type !=null) ? $get_vehical_type[0]['vehical_number'].',' : '';
							$vehical[] = $get_vehical_type[0]['vehical_number'];
							// echo"<pre>"; print_r($vehical);
						}
						
					}
					echo implode(',', $vehical);
				}
				?>

			&nbsp;&nbsp;&nbsp; Driver Signature &nbsp;&nbsp;&nbsp;: <span></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>CONSIGNER COPY</strong></p>
		</div>
		<div class="right-side">
			<p>
				For <strong><?= ($consignee_pdf !=null) ? $consignee_pdf[0]['warehouse_name'] : '';?></strong>
			</p>
			<br>
			<p>
				Signature
			</p>
		</div>
	</div>
</body>
</html>
