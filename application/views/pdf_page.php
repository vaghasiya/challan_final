<html>   
   <body>
      <h1 align="center">SUPER PACKING SERVICE </h1>  
        <div>
            <!-- <img height="90" width="100" alt="" src="<?//= base_url('assets/images/logo.png')?>" style="float:left; margin-left:50px;"></div> -->
            <center>
               <div style="width:70%; padding:20px; text-align:center; border: 2px; border-style:solid;">
              
                 
                  <table style="width:100%"> 
                     <h2 align="center" style="font-size:30px">HEAT TREATMENT CERTIFICATE </h2>
                     <p style="text-align:left;">F.H.A.T. FACILITY
                        <span style="float:right;">CODE NO.IN-420HT</span>
                     </p>
                     <p style="text-align:center;">ISPM-15</p>
                     <p style="text-align:left;">CERTIFICATE NO. <b>12345</b>
                        <span style="float:right;">ISSUE DATE <b>16/5/2017</b></span>
                     </p>
                     <p align="justifY" >  &emsp;&emsp;&emsp;&emsp;&emsp; THIS IS TO CERTIFY THAT FOLLOWING WOODEN PACKING MATERIALS WITH OUR LOGO DULY AUTHORISED AND AS PER NORMS OF ISPM-15 DIRECTED BY DIRECTORATE OF PPQS,MINISTRY OF AGRICULTURE,
                        GOVERNMENT OF INDIA.(MEMBER OF PPQ) 
                     </p>
                     <p align="center"> PARTICULARS </p>
                     <table border="2" align="center" width="100%">
                        <tr align="center" >
                           <td>DATE OF TREATMENT</td>
                           <td> TR.NO </td>
                           <td>BATCH NO.</td>
                        </tr>
                        <tr align="center">
                           <td>12345</td>
                           <td>12345</td>
                           <td>12345</td>
                        </tr>
                     </table>
                     <br>
                     <table border="2" align="center" width="100%">
                        <tr align="center">
                           <td colspan="6">EXPORTER NAME & ADDRESS</td>
                           <td colspan="6">CONSIGNMENT/SHIPPING PARTICULARS</td>
                        </tr>
                        <tr align="left">
                           <td colspan="6"><strong>12345</strong>                  
                           </td>
                           <td colspan="6"><strong>12345</strong>
                           </td>
                        </tr>
                        <tr align="center">
                           <td colspan="4">DESCRIPTION PACKING MATERIALS</td>
                           <td colspan="4">QNT.TREATED</td>
                           <td colspan="4">PORT/COUNTRY OF EXPORT</td>
                        </tr>
                        <tr align="center">
                           <td colspan="4"><b>12345</b></td>
                           <td colspan="4"><b>12345</b></td>
                           <td colspan="4"><b>12345</b></td>
                        </tr>
                        <tr align="center">
                           <td colspan="8">ATTANDING TIME OF 56.C IN WOOD CORE(MINI)</td>
                           <td colspan="4">TOTAL TIME(MIN)</td>
                        </tr>
                        <tr align="center">
                           <td colspan="8"><b>12345</b></td>
                           <td colspan="4"><b>12345</b></td>
                        </tr>
                        <tr align="center">
                           <td colspan="8">CONTAINER</td>
                           <td colspan="2">BEFORE TMT.</td>
                           <td colspan="2">AFTER TMT.</td>
                        </tr>
                        <tr align="center">
                           <td colspan="8">12345</td>
                           <td colspan="2"><b>12345</b></td>
                           <td colspan="2"><b>12345</b></td>
                        </tr>
                        <tr>
                           <td colspan="9"><strong><u>TERMS & CONDITION</u></strong></td>
                           <td colspan="3" align="center">SEAL & AUTH.SIGN</td>
                        </tr>
                        <tr>
                           <td colspan="9"> *No liabillities to or is assumed by the certifine the company,<br>
                              it's directoraes or representative in respect to this certificate.<br><br>
                              *This certificate is for standard treatment only.<br><br>
                              *Quality place,time,situation for storage,packing and stuffing is <br>
                              depend on exporter to avoid infection.<br><br>
                              *This treated wooden packing materials keeps seprate from<br>
                              untreated wood,water,dusts.etc.<br><br>
                              *Hygienically use these packing materials as possible as early.<br>
                              The effectiveness of heat treatment is best before 30 days.
                           </td>
                        </tr>
                     </table>
                  </table>
                
               </div>               
            </center>
        </div>          
      </form>
   </body>  
</html>

