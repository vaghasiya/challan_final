<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="index-2.html"> Home</a></li> 
					 <li class="active">Profile</li>	 
				</ul>
			</div><!--breadcrumb-->
			<ul class="tab-bar grey-tab">
				<li>
					<a href="javascript:void();" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-edit fa-2x"></i> 
						</span>
						Edit Profile
					</a>
				</li>
			</ul>
			<?php $this->load->view('include/messages')?>

			<div class="padding-md">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="tab-content">	
							<div class="panel panel-default">
								<div class="panel-heading">
									Profile Information
								</div>

								<div class="panel-body col-md-6">
									<form method="post" action="<?php echo base_url('Profile/change_password'); ?>">
			             		 		<label>Name</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-bars" aria-hidden="true"></i></span>
											<input class="form-control" id="name" name="name" value="<?= ($this->session->userdata('status') == '0') ? $get_details[0]['admin_name'] : $get_details[0]['user_name']?>" type="text" disabled readonly/>
										</div><br>

										<label>Old Password:</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
											<input class="form-control" name="old_password" id="old_password" type="password" placeholder="Old Password"/>
										</div><br>

										<label>New Password:</label>
										<div class="input-group">
											<span class="input-group-addon" id="password-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
											<input class="form-control" name="new_password" id="new_password" type="password" placeholder="New Password"/>
										</div><br>

										<!-- <label>Retype Password:</label>
										<div class="input-group">
											<span class="input-group-addon" id="password-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
											<input class="form-control" name="Re_Password" id="re_password" type="password" placeholder="Retype New Password"/>
										</div><br> -->

										<div class="footer">      
						                    <button type="submit" id="change_pwd" name="change_pwd" class="btn btn-success check" style="width:140px;">Change Password</button>

						                    <a href="<?php echo base_url('Dashboard'); ?>" name="clear" class="btn btn-danger pull-right " style="width:100px;">Cancel</a><br>
						                </div>
									</form>
								</div>

								<div class="panel-body col-md-6">
									<form method="post" action="<?php echo base_url('Profile/edit_profile'); ?>">
										
										<label>Name</label>
						                <div class="input-group">
						                    <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
						                    <input type="text" class="form-control txtOnly" id="user_name" placeholder="Enter name" name="Name" value="<?= ($this->session->userdata('status') == '0') ? $get_details[0]['admin_name'] : $get_details[0]['user_name']?>">
						                </div> 
						                <span class="text-danger"><?php echo form_error('UserName')?> </span><br>

						                <label>Email</label>
						                <div class="input-group">
						                    <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
						                    <input type="text" class="form-control" id="user_email" placeholder="Enter email" name="Email" value="<?= ($this->session->userdata('status') == '0') ? $get_details[0]['admin_email'] : $get_details[0]['user_email']?>">
						                </div><br>

				                  		<div class="footer">      
						                    <button type="sub	mit" id="update_profile" name="update_profile" class="btn btn-success check1" style="width:118px;">Done</button>

						                    <a href="<?php echo base_url('Dashboard'); ?>" name="clear" class="btn btn-danger pull-right " style="width:100px;">Cancel</a>
						                    <br><br>
						                </div>
						            </form>    
								</div>	
							</div><!-- /panel -->
						</div><!-- /tab-content -->
					</div><!-- /.col -->
				</div><!-- /.row -->			
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->
	</div><!-- /wrapper -->
<?php $this->load->view('include/footer')?>	

<script type="text/javascript">
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('name')  || isemptyfocus('old_password')  || isemptyfocus('new_password')  || isemptyfocus('re_password')){
                return false;
            }
        });

        $('.check1').click(function(){
            if(isemptyfocus('user_name')  || isemptyfocus('user_email')){
                return false;
            }
        });    
    });

    $('.txtOnly').keypress(function(){
        if(onlyalfabetpress('user_name'))
        {
            return true;
        }
    });
</script>
