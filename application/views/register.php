<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from minetheme.com/Endless1.5.1/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 08 Jun 2016 07:01:12 GMT -->
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">	
	<link href="<?= base_url()?>assets/css/endless.min.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?= base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">

  </head>

  <body>
	<div class="login-wrapper">
		<div class="text-center">
			<h2 class="fadeInUp animation-delay8" style="font-weight:bold">
				<span class="text-success">Admin</span> 
			</h2>
		</div>
		<div class="login-widget animation-delay1">	
			<div class="panel panel-default">
			<?php $this->load->view('include/messages')?>

				<div class="panel-heading clearfix">
					<div class="pull-left">
						<i class="fa fa-lock fa-lg"></i> Admin
					</div>

					<div class="pull-right">
						<span style="font-size:11px;">Don't have any account?</span>
						<a class="btn btn-default btn-xs login-link" href="<?= base_url('register/register_add')?>" style="margin-top:-2px;"><i class="fa fa-plus-circle"></i> Register</a>
					</div>
				</div>
				<div class="panel-body">
					<form class="form-login" method="post" action="<?= base_url('Login/doLogin')?>">
						<div class="form-group valid_email">
							<label>UserEmail</label>
							<input type="text" name="user_email" id="user_email" placeholder="UserEmail" class="form-control input-sm bounceIn animation-delay2">
						</div>
						<div style="color:red;"><p><?php echo form_error('user_email'); ?></p></div>
						<div id="valid" style="color: red"></div>

						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" id="user_password" placeholder="Password" class="form-control input-sm bounceIn animation-delay4">
						</div>
						<div style="color:red;"><p><?php echo form_error('password'); ?></p></div>

						<div class="seperator"></div>
						<!-- <div class="form-group">
							Forgot your password?<br/>
							Click <a href="javascript:void(0);" data-toggle="modal" data-target="#ForgetPassword">here</a> to reset your password
						</div> -->
						<!-- <hr/>							 -->
						<button type="submit" class="btn btn-success check">Login</button>
						<button type="reset" class="btn btn-danger pull-right">Cancel</button>
					</form>
				</div>
			</div><!-- /panel -->
		</div><!-- /login-widget -->
	</div><!-- /login-wrapper -->

	<!-- View Information Modal -->
	<div id="ForgetPassword" class="modal fade" role="document" tabindex='-1' style="margin-top: 5%;">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title">Forget Passsword</h3>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	          <div class="col-md-1"></div>
	          <div class="col-md-10">
	            <label> Enter Your Email Here To Receive Your Password.</label>
	          </div>
	          <div class="col-md-1"></div>
	        </div><!-- end row-->
	        <!-- Error And Success Message -->
	        <div class="row">
	            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	               <!--  <div id="emailmessage" class="alert">
	                </div> -->
	            </div>
	        </div>
	      <!-- Error And Success Message -->
	        <div class="row">
	          <div class="col-md-1"></div>
	          <div class="col-md-10">

	            <form class="form" method="post" action="<?= base_url('Login/forgotPassword')?>"><!-- Profile/forgot_password-->
	              <!-- <label>Email</label> -->
	              <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
	                <input type="text" class="form-control" id="UserEmail" placeholder="Enter Email" name="UserEmail">
	              </div>
	              <label id="UserEmail-error" class="text-danger"></label>
	              <br>
	              <div class="footer">      
	                <button type="submit" id="ForgetUser" name="ForgetUser" class="btn btn-success" style="width:118px;">Send</button>
	                <br><br>
	              </div>
	            </form>
	          </div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script src="<?= base_url()?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script src="<?= base_url('assets/plugins/validation/common.js') ?>"></script>	

    <script type="text/javascript">
    	$(document).ready(function() {
	        $('.check').click(function(){
	            // if(isemptyfocus('user_name') || isemptyfocus('user_password')){
	            //     return false;
	            // }

	            if(isvalidemail('user_email')){
	                return false;
	            }
	        }); 

	        $('.txtOnly').bind('keydown', function(event) {
			  var key = event.which;
			  if (key >=48 && key <= 57) {
			    event.preventDefault();
			  }
			}); 

			$("#user_email").keyup(function(){
				var email = $("#user_email").val();
		        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		        
		        if (!filter.test(email)) {
		           $("#valid").text(email+" is not a valid email");
		           email.focus();
		           return false;
		        } else {
		            $("#valid").text("");
		        }
		    });
	    });
    </script>
  </body>
</html>

