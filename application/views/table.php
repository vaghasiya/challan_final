<!-- <div style="margin-top:10px;background-color:blue;width: 100%;height:50px;">
   </div> -->
<html>
   <head>
      <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?>">
      <script src="<?= base_url('assets/js/jquery.min.j')?>"></script>
      <script src="<?= base_url('assets/js/bootstrap.min.js')?>"></script>
      <!-- <center> -->
      <style type="text/css"  media="print">
         table tr td p{
         font-size: 4px;
         }
         td{
         font-size: 4px;
         }

          @page {
             size: auto;   /* auto is the initial value */
             margin: 20px;  /* this affects the margin in the printer settings */
         }
      </style>

   </head>
   <body>
      <form method="post">
         <div id="OrderDetails">
            <h1 align="center" style="font-size:20px">SUPER PACKING SERVICE </h1>
            <div class="pull-right" id="hide">
               <div class="col-md-6">
                  <a href="<?= base_url('Dompdf_test/create_pdf')?>"><button type="button" class="btn btn-warning" style="margin-top:-3%;">PDF</button></a>
               </div>
               <div class="col-md-6">
                  <!-- <button type="button" class="btn btn-primary pull-right" id="print_button" onclick="PrintInvoice('OrderDetails')" style="margin-top:-3%;">PRINT</button>    -->
               </div>
            </div>

            <div>
               <center>
                  <div style="width:75%; padding:25px; text-align:center; border: 2px solid #787878">
                     <table style="width:100%">
                        <h2 align="center" style="font-size:20px">HEAT TREATMENT CERTIFICATE </h2>
                        <p style="text-align:left; font-size:12px;">F.H.A.T. FACILITY
                           <span style="float:right;">CODE NO.IN-420HT</span>
                        </p>
                        <p style="text-align:center;">ISPM-15</p>
                        <p style="text-align:left;">CERTIFICATE NO. <b></b>
                           <span style="float:right;">ISSUE DATE <b><?php echo $date = date("d-m-Y");?></b></span>
                        </p>
                        <p align="justifY" >&emsp;&emsp;&emsp;&emsp;&emsp; THIS IS TO CERTIFY THAT FOLLOWING WOODEN PACKING MATERIALS WITH OUR LOGO DULY AUTHORISED AND AS PER NORMS OF ISPM-15 DIRECTED BY DIRECTORATE OF PPQS,MINISTRY OF AGRICULTURE,
                           GOVERNMENT OF INDIA.(MEMBER OF PPQ) 
                        </p>
                        <p align="center"> PARTICULARS </p>
                        <table border="2" align="center" class="fntsize" width="100%">
                           <tr align="center" >
                              <td style="font-size:14px;">DATE OF TRITMENT</td>
                              <td style="font-size:14px;"> TR.NO </td>
                              <td style="font-size:14px;">BATCH NO.</td>
                           </tr>
                           <tr align="center">
                              <td style="font-size:14px;"><strong></strong></td>
                              <td style="font-size:14px;"><strong></strong></td>
                              <td style="font-size:14px;"><strong></strong></td>
                           </tr>
                        </table>
                        <br>
                        <table border="2" align="center" class="fntsize" width="100%">
                           <tr align="center">
                              <td colspan="6"  style="font-size:14px;">EXPORTER NAME & ADDRESS</td>
                              <td colspan="6"  style="font-size:14px;">CONSIGNMENT/SHIPPING PARTICULARS</td>
                           </tr>
                           <tr align="left">
                              <td colspan="6" style="height:100px;  font-size:14px;"><strong></strong>

                              </td>
                              <td colspan="6"  style="font-size:14px;"><strong></strong>
                              </td>
                           </tr>
                           <tr align="center">
                              <td colspan="4"  style="font-size:14px;">DESCRIPTION PACKING MATERIALS</td>
                              <td colspan="4"  style="font-size:14px;">QNT.TREATED</td>
                              <td colspan="4"  style="font-size:14px;">PORT/COUNTRY OF EXPORT</td>
                           </tr>
                           <tr align="center">
                              <td colspan="4"  style="font-size:14px;"><b></b></td>
                              <td colspan="4"><b></b></td>
                              <td colspan="4"><b></b></td>
                           </tr>
                           <tr align="center">
                              <td colspan="8" style="font-size:14px;">ATTANDING TIME OF 56.C IN WOOD CORE(MINI)</td>
                              <td colspan="4" style="font-size:14px;">TOTAL TIME(MIN)</td>
                           </tr>
                           <tr align="center">
                              <td colspan="8" style="font-size:14px;"><b> MINS </b></td>
                              <td colspan="4" style="font-size:14px;"><b> MINS </b></td>
                           </tr>
                           <tr align="center">
                              <td colspan="8" style="font-size:14px;">CONTAINER NO</td>
                              <td colspan="2" style="font-size:14px;">BEFORE TMT.</td>
                              <td colspan="2" style="font-size:14px;">AFTER TMT.</td>
                           </tr>
                           <tr align="center">
                              <td colspan="8" style="font-size:14px;"><b></b></td>
                              <td colspan="2" style="font-size:14px;"><b>%</b></td>
                              <td colspan="2" style="font-size:14px;"><b>%</b></td>
                           </tr>
                           <tr>
                              <td colspan="9" style="font-size:14px;"><strong><u>TERMS & CONDITION</u></strong></td>
                              <td colspan="3" style="font-size:14px;" align="center">SEAL & AUTH.SIGN</td>
                           </tr>
                           <tr>
                              <td colspan="9">
                                 *No liabillities to or is assumed by the certifine the company,<br>
                                 it's directoraes or representative in respect to this certificate.<br><br>
                                 *This certificate is for standard treatment only.<br><br>
                                 *Quality place,time,situation for storage,packing and stuffing is <br>
                                 depend on exporter to avoid infection.<br><br>
                                 *This treated wooden packing materials keeps seprate from<br>
                                 untreated wood,water,dusts.etc.<br><br>
                                 *Hygienically use these packing materials as possible as early.<br>
                                 The effectiveness of heat treatment is best before 30 days.
                              </td>
                           </tr>
                        </table>
                     </table>
                  </div> 
               </center>                 
            </div>
         </div>
      </form>
   </body>
   
</html>

