<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					User List
					<!-- <span class="label label-info pull-right">10 Items</span> -->
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_user">Add User</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>User Name</th>
								<th>User Email</th>
								<th>User Address</th>
								<th>Create Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($user_details !=null) {
									foreach ($user_details as $key => $value) {
										$id = $value['user_id'];
										$password = $this->encryption->decrypt($value['user_password']);
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['user_name']?></td>
											<td><?= $value['user_email']?></td>	
											<td><?= $value['user_address']?></td>	
											<td><?= $value['create_date']?></td>										
											<td>   
												<a href="javascript:void(0)" onclick="edit_user('<?=$value['user_id']?>','<?=$value['user_name']?>','<?=$value['user_email']?>','<?=$password?>','<?=$value['user_address']?>');">

					                            	<button type="button" title="Edit User" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button>
					                            </a>

					                            <a href="<?= base_url('User_list/delete_user/'.$id)?>"><button type="button" title="Delete User" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Add user -->

	<div class="modal fade" id="add_user" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add User</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('User_list/add_user') ?>" method="post" enctype="multipart/form-data">
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">User Name</label>
	                                    <input type="text" name="user_name" id="user_name" class="form-control txtOnly" value="" placeholder="User Name">
	                                    <label id="consignee_address-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">User Email</label>
	                                    <input type="text" name="user_email" id="user_email" class="form-control user_email" value="" placeholder="User Email">
	                                    <div class="valid" style="color: red"></div>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">User Password</label>
	                                    <input type="password" name="user_password" id="user_password" class="form-control user_password" value="" placeholder="User Password">
	                                </div>

	                                <div class="form-group">
	                                    <label for="">User Address</label>
	                                    <textarea name="user_address" id="user_address" class="form-control"></textarea>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- end user -->

	<!-- edit user -->
	<div class="modal fade" id="edituser" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit User</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('User_list/update_user') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="user_id" type="hidden" name="user_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">User Name</label>
	                                    <input type="text" name="edit_user_name" id="edit_user_name" class="form-control txtOnly" value="" placeholder="User Name">
	                                    <label id="consignee_address-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">User Email</label>
	                                    <input type="text" name="edit_user_email" id="edit_user_email" class="form-control" value="" placeholder="User Email">
	                                    <div id="valid" style="color: red"></div>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">User Password</label>
	                                    <input type="text" name="edit_user_password" id="edit_user_password" class="form-control edit_user_password" value="" placeholder="User Password">
	                                </div>

	                                <div class="form-group">
	                                    <label for="">User Address</label>
	                                    <textarea name="edit_user_address" id="edit_user_address" class="form-control"></textarea>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- end user -->

	<!-- Modal -->
<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {

		$('.check').click(function(){
            if(isemptyfocus('user_name') || isemptyfocus('user_email') || isemptyfocus('user_password') || isemptyfocus('user_address')){
                return false;
            }
            if(isvalidemail('user_email')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_user_name') || isemptyfocus('edit_user_email') || isemptyfocus('edit_user_password') || isemptyfocus('edit_user_address')){
                return false;
            }
            if(isvalidemail('edit_user_email')){
                return false;
            }
        });  
    });

    $(".user_email").keyup(function(){
    	// alert();
		var email = $(".user_email").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        if (!filter.test(email)) {
           $(".valid").text(email+" is not a valid email");
           email.focus();
           return false;
        } else {
            $(".valid").text("");
        }
    });

    $("#edit_user_email").keyup(function(){
    	// alert();
		var email = $("#edit_user_email").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        if (!filter.test(email)) {
           $("#valid").text(email+" is not a valid email");
           email.focus();
           return false;
        } else {
            $("#valid").text("");
        }
    });

    function edit_user(id,u_name,u_email,u_password,u_address)
    {
    	// alert(id);
	    $('#user_id').val(id);
	    $('#edit_user_name').val(u_name);
	    $('#edit_user_email').val(u_email);
	    $('#edit_user_password').val(u_password);
	    $('#edit_user_address').val(u_address);
      	$("#edituser").modal('show');
    } 
</script>

