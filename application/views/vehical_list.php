<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Vehical List
					<!-- <span class="label label-info pull-right">10 Items</span> -->
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_item">Add Vehical</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Vehical Number</th>
								<th>Vehical Type</th>
								<th>Create Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($vehical_details !=null) {
									foreach ($vehical_details as $key => $value) {
										$id = $value['vehical_id'];
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['vehical_number']?></td>	
											<td><?= $value['vehical_type']?></td>	
											<td><?= $value['create_date']?></td>										
											<td>              
					                            <a href="javascript:void(0)" onclick="edit_vehical('<?=$value['vehical_id']?>','<?=$value['vehical_number']?>','<?=$value['vehical_type']?>');">

					                            	<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button></a>
					                            
					                            <a href="<?= base_url('Vehical/delete_vehical/'.$id)?>"><button type="button" title="Delete Vehical" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Modal -->

	<!-- Add Menu -->
	
	<div class="modal fade" id="add_item" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add Vehical</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                	<div class="col-md-12">

	                        <form action="<?= base_url('Vehical/add_vehical') ?>" method="post" enctype="multipart/form-data" id="coupon">
	                           
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Vehical Number</label>
	                                    <input type="text" name="vehical_number" id="vehical_number" class="form-control" maxlength="15" value="" placeholder="Vehical Number"></textarea>
	                                    <label id="vehical_number-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Vehical Type</label>
	                                    <input type="text" name="vehical_type" id="vehical_type" class="form-control" value="" placeholder="Vehical Type"></textarea>
	                                    <label id="vehical_type-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<!-- /.modal -->
	<!-- Add Menu -->

	<!-- edit Menu -->

	<div class="modal fade" id="editvehical" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit Vehical Details</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('Vehical/update_vehical') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="vehical_id" type="hidden" name="vehical_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Vehical Number</label>
	                                    <input type="text" name="edit_vehical_number" id="edit_vehical_number" class="form-control" maxlength="15" value="" placeholder="Vehical Number"></textarea>
	                                    <label class="text-danger name_allredy"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Vehical Type</label>
	                                    <input type="text" name="edit_vehical_type" id="edit_vehical_type" class="form-control" value="" placeholder="Vehical Type"></textarea>
	                                    <label id="edit_vehical_type-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- edit Menu -->

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('vehical_number') || isemptyfocus('vehical_type')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_vehical_number') || isemptyfocus('edit_vehical_type')){
                return false;
            }
        });     
    });

    $("#edit_vehical_number").keyup(function () {
   		// alert();
	    var name = $(this).val();

	    $.ajax({
           type: "POST",
           url: "<?= base_url('vehical/record_exist')?>",
           data: {edit_vehical_number:name},
           success: function(data)
           {
           		$('.name_allredy').html(data);
               // alert(data);
           }
        });
	}); 

    function edit_vehical(id,vehical_number,vehical_type)
    {
    	// alert(id);
	    $('#vehical_id').val(id);
	    $('#edit_vehical_number').val(vehical_number);
	    $('#edit_vehical_type').val(vehical_type);
      	$("#editvehical").modal('show');
    } 
</script>
		