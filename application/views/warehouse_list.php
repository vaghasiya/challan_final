<?php $this->load->view('include/header')?>
<?php $this->load->view('include/side_panel')?>

	<div id="main-container">
		<div class="padding-md">
			<div class="panel panel-default table-responsive">
				<?php $this->load->view('include/messages')?>

				<div class="panel-heading">
					Warehouse List
					<!-- <span class="label label-info pull-right">10 Items</span> -->
				</div>
				<div class="padding-md clearfix">
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_warehouse">Add warehouse</button><br><br><br>

					<table class="table table-striped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>warehouse Name</th>
								<th>warehouse Address</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($warehouse_details !=null) {
									foreach ($warehouse_details as $key => $value) {
										$id = $value['warehouse_id'];
									?>
										<tr> 
											<td><?= $key+1;?></td>
											<td><?= $value['warehouse_name']?></td>
											<td><?= $value['warehouse_address']?></td>									
											<td>              
					                            <a href="javascript:void(0)" onclick="edit_warehouse('<?=$value['warehouse_id']?>','<?=$value['warehouse_name']?>','<?=$value['warehouse_address']?>');">

					                            	<button type="button" title="Edit product" class="btn btn-success btn-xs bt"><i class="fa fa-pencil"></i></button></a>
					                            
					                            <a href="<?= base_url('Warehouse/delete_warehouse/'.$id)?>"><button type="button" title="Delete Warehouse" class="btn btn-danger btn-xs" onclick="return ConfirmDelete();"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>              
					                        </td> 
										</tr>
									<?php }
								}	
							?>
							
						</tbody>
					</table>
				</div><!-- /.padding-md -->
			</div><!-- /panel -->
		</div><!-- /.padding-md -->
	</div><!-- /main-container -->

	<!-- Modal -->

	<!-- Add Menu -->
	
	<div class="modal fade" id="add_warehouse" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Add Warehouse</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                	<div class="col-md-12">

	                        <form action="<?= base_url('Warehouse/add_warehouse') ?>" method="post" enctype="multipart/form-data" id="coupon">
	                           
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Warehouse Name</label>
	                                    <input type="text" name="warehouse_name" id="warehouse_name" class="form-control txtOnly" value="" placeholder="Warehouse Name" />
	                                    <label id="warehouse_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Warehouse Address</label>
	                                    <textarea name="warehouse_address" id="warehouse_address" class="form-control" value="" placeholder="Address"></textarea>
	                                    <label id="warehouse_address-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<!-- /.modal -->
	<!-- Add Menu -->

	<!-- edit Menu -->

	<div class="modal fade" id="editwarehouse" tabindex="-1">
	    <div class="modal-dialog modal-md" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title">Edit Warehouse</h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-12 col-lg-12">
	                        <form action="<?= base_url('Warehouse/update_warehouse') ?>" method="post" enctype="multipart/form-data">

	                            <input class="form-control" id="warehouse_id" type="hidden" name="warehouse_id"/>
	                          
	                            <div class="col-md-12 col-lg-12">
	                                <div class="form-group">
	                                    <label for="">Warehouse Name</label>
	                                    <input type="text" name="edit_warehouse_name" id="edit_warehouse_name" class="form-control txtOnly" value="" placeholder="Warehouse Name" />
	                                    <label id="warehouse_name-error" class="text-danger pull-right"></label>
	                                </div>

	                                <div class="form-group">
	                                    <label for="">Warehouse Address</label>

	                                    <textarea name="edit_warehouse_address" id="edit_warehouse_address" class="form-control" value="" placeholder="Address"></textarea>
	                                    <label id="warehouse_address-error" class="text-danger pull-right"></label>
	                                </div>

		                            <div class="form-group">
		                                <button type="submit" id="EditC" class="btn btn-success edit_check">Save</button>
		                                <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
		                            </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- edit Menu -->

<?php $this->load->view('include/footer')?>	

<script>
	$(document).ready(function() {
        $('.check').click(function(){
            if(isemptyfocus('warehouse_name') || isemptyfocus('warehouse_address')){
                return false;
            }
        });

        $('.edit_check').click(function(){
            if(isemptyfocus('edit_warehouse_name') || isemptyfocus('edit_warehouse_address')){
                return false;
            }
        });     
    });

    function edit_warehouse(id,w_name,address)
    {
    	// alert(id);
	    $('#warehouse_id').val(id);
	    $('#edit_warehouse_name').val(w_name);
	    $('#edit_warehouse_address').val(address);
      	$("#editwarehouse").modal('show');
    } 
</script>