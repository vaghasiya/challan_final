-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2018 at 06:18 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challan_genration`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_mobile` double NOT NULL,
  `admin_password` text NOT NULL,
  `admin_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'active=0,deactive=1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_mobile`, `admin_password`, `admin_status`) VALUES
(1, 'admin', 'admin@mail.com', 9999999999, 'bd84f8b20a3a0af66b6f6becea1366ae2c0ca5c9ff0fbdd7132e980fe43178fe6ee29075acae36c7a430ea7cf9dedd520a27f008c6ed4d36f3d04ce796635251s3mXoDyJCZl89BF9QncA59r9/Ii33ozR6RpYj09Mffs=', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_challan_item`
--

CREATE TABLE `tbl_challan_item` (
  `challan_item_id` int(11) NOT NULL,
  `challan_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_challan_item`
--

INSERT INTO `tbl_challan_item` (`challan_item_id`, `challan_id`, `item_id`, `item_quantity`, `user_id`) VALUES
(57, 14, 13, 12, 5),
(58, 14, 12, 22, 5),
(59, 14, 11, 5, 5),
(60, 15, 10, 4, 5),
(61, 15, 12, 3, 5),
(62, 16, 13, 2, 5),
(63, 16, 11, 6, 5),
(64, 16, 12, 8, 5),
(66, 17, 14, 4, 6),
(67, 18, 14, 450, 6),
(69, 18, 16, 450, 6),
(71, 19, 17, 77, 7),
(75, 21, 16, 5, 6),
(76, 22, 18, 6, 6),
(77, 22, 16, 11, 6),
(78, 23, 16, 2, 6),
(79, 24, 14, 6, 6),
(80, 24, 15, 8, 6),
(81, 26, 16, 12, 6),
(82, 27, 16, 77, 6),
(83, 27, 18, 44, 6),
(84, 27, 15, 44, 6),
(85, 27, 14, 20, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignee`
--

CREATE TABLE `tbl_consignee` (
  `consignee_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `consignee_name` varchar(255) DEFAULT NULL,
  `consignee_address` text,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_consignee`
--

INSERT INTO `tbl_consignee` (`consignee_id`, `user_id`, `consignee_name`, `consignee_address`, `create_date`) VALUES
(10, 5, 'kamala consignee', 'rajkot', '2018-08-09 15:38:25'),
(11, 6, 'walmart consignee', 'mumbai', '2018-08-09 15:38:43'),
(12, 6, 'consignee one', 'consignee one consignee one consignee one consignee one consignee one', '2018-08-10 09:43:00'),
(13, 6, 'uuuu', '67668hikkjhk', '2018-08-10 14:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignee_details`
--

CREATE TABLE `tbl_consignee_details` (
  `consignee_details_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `consignee_id` int(11) NOT NULL,
  `challan_id` int(11) NOT NULL,
  `invoice_date` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `gross_weight` varchar(255) NOT NULL,
  `freight_amount` varchar(255) NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_consignee_details`
--

INSERT INTO `tbl_consignee_details` (`consignee_details_id`, `user_id`, `consignee_id`, `challan_id`, `invoice_date`, `amount`, `gross_weight`, `freight_amount`, `remarks`) VALUES
(21, 7, 11, 19, '10/08/2018', '77777', '8', '7768', 'hkhjikhj'),
(22, 6, 14, 18, '08-10-2018', '12000', '65', '4', 'ressss'),
(23, 6, 14, 17, '06-09-2018', '5654657', '88', '567', 'hjhgjhgjhgj'),
(24, 6, 15, 15, '21-08-2018', '12000', '3', '2500', 'test'),
(25, 6, 15, 26, '24/08/2018', '7657', '55', '54675', 'ghjghj'),
(26, 6, 16, 24, '01-01-1970', '15000', '45', '6000', 'test\r\n'),
(27, 6, 17, 27, '01-01-1970', '555', '6', '77', 'ghgfhgfh'),
(28, 6, 16, 27, '01-01-1970', '5566', '7', '888', 'hgjhkhjk'),
(117, 6, 64, 27, '31/08/2018', '654654', '6546', '456', '546546'),
(118, 6, 65, 26, '01-01-1970', '567', '44', '657', 'utyu'),
(119, 6, 66, 27, '31/08/2018', '56756756', '765756', '75675', '756'),
(124, 6, 67, 27, '31-08-2018', '54646', '55', '566', 'gfhgfh'),
(125, 6, 68, 27, '05/09/2018', '565', '5467', '567', '567');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consignor`
--

CREATE TABLE `tbl_consignor` (
  `consignor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `consignor_name` varchar(255) DEFAULT NULL,
  `consignor_address` text,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_consignor`
--

INSERT INTO `tbl_consignor` (`consignor_id`, `user_id`, `consignor_name`, `consignor_address`, `create_date`) VALUES
(8, 5, 'Kamala warehouse one', 'rajkot', '2018-08-09 15:37:22'),
(9, 6, 'Walmart warehouse one', 'mumbai', '2018-08-09 15:37:53'),
(10, 6, 'Consignior one', 'consignior one consignior one consignior one consignior one', '2018-08-10 09:42:40'),
(11, 7, 'Cc', 'sssssssssss', '2018-08-10 13:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_despatch`
--

CREATE TABLE `tbl_despatch` (
  `despatch_id` int(11) NOT NULL,
  `despatch_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_despatch`
--

INSERT INTO `tbl_despatch` (`despatch_id`, `despatch_name`, `user_id`, `create_date`) VALUES
(4, 'Delhi', 5, '2018-08-09 16:04:51'),
(5, 'Despath one', 6, '2018-08-10 09:44:07'),
(6, 'Despath two', 6, '2018-08-10 09:44:17'),
(7, 'Jkhkhk', 6, '2018-08-10 14:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_destination`
--

CREATE TABLE `tbl_destination` (
  `destination_id` int(11) NOT NULL,
  `destination_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_destination`
--

INSERT INTO `tbl_destination` (`destination_id`, `destination_name`, `user_id`, `create_date`) VALUES
(5, 'Rajkot', 5, '2018-08-09 16:04:35'),
(6, 'Destination one', 6, '2018-08-10 09:44:29'),
(7, 'Destination two', 6, '2018-08-10 09:44:40'),
(8, 'Uyiuyiuyiyu', 7, '2018-08-10 14:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_genrate_challan`
--

CREATE TABLE `tbl_genrate_challan` (
  `challan_id` int(11) NOT NULL,
  `refrence_number` text NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'login id',
  `consignor_id` int(11) NOT NULL,
  `consignee_id` int(11) NOT NULL,
  `vehical_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `perticulars_goods` text,
  `e_way_bill_no` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_genrate_challan`
--

INSERT INTO `tbl_genrate_challan` (`challan_id`, `refrence_number`, `user_id`, `consignor_id`, `consignee_id`, `vehical_id`, `warehouse_id`, `perticulars_goods`, `e_way_bill_no`, `create_date`) VALUES
(14, '493', 5, 9, 11, 12, 15, 'good', '', '2018-08-09 10:13:18'),
(15, '635', 5, 9, 11, 11, 15, 'qqq', '', '2018-08-09 10:15:21'),
(16, '623', 5, 8, 10, 10, 14, 'nyc', '', '2018-08-09 10:16:54'),
(17, '191', 6, 10, 12, 14, 16, 'test goods', '', '2018-08-10 04:15:26'),
(18, '243', 6, 10, 13, 13, 17, 'gfhggggggg', '', '2018-08-10 06:50:15'),
(19, '504', 7, 11, 13, 15, 18, 'uyiuyiuyi', '', '2018-08-10 10:48:54'),
(21, '408', 6, 10, 13, 16, 17, 'fghfh', '', '2018-08-22 08:13:26'),
(22, '589', 6, 10, 12, 16, 17, 'fghfh', '789456', '2018-08-22 08:13:48'),
(23, '112', 6, 10, 11, 16, 17, 'gfgfhfgh', '', '2018-08-23 15:11:07'),
(24, '563', 6, 10, 12, 16, 17, 'ghghjhgjghj', '9999', '2018-08-24 06:51:52'),
(25, '697', 6, 10, 12, 0, 17, 'jlkjl', '', '2018-08-24 07:07:16'),
(26, '211', 6, 10, 12, 13, 17, 'fghgfhgfh', '11550', '2018-08-28 11:51:02'),
(27, '700', 6, 10, 12, 16, 17, 'jhkjhk', '888', '2018-08-28 12:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_genrate_consignee`
--

CREATE TABLE `tbl_genrate_consignee` (
  `consignee_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `despatch_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `refrence_number` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_genrate_consignee`
--

INSERT INTO `tbl_genrate_consignee` (`consignee_id`, `user_id`, `warehouse_id`, `despatch_id`, `destination_id`, `refrence_number`, `create_date`) VALUES
(7, 5, 15, 4, 5, '549', '2018-08-09 10:37:21'),
(8, 5, 15, 4, 5, '458', '2018-08-09 13:40:23'),
(11, 7, 18, 7, 8, '766', '2018-08-10 10:49:14'),
(14, 6, 17, 6, 6, '725', '2018-08-10 13:29:57'),
(15, 6, 17, 7, 7, '601', '2018-08-24 08:24:04'),
(16, 6, 17, 6, 6, '572', '2018-08-29 13:00:00'),
(64, 6, 17, 7, 7, '189', '2018-08-31 14:05:46'),
(65, 6, 17, 7, 7, '374', '2018-08-31 14:07:05'),
(66, 6, 17, 6, 7, '128', '2018-08-31 14:09:32'),
(67, 6, 17, 6, 7, '604', '2018-08-31 14:11:37'),
(68, 6, 17, 7, 7, '637', '2018-09-05 06:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice_number`
--

CREATE TABLE `tbl_invoice_number` (
  `invoice_number_id` int(11) NOT NULL,
  `challan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `invoice_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_invoice_number`
--

INSERT INTO `tbl_invoice_number` (`invoice_number_id`, `challan_id`, `user_id`, `invoice_number`) VALUES
(18, 14, 5, '1234'),
(19, 14, 5, '4567'),
(20, 15, 5, '3456'),
(21, 16, 5, '7896'),
(22, 16, 5, '4585'),
(23, 17, 6, '1254'),
(24, 17, 6, '1236'),
(25, 18, 6, '456'),
(26, 18, 6, '789'),
(27, 19, 7, '7767'),
(28, 19, 7, '7888'),
(30, 21, 6, '45654'),
(31, 22, 6, '45654'),
(32, 23, 6, '4545'),
(33, 23, 6, '4545645'),
(34, 24, 6, '657'),
(35, 25, 6, '56756'),
(36, 27, 6, '44'),
(37, 27, 6, '787'),
(39, 26, 6, '4444');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item`
--

CREATE TABLE `tbl_item` (
  `item_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_item`
--

INSERT INTO `tbl_item` (`item_id`, `user_id`, `item_name`, `create_date`) VALUES
(10, 5, 'Snacks', '2018-08-09 15:39:10'),
(11, 5, 'Beverages', '2018-08-09 15:39:21'),
(12, 5, 'Cloths', '2018-08-09 15:39:37'),
(13, 5, 'Cerials', '2018-08-09 15:40:14'),
(14, 6, 'Item one', '2018-08-10 09:43:10'),
(15, 6, 'Item two', '2018-08-10 09:43:19'),
(16, 6, 'Ttttttt', '2018-08-10 10:05:04'),
(17, 7, 'Uuuuu', '2018-08-10 14:17:49'),
(18, 6, 'Ssss', '2018-08-29 18:55:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_address` text,
  `user_name` varchar(255) DEFAULT NULL,
  `user_email` text,
  `user_password` text,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_address`, `user_name`, `user_email`, `user_password`, `create_date`) VALUES
(5, 'rajkot', 'developer', 'developer@gmail.com', 'a99b01f759b9466dddfa87b7a46174e3fd70ad4a6e882ea7d72f3ee2a8da2254ce1d3b105d8e008793e4e156d0a6ec1d0f88e2e9a7adac04a4bf908e64117a45KOwNMpEW7mXj4PZ9L1EUpk/hhUqVAHQBR9JRWKiwjbI=', '2018-08-09 15:34:38'),
(6, 'surat', 'milan', 'milan@mail.com', 'a99b01f759b9466dddfa87b7a46174e3fd70ad4a6e882ea7d72f3ee2a8da2254ce1d3b105d8e008793e4e156d0a6ec1d0f88e2e9a7adac04a4bf908e64117a45KOwNMpEW7mXj4PZ9L1EUpk/hhUqVAHQBR9JRWKiwjbI=', '0000-00-00 00:00:00'),
(8, 'dfgdfgwwwwwwww', 'test', 'test@mail.com', '3436effebd604d7c2d3b9307fe09905d637ce39dea705c4607c4a6caa6fce790c80ebd184f86fd5574b48ae83072ed9a30274ae60848051018cfbe40323662b0nzrIP3yxo4/H7wG0FWk/dcOPkyvJuYO+aWM4N7n4wEk=', '2018-08-20 18:42:58'),
(11, 'sdsfdsf', 'hgjghj', 'milanvv@mail.com', 'c212635cbf6337c5a50a7c261654708ff04d9a4f8d3af4fa6df35f1241537932e5b2eef0b3bbabd1b5a50e323e866ebc4a1390d39aa9811f97dd0ebb6e3c41b9v9JJLQUC7K8sfuBE0qLhdIk+wK9RhKGk3IuPZpDDfoU=', '2018-08-22 12:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vehical`
--

CREATE TABLE `tbl_vehical` (
  `vehical_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehical_number` varchar(255) DEFAULT NULL,
  `vehical_type` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_vehical`
--

INSERT INTO `tbl_vehical` (`vehical_id`, `user_id`, `vehical_number`, `vehical_type`, `create_date`) VALUES
(10, 5, 'GJ3 -122', 'car', '2018-08-09 15:40:35'),
(11, 5, 'GJ3-224', 'activa', '2018-08-09 15:40:57'),
(12, 5, 'GJ3-445', 'bike', '2018-08-09 15:41:23'),
(13, 6, 'Gj-03-3547', 'activa', '2018-08-10 09:43:39'),
(14, 6, 'Gj-04-3216', 'tata ace', '2018-08-10 09:43:54'),
(15, 7, '6357', 'car', '2018-08-10 14:18:08'),
(16, 6, 'Gj-04-2216', 'tata ace', '2018-08-20 17:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_warehouse`
--

CREATE TABLE `tbl_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'login id',
  `warehouse_name` varchar(255) DEFAULT NULL,
  `warehouse_address` text,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_warehouse`
--

INSERT INTO `tbl_warehouse` (`warehouse_id`, `user_id`, `warehouse_name`, `warehouse_address`, `create_date`) VALUES
(14, 5, 'Kamala warehouse', 'rajkot', '2018-08-09 15:35:25'),
(15, 5, 'Walmart', 'mumbai', '2018-08-09 15:36:41'),
(16, 6, 'Ware house one', ' warehouse onewarehouse onewarehouse onewarehouse onewarehouse onewarehouse onewarehouse one', '2018-08-10 09:42:25'),
(17, 6, 'Ware house new test', 'ware house new test ware house new test ware house new testware house new test', '2018-08-10 10:19:30'),
(18, 7, 'W', 'wewewewe', '2018-08-10 13:41:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_challan_item`
--
ALTER TABLE `tbl_challan_item`
  ADD PRIMARY KEY (`challan_item_id`);

--
-- Indexes for table `tbl_consignee`
--
ALTER TABLE `tbl_consignee`
  ADD PRIMARY KEY (`consignee_id`);

--
-- Indexes for table `tbl_consignee_details`
--
ALTER TABLE `tbl_consignee_details`
  ADD PRIMARY KEY (`consignee_details_id`);

--
-- Indexes for table `tbl_consignor`
--
ALTER TABLE `tbl_consignor`
  ADD PRIMARY KEY (`consignor_id`);

--
-- Indexes for table `tbl_despatch`
--
ALTER TABLE `tbl_despatch`
  ADD PRIMARY KEY (`despatch_id`);

--
-- Indexes for table `tbl_destination`
--
ALTER TABLE `tbl_destination`
  ADD PRIMARY KEY (`destination_id`);

--
-- Indexes for table `tbl_genrate_challan`
--
ALTER TABLE `tbl_genrate_challan`
  ADD PRIMARY KEY (`challan_id`);

--
-- Indexes for table `tbl_genrate_consignee`
--
ALTER TABLE `tbl_genrate_consignee`
  ADD PRIMARY KEY (`consignee_id`);

--
-- Indexes for table `tbl_invoice_number`
--
ALTER TABLE `tbl_invoice_number`
  ADD PRIMARY KEY (`invoice_number_id`);

--
-- Indexes for table `tbl_item`
--
ALTER TABLE `tbl_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_vehical`
--
ALTER TABLE `tbl_vehical`
  ADD PRIMARY KEY (`vehical_id`);

--
-- Indexes for table `tbl_warehouse`
--
ALTER TABLE `tbl_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_challan_item`
--
ALTER TABLE `tbl_challan_item`
  MODIFY `challan_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `tbl_consignee`
--
ALTER TABLE `tbl_consignee`
  MODIFY `consignee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_consignee_details`
--
ALTER TABLE `tbl_consignee_details`
  MODIFY `consignee_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `tbl_consignor`
--
ALTER TABLE `tbl_consignor`
  MODIFY `consignor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_despatch`
--
ALTER TABLE `tbl_despatch`
  MODIFY `despatch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_destination`
--
ALTER TABLE `tbl_destination`
  MODIFY `destination_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_genrate_challan`
--
ALTER TABLE `tbl_genrate_challan`
  MODIFY `challan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_genrate_consignee`
--
ALTER TABLE `tbl_genrate_consignee`
  MODIFY `consignee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `tbl_invoice_number`
--
ALTER TABLE `tbl_invoice_number`
  MODIFY `invoice_number_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbl_item`
--
ALTER TABLE `tbl_item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_vehical`
--
ALTER TABLE `tbl_vehical`
  MODIFY `vehical_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_warehouse`
--
ALTER TABLE `tbl_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
